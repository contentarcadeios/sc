//
//  ThumbNailDetailViewController.swift
//  Secret Calculator
//
//  Created by Awais on 10/08/2018.
//  Copyright © 2018 Awais. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit



class ThumbNailDetailViewController: UIViewController {
    //MARK: Outlets
    @IBOutlet weak var labelHeader: UILabel!
    @IBOutlet weak var ImageScrollView: ImageScrollView!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var videoView: UIView!
    var imageNumber: String = ""
    var selectedImage : String = ""
    var selectedAlbum : String = ""
    var selectedThumbNail : String = ""
    var urlOfAllImagesInSelectedAlbum = [String]()
    var img = UIImage()
    var imageToShow = UIImage()
    var playerViewController:AVPlayerViewController?
    var currentIndex : Int = 0
    let directions: [UISwipeGestureRecognizerDirection] = [.right, .left]
    var swipeGesture  = UISwipeGestureRecognizer()
    var urls = [String]()
  
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var footerView: UIView!
    
    class func ThumbNailDetailVC() -> ThumbNailDetailViewController{
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "thumbnailDetailVC") as! ThumbNailDetailViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapGR = UITapGestureRecognizer(target: self, action: #selector(ThumbNailDetailViewController.handleTap(_:)))
        tapGR.delegate = self
        tapGR.numberOfTapsRequired = 1
        view.addGestureRecognizer(tapGR)
        
        self.view.backgroundColor = UIColor.black
        self.ImageScrollView.initialOffset = .center
        self.ImageScrollView.imageContentMode = .widthFill
        //self.ImageScrollView.initialOffset =
        //self.ImageScrollView.imageContentMode = .widthFill
        labelHeader.text = imageNumber
        print(urlOfAllImagesInSelectedAlbum)
        for direction in directions {
            swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(self.swipwView(_:)))
            ImageScrollView.addGestureRecognizer(swipeGesture)
            swipeGesture.direction = direction
            ImageScrollView.isUserInteractionEnabled = true
            ImageScrollView.isMultipleTouchEnabled = true
        }
        let imageNumbers = imageNumber.components(separatedBy: "/").first
        currentIndex = Int(imageNumbers!)! - 1
        for url in urlOfAllImagesInSelectedAlbum{
            urls.append(url.replacingOccurrences(of: "/thumbNail", with: ""))
        }
 
        
    }
  
    func getThumbNailWithURL(videoUrl: String) -> UIImage
    {
        let video = NSURL(fileURLWithPath: videoUrl.replacingOccurrences(of: ".jpg", with: ".mov"), isDirectory: false)
        let videoImage = AVAsset(url: video as URL)
        let frameImageGen : AVAssetImageGenerator = AVAssetImageGenerator.init(asset: videoImage)
        var time = videoImage.duration
        time.value = 0
        let framePhoto = try! frameImageGen.copyCGImage(at: time, actualTime: nil)
        let frameFirstPhoto = UIImage(cgImage: framePhoto)
        return frameFirstPhoto
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        UserDefaults.standard.set(true, forKey: "allowLock")
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.videoView.isHidden = true
    }
    override func viewDidAppear(_ animated: Bool) {
        setTheImage()
    }
    
    @IBAction func dismissVc(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func setTheImage() -> Void {
        if selectedImage.range(of:"movie") != nil {
            let img = getThumbNailWithURL(videoUrl: selectedImage)
            playButton.isHidden = false
            if(img != nil){
                self.ImageScrollView.display(image: img)
                self.ImageScrollView.layoutSubviews()
                self.ImageScrollView.superview?.layoutIfNeeded()
            }
        }else
        {
            let img = UIImage(named: selectedImage)
            playButton.isHidden = true
            if(img != nil){
                self.ImageScrollView.display(image: img!)
                self.ImageScrollView.layoutSubviews()
                self.ImageScrollView.superview?.layoutIfNeeded()
            }
        }
        self.ImageScrollView.layoutSubviews()
        self.ImageScrollView.layoutIfNeeded()
    }
    @IBAction func deleteImage(_ sender: Any) {
        Utility.showAlertWithTwoButtons(caller: self, title: "Delete", message: "Are you sure to delete ?",firstButtonTitle:"Cancel",firstButtonStyle: .default,firstButtonHandler: {
            (action) in
        }, secondButtonTitle: "Delete",secondButtonStyle: .destructive,secondButtonHandler: {
            (action) in
            do{
                
                let object_to_Delete = self.urlOfAllImagesInSelectedAlbum[self.currentIndex]
                print(object_to_Delete)
                if(self.selectedImage.contains("movie")){
                    var pathToDelete = object_to_Delete.replacingOccurrences(of: "jpg", with: "mov")
                    pathToDelete = pathToDelete.replacingOccurrences(of: "/thumbNail", with: "")
                    if(pathToDelete.count > 0){
                        try FileManager.default.removeItem(atPath: pathToDelete)
                        SyncManager.sharedManager.syncData()
                    }
                 
                }else
                {
                    let pathToDelete = object_to_Delete.replacingOccurrences(of: "/thumbNail", with: "")
                    try FileManager.default.removeItem(atPath: pathToDelete)
                    SyncManager.sharedManager.syncData()
                }
                try FileManager.default.removeItem(atPath: object_to_Delete)
                self.urls.remove(at: self.currentIndex)
                self.urlOfAllImagesInSelectedAlbum.remove(at: self.currentIndex)
                self.moveToNextORBackWard()
//                let swipe = UISwipeGestureRecognizer()
//                swipe.direction = .left
//                self.swipwView(swipe)
//                self.viewWillAppear(true)
            }
            catch{
                print(error.localizedDescription)
            }
           // self.navigationController?.popViewController(animated: true)
        })
    }
    //MARK: Move to Next Image after Delete
    func moveToNextORBackWard(){
        print(self.currentIndex)
        if(self.urls.count == 0){
            self.navigationController?.popViewController(animated: true)
            return
        }
        if(self.currentIndex == urls.count){
            //show last
            currentIndex = currentIndex - 1
            print("next to show", self.currentIndex)
        }
        if(self.currentIndex < 0){
            currentIndex = self.urls.count - 1
            print("pre to show", self.currentIndex)
            
        }
        self.labelHeader.text = "\(self.currentIndex + 1) / \(self.urls.count)"
        self.swipeImage(animation: UIViewAnimationOptions.transitionFlipFromRight)
    }
    @IBAction func saveBackToAlbum(_ sender: Any) {
        UIImageWriteToSavedPhotosAlbum(img, nil, nil, nil)
    }
    override func viewDidDisappear(_ animated: Bool) {
        
    }
    //MARK: Share Image/video
    @IBAction func shareImage(_ sender: Any) {
        let object_to_share = urlOfAllImagesInSelectedAlbum[currentIndex]
        if(object_to_share.contains("movie")){
            let url_of_object = object_to_share.replacingOccurrences(of: "/thumbNail", with: "")
            let full_path_of_object = url_of_object.replacingOccurrences(of: ".jpg", with: ".mov")
            let object = URL(fileURLWithPath: full_path_of_object, isDirectory: false)
            let activityViewController = UIActivityViewController(activityItems: [ "Hay Check This",object ] , applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
             self.present(activityViewController, animated: true, completion: nil)
        }else
        {
            let url_of_object = object_to_share.replacingOccurrences(of: "/thumbNail", with: "")
            let object = URL(fileURLWithPath: url_of_object, isDirectory: false)
            let activityViewController = UIActivityViewController(activityItems: [ "Hay Check This",object ] , applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
             self.present(activityViewController, animated: true, completion: nil)
        }
    }
    //MARK: Swipe to next image
    @objc func swipwView(_ sender : UISwipeGestureRecognizer){
        setCurrentIndex(sender)
    }
    
    func setCurrentIndex(_ sender : UISwipeGestureRecognizer){
        if(urls.count > 1){
            switch sender.direction {
            case .right:
                self.currentIndex = self.currentIndex - 1
                if(self.currentIndex < 0){
                    self.currentIndex = self.urls.count - 1
                }
                self.labelHeader.text = "\(self.currentIndex + 1) / \(self.urls.count)"
                self.swipeImage(animation: UIViewAnimationOptions.transitionFlipFromRight)
                break
            case .left:
                self.currentIndex = self.currentIndex + 1
                if(self.currentIndex == self.urls.count){
                    self.currentIndex = 0
                }
                self.labelHeader.text = "\(self.currentIndex + 1) / \(self.urls.count)"
                self.swipeImage(animation: UIViewAnimationOptions.transitionFlipFromLeft)
                break
            default: break
            }
        }
    }
    
    func swipeImage(animation: UIViewAnimationOptions){
        if(self.urls[self.currentIndex].contains("movie")){
            imageToShow = UIImage(contentsOfFile: self.urlOfAllImagesInSelectedAlbum[self.currentIndex])!
            selectedImage = self.urls[self.currentIndex]
            
            //imageToShow = UIImage(named: self.urlOfAllImagesInSelectedAlbum[self.currentIndex])!
            self.playButton.isHidden = false
            if(imageToShow != nil){
                self.changeImageWithAnimation(img: imageToShow, animation: animation)
               // self.ImageScrollView.display(image: imageToShow)
                self.ImageScrollView.imageContentMode = .widthFill
                self.ImageScrollView.layoutIfNeeded()
            }
        }else
        {
            selectedImage = self.urls[self.currentIndex]
         imageToShow = UIImage(contentsOfFile: self.urlOfAllImagesInSelectedAlbum[self.currentIndex].replacingOccurrences(of: "/thumbNail", with: ""))!
            self.playButton.isHidden = true
            if(imageToShow != nil){
                self.changeImageWithAnimation(img: imageToShow, animation: animation)
                self.ImageScrollView.imageContentMode = .widthFill
                self.ImageScrollView.layoutIfNeeded()
            }
        }
    }
    func changeImageWithAnimation(img: UIImage,animation: UIViewAnimationOptions){
        self.ImageScrollView.zoomView?.image = nil
        UIView.transition(with: ImageScrollView!,
                          duration: 0.75,
                          options: animation ,
                          animations: { self.ImageScrollView.display(image: img)},
                          completion: nil)
    }
    
    //MARK: Play Video
    @IBAction func playButton(_ sender: Any) {
        let url = self.urls[self.currentIndex].replacingOccurrences(of: ".jpg", with: ".mov")
        let videoURL = NSURL(fileURLWithPath: url, isDirectory: false)
        if(videoURL != nil){
            let player = AVPlayer(url: videoURL as URL)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            playerViewController.player?.play()
            UserDefaults.standard.set(false, forKey: "allowLock")
            self.present(playerViewController, animated: true)
//            {
//                playerViewController.player!.play()
//            }
        }
    } 
    
}
extension ThumbNailDetailViewController: UIGestureRecognizerDelegate {
    @objc func handleTap(_ gesture: UITapGestureRecognizer){
        print("doubletapped")
        //hideShowHeaderAndFooterView()
    }
}
extension ThumbNailDetailViewController{
    func hideShowHeaderAndFooterView() -> Void {
        let height_of_header = self.headerView.frame.height
        let height_of_footer = self.footerView.frame.height
        let y_of_header = self.headerView.frame.origin.y
        let y_of_footer = self.footerView.frame.origin.y
        print("y of header", y_of_header)
        print("y of footer", y_of_footer)
        if(y_of_header == 0.0){
            UIView.animate(withDuration: 0.5) {
                self.headerView.frame = CGRect(x: 0, y: -height_of_header, width: self.headerView.frame.width, height: self.headerView.frame.height)
                self.ImageScrollView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            }
            
        }else
        {
            UIView.animate(withDuration: 0.5) {
                self.headerView.frame = CGRect(x: 0, y:0 , width: self.headerView.frame.width, height: self.headerView.frame.height)
                self.ImageScrollView.frame = CGRect(x: 0, y: self.headerView.frame.height , width: self.view.frame.width, height: self.view.frame.height - (self.headerView.frame.height + self.footerView.frame.height))
            }
        }
        if(y_of_footer == 622){
            UIView.animate(withDuration: 0.5) {
                self.footerView.frame = CGRect(x: 0, y: y_of_footer + height_of_footer, width: self.footerView.frame.width, height: self.footerView.frame.height)
            }
        }else
        {
            UIView.animate(withDuration: 0.5) {
                self.footerView.frame = CGRect(x: 0, y: y_of_footer - height_of_footer , width: self.footerView.frame.width, height: self.footerView.frame.height)
            }
        }
    }
}



