//
//  LogInViewController.swift
//  Secret Calculator
//
//  Created by Awais on 06/08/2018.
//  Copyright © 2018 Awais. All rights reserved.
//

import UIKit
import Reachability

class LogInViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var textFieldEmail: UITextField!
    var userName: String? = nil
    let reachability = Reachability()

    override func viewDidLoad() {
        super.viewDidLoad()

      
        // Do any additional setup after loading the view.
        textFieldEmail.delegate = self
        
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        textFieldEmail.text = ""
    }


    @IBAction func logIn(_ sender: Any) {
    
     
        if(textFieldEmail.text?.count != 0){
            if(isValidEmail(testStr: self.textFieldEmail.text!))
            {
                if let reachable = reachability?.isReachable{
                    if(reachable){
                        userDefaults.set(textFieldEmail.text!.lowercased(), forKey: "uN")
                        let vc = storyboard?.instantiateViewController(withIdentifier: "resetpasswordvc")
                        userDefaults.set(true, forKey: "fromLoginVC")
                        self.navigationController?.pushViewController(vc!, animated: true)
                    }else
                    {
                        Utility.showErrorAlert(caller: self, message: "Could not connect to internet")
                    }
                }
            }
            else{
                Utility.showMessageWithOkButton(caller: self, title: "Alert", message: "Please enter a valid email address")
            }
        }else
        {
            Utility.showMessageWithOkButton(caller: self, title: "Alert", message: "Please enter a valid email address")
        }
    }
    
    @IBAction func signUpVc(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "signupvc") as! SignUpViewController
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func forgetPassword(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "fgemailvc") as! ForegetPasswordViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
//MARK: TextField Delegates
extension LogInViewController{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textFieldEmail.resignFirstResponder()
        return true
    }
}
