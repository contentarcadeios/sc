//
//  SignUpViewController.swift
//  Secret Calculator
//
//  Created by Awais on 06/08/2018.
//  Copyright © 2018 Awais. All rights reserved.
//

import UIKit
import Firebase
import NVActivityIndicatorView
class SignUpViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        textFieldEmail.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func dismissVc(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)

    }
    @IBAction func signUp(_ sender: Any) {
        if(textFieldEmail.text?.count != 0){
            if(isValidEmail(testStr: self.textFieldEmail.text!))
            {
                textFieldEmail.resignFirstResponder()
                activityIndicator.startAnimating()
                Auth.auth().fetchProviders(forEmail: self.textFieldEmail.text!, completion: {
                    (providers, error) in
                    
                    if let error = error {
                        self.activityIndicator.stopAnimating(); print(error.localizedDescription)
                    } else if let providers = providers {
                        print(providers)
                        self.activityIndicator.stopAnimating();
                        Utility.showMessageWithOkButton(caller: self, title: "Alert", message: "Please use a different email address")
                       
                    }else{
                       self.activityIndicator.stopAnimating()
                        UserDefaults.standard.setValue(self.textFieldEmail.text, forKey: "UN")
                        let setPassword  = self.storyboard?.instantiateViewController(withIdentifier: "enterpasswordvc") as! SetPasswordViewController
                        self.present(setPassword, animated: true, completion: nil)
                    }
                })
            
            }
            else{
                Utility.showMessageWithOkButton(caller: self, title: "Alert", message: "Please enter a valid email address")
            }
        }
        else{
            Utility.showMessageWithOkButton(caller: self, title: "Alert", message: "Please enter a valid email address")
        }
        
    }
    
 

}
//MARK: TextField Delegates
extension SignUpViewController{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textFieldEmail.resignFirstResponder()
        return true
    }
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
}
