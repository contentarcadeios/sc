//
//  PrivateCloudViewController.swift
//  Secret Calculator
//
//  Created by Awais on 06/09/2018.
//  Copyright © 2018 Awais. All rights reserved.
//

import UIKit
import AWSS3
import NVActivityIndicatorView
class PrivateCloudViewController: UIViewController {
    @IBOutlet weak var FreeSpaceViewWidth: NSLayoutConstraint!
    var sizeOfObjects : CGFloat = 0.0
    let screenSize = UIScreen.main.bounds
    let totalSpace : CGFloat = 1024
    var numberOfImages = 0
    var numberOfVideos = 0
    var fetchedAlbumsCoreData = [Album]()
    @IBOutlet weak var barView: UIStackView!
    @IBOutlet weak var labelVideoCount: UILabel!
    @IBOutlet weak var lableImageCount: UILabel!
    @IBOutlet weak var labelFreeSpace: UILabel!
    @IBOutlet weak var labelUsedSpace: UILabel!
    @IBOutlet weak var blackView: UIView!
    @IBOutlet weak var indicatorNV: NVActivityIndicatorView!
    var spaceSubscribed : CGFloat = 0.0
    @IBOutlet weak var switchBackUpWifi: UISwitch!
    override func viewDidLoad() {
        super.viewDidLoad()
        let space = userDefaults.value(forKey: "spaceSubscribe") as? CGFloat
        spaceSubscribed = space! * TOBYTES
        // Do any additional setup after loading the view.
        indicatorNV.color = UIColor(red: 255/255, green: 157/255, blue: 0, alpha: 1)
        switchBackUpWifi.onTintColor = UIColor(red: 255/255, green: 157/255, blue: 0, alpha: 1)
        let wifiBackup = userDefaults.bool(forKey: "wifiBackup")
        if(wifiBackup){
            switchBackUpWifi.isOn = true
        }else
        {
            switchBackUpWifi.isOn = false
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        sizeOfObjects = 0
      
        fetchedAlbumsCoreData = CoreDataManager.sharedManager.fetchAllalbums()
        if(fetchedAlbumsCoreData.count > 0){
        var i = 0;
        indicatorNV.startAnimating()
        for album in fetchedAlbumsCoreData{
            
                calculateSizeOfBucket(albumName: album.albumName!, callbackHandler: {
                    
                    DispatchQueue.main.async{
                        if(i == self.fetchedAlbumsCoreData.count){
                            
                            let freeSpace = self.spaceSubscribed - self.sizeOfObjects
                            self.labelUsedSpace.text = self.sizeToPrettyString(size: self.sizeOfObjects)
//                            let remSpace = (( 1024 - self.sizeOfObjects / (1024 * 1024)) / 1024)
                            self.labelFreeSpace.text = self.sizeToPrettyString(size: freeSpace)
                                //String(format: "%.2f GB", remSpace)
                            self.calculateTheLengthOfBlackView(used: self.sizeOfObjects, free: self.spaceSubscribed)
                            self.indicatorNV.stopAnimating()
                        }
                    }
                })
                i = i + 1
            }
        }else
        {
            
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        if(fetchedAlbumsCoreData.count == 0){
            let barlength = self.barView.frame.size.width
            FreeSpaceViewWidth.constant = CGFloat(barlength - 0)
            blackView.updateConstraints()
            UIView.animate(withDuration: 1.5) {
                self.view.layoutIfNeeded()
            }
            labelFreeSpace.text = self.sizeToPrettyString(size: self.spaceSubscribed)
            labelUsedSpace.text = "0 KB"
            indicatorNV.stopAnimating()
        }
    }
    @IBAction func backupWifiOnOff(_ sender: Any) {
        
        let wifiBackup = userDefaults.bool(forKey: "wifiBackup")
        
        if(wifiBackup){
         
            Utility.showAlertWithTwoButtons(caller: self, title: "Backup on wifi", message: "Data will be synced to private cloud either  you are connected to cellular data or Wifi", firstButtonTitle: "Turn Off", firstButtonStyle: .default, firstButtonHandler: { (alert) in
                userDefaults.set(false, forKey: "wifiBackup")
                DispatchQueue.main.async {
                    self.switchBackUpWifi.isOn = false
                }
            }, secondButtonTitle: "Cancel", secondButtonStyle: .cancel, secondButtonHandler: {(alert) in
                userDefaults.set(true, forKey: "wifiBackup")
                DispatchQueue.main.async {
                    self.switchBackUpWifi.isOn = true
                }
            })
//            userDefaults.set(false, forKey: "wifiBackup")
//            switchBackUpWifi.isOn = false
        }else
        {
            Utility.showAlertWithTwoButtons(caller: self, title: "Backup on wifi", message: "Data will be synced to private cloud only when you will be connected to wifi", firstButtonTitle: "Turn On", firstButtonStyle: .default, firstButtonHandler: { (alert) in
                userDefaults.set(true, forKey: "wifiBackup")
                DispatchQueue.main.async {
                    self.switchBackUpWifi.isOn = true
                }
            }, secondButtonTitle: "Cancel", secondButtonStyle: .cancel, secondButtonHandler: {(alert) in
                userDefaults.set(false, forKey: "wifiBackup")
                DispatchQueue.main.async {
                     self.switchBackUpWifi.isOn = false
                }
               
            })
//            userDefaults.set(true, forKey: "wifiBackup")
//            switchBackUpWifi.isOn = true
        }
    }
    @IBAction func popVC(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}
extension PrivateCloudViewController{
    func calculateSizeOfBucket(albumName: String, callbackHandler: @escaping () -> Void) -> Void {
        let userName = userDefaults.value(forKey: "userName") as! String
        let s3 = AWSS3.default()
        let req = AWSS3ListObjectsRequest()
        req?.bucket = "ca-ios-apps"
        req?.prefix = "ios/SecretCalculator/"+userName+"/"+albumName+"/"
        s3.listObjects(req!).continueWith { (task) -> Void in
            if (task.error != nil) {
                print("error fetching objects")
            }else{
               if let data = task.result?.contents{
                for  i in 0 ..< data.count {
                    let firstObject = data[i]
                    if(((firstObject.key?.range(of: "jpg")) != nil) || ((firstObject.key?.range(of: "mov")) != nil)){
                        self.sizeOfObjects =  self.sizeOfObjects + CGFloat(truncating: (firstObject.size)!)
                        if (firstObject.key?.contains("jpg"))!{
                            self.numberOfImages = self.numberOfImages + 1
                            DispatchQueue.main.async {
                                 self.lableImageCount.text = "\(self.numberOfImages)"
                            }
                           
                        }else{
                            self.numberOfVideos = self.numberOfVideos + 1
                             DispatchQueue.main.async {
                                self.labelVideoCount.text = "\(self.numberOfVideos)"
                            }
                        }
                    }
                    print("size of object ",round(self.sizeOfObjects/(1024*1024)) , "MB")
                    
                }
                
            }
                callbackHandler()
            }
        }
    }
    func calculateTheLengthOfBlackView(used: CGFloat, free:CGFloat)->Void{
        let barlength = self.barView.frame.size.width
        let usedPerctent = (used / free ) * 100
        print("user % age", usedPerctent)
        let widthOfYellowView = ((usedPerctent * screenSize.size.width) / 100 )
        print("yellow width", widthOfYellowView)
        print("black width", ((usedPerctent * screenSize.size.width) / 100 ))
        FreeSpaceViewWidth.constant = (barlength - widthOfYellowView)
        blackView.updateConstraints()
        UIView.animate(withDuration: 1.5) {
            self.view.layoutIfNeeded()
        }
    }
    private func sizeToPrettyString(size: CGFloat) -> String {
        let s = size/(1024*1024*1024)
        let folderSize = String(format: "%.2f GB", s)
//        let byteCountFormatter = ByteCountFormatter()
//        byteCountFormatter.allowedUnits = .useGB
//        byteCountFormatter.countStyle = .file
//        let folderSizeToDisplay = byteCountFormatter.string(fromByteCount: Int64(round(( Double(Int64(size))))))
        
        return folderSize
        
    }
}
