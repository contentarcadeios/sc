//
//  SelectLockTimeVC.swift
//  Secret Calculator
//
//  Created by Awais on 30/08/2018.
//  Copyright © 2018 Awais. All rights reserved.
//

import UIKit

class SelectLockTimeVC: UIViewController {
    @IBOutlet weak var ltableViewLockTime: UITableView!
    var time = ["10S","20S","30S","60S"]
    var lockTimeOldSelected: Float?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        ltableViewLockTime.delegate = self
        ltableViewLockTime.dataSource = self
        ltableViewLockTime.tableFooterView = UIView()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        lockTimeOldSelected = UserDefaults.standard.float(forKey: "lockTime")
        ltableViewLockTime.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func popVC(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension SelectLockTimeVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return time.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = ltableViewLockTime.dequeueReusableCell(withIdentifier: "cell") as? SelectTimeTableViewCell
        let timeCurrent = Float(time[indexPath.row].dropLast())
        if(timeCurrent == lockTimeOldSelected){
           cell?.imageViewSelected.isHidden = false
        }
        else{
            cell?.imageViewSelected.isHidden = true
        }
        cell?.labelTime.text = time[indexPath.row]
        cell?.selectionStyle = .none
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let cell = tableView.cellForRow(at: indexPath) as? SelectTimeTableViewCell
        cell?.imageViewSelected.isHidden = false
        let selectedTime = Float(time[indexPath.row].dropLast())
        UserDefaults.standard.set(selectedTime, forKey: "lockTime")
        viewWillAppear(true)
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as? SelectTimeTableViewCell
        cell?.imageViewSelected.isHidden = true
        print("old value" + time[indexPath.row])
        viewWillAppear(true)

    }
    
}
