//
//  SelectTimeTableViewCell.swift
//  Secret Calculator
//
//  Created by Awais on 30/08/2018.
//  Copyright © 2018 Awais. All rights reserved.
//

import UIKit

class SelectTimeTableViewCell: UITableViewCell {
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var imageViewSelected: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imageViewSelected.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if(selected)
        {
            imageViewSelected.isHidden = false

        }
    
   
        // Configure the view for the selected state
    }


}
