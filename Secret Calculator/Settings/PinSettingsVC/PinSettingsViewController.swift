//
//  PinSettingsViewController.swift
//  Secret Calculator
//
//  Created by Awais on 29/08/2018.
//  Copyright © 2018 Awais. All rights reserved.
//

import UIKit

class PinSettingsViewController: UIViewController {
    @IBOutlet weak var tableViewPinSettings: UITableView!
    var settingsName = ["CHANGE PASSCODE","APP LOCKED AFTER"]
    var oldSelectedTime: Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        tableViewPinSettings.delegate = self
        tableViewPinSettings.dataSource = self
        tableViewPinSettings.tableFooterView = UIView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        oldSelectedTime = Int(UserDefaults.standard.float(forKey: "lockTime"))
        tableViewPinSettings.reloadData()
    }
    @IBAction func popVC(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
extension PinSettingsViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settingsName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableViewPinSettings.dequeueReusableCell(withIdentifier: "cell") as? PinSettingsTableViewCell
        if(indexPath.row == 0)
        {
            cell?.imageViewMoveNext.isHidden = false
            cell?.labelLockTime.isHidden = true
        }
        else{
            cell?.imageViewMoveNext.isHidden = false
            cell?.labelLockTime.isHidden = false
            cell?.labelLockTime.text = "\(oldSelectedTime!)S"
        }
        cell?.labelSettingsName.text = settingsName[indexPath.row]
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.row == 0)
        {
            userDefaults.set(true, forKey: "isFromSettings")
            let vc = storyboard?.instantiateViewController(withIdentifier: "resetpasswordvc") as! ResetPasswordViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if (indexPath.row == 1){
            let vc = storyboard?.instantiateViewController(withIdentifier: "timevc") as! SelectLockTimeVC
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }

}
