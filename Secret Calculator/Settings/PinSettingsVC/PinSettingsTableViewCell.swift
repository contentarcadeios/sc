//
//  PinSettingsTableViewCell.swift
//  Secret Calculator
//
//  Created by Awais on 29/08/2018.
//  Copyright © 2018 Awais. All rights reserved.
//

import UIKit

class PinSettingsTableViewCell: UITableViewCell {
    @IBOutlet weak var labelSettingsName: UILabel!
    @IBOutlet weak var labelLockTime: UILabel!
    @IBOutlet weak var imageViewMoveNext: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
