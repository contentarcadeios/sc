//
//  PremiumTableViewCell.swift
//  Secret Calculator
//
//  Created by Awais on 06/09/2018.
//  Copyright © 2018 Awais. All rights reserved.
//

import UIKit

class PremiumTableViewCell: UITableViewCell {
    @IBOutlet weak var labelStorage: UILabel!
    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet weak var imageViewBestSellings: UIImageView!
    @IBOutlet weak var buyButton: UIButton!
    
    @IBOutlet weak var imageViewMemory: UIImageView!
    var premiumButtonPressed : (() -> Void)? = nil
    var privacyPolicyButtonPressed : (() -> Void)? = nil
    var termAndConditionButtonPressed : (() -> Void)? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBAction func buyPremium(_ sender: Any) {
        if let btnAction = self.premiumButtonPressed
        {
            btnAction()
        }
    }
    @IBAction func privacyPolicyPressed(_ sender: Any) {
        if let btnAction = self.privacyPolicyButtonPressed
        {
            btnAction()
        }
    }
    
    @IBAction func termAndConditionPressed(_ sender: Any) {
        if let btnAction = self.termAndConditionButtonPressed
        {
            btnAction()
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
