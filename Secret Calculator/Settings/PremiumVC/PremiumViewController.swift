//
//  PremiumViewController.swift
//  Secret Calculator
//
//  Created by Awais on 06/09/2018.
//  Copyright © 2018 Awais. All rights reserved.
//

import UIKit
import StoreKit
import SwiftyStoreKit
import NVActivityIndicatorView
import Reachability


class PremiumViewController: UIViewController {
    @IBOutlet weak var tableViewPremium: UITableView!
    let storage = ["10 GB","25 GB","100 GB","250 GB"]
    @IBOutlet weak var progressIndicator: NVActivityIndicatorView!
    let price = ["$5","$10","$30","$75"]
    var products = [SKProduct]()
    let reachability = Reachability()!
    var currentProducts = [SKProduct]()
    var subscription = String()
    let productIds = Set([ tenGB,twentyFiveGB,hundredGB,twoFiftyGB])
    let productIdentifies = Set([tenGB,twentyFiveGB,hundredGB,twoFiftyGB])
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var headerView: UIView!
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        products.removeAll()
        currentProducts.removeAll()
        // Do any additional setup after loading the view.
        tableViewPremium.delegate = self
        tableViewPremium.dataSource = self
        tableViewPremium.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if(appDelegate.currentProducts.count > 0){
            self.currentProducts = appDelegate.currentProducts
            self.sortPurchases()
        }else
        {
            if reachability.connection == .none{
                getPurchases(complitionHandler: {
                    self.sortPurchases()
                })
            }else{
                Utility.showErrorAlert(caller: self, message: "No internet connectivity, Please connect to an active internet")
            }
            
        }
        if(userDefaults.value(forKey: "subscribedCat") as? String != nil){
            subscription = userDefaults.value(forKey: "subscribedCat") as! String
        }
        if(userDefaults.value(forKey: "subscribedCat") as? String != nil){
            subscription = userDefaults.value(forKey: "subscribedCat") as! String
          
        }
        tableViewPremium.reloadData()
    }
    func sortPurchases()->Void{
        let prodcts = currentProducts
        //sorting products
        if(prodcts.count > 0){
            for prodct in prodcts
            {
                if(prodct.productIdentifier == tenGB){
                    currentProducts.remove(at: 0)
                    currentProducts.insert(prodct, at: 0)
                }
                else if(prodct.productIdentifier == twentyFiveGB){
                    currentProducts.remove(at: 1)
                    currentProducts.insert(prodct, at: 1)
                }else if(prodct.productIdentifier == hundredGB){
                    currentProducts.remove(at: 2)
                    currentProducts.insert(prodct, at: 2)
                }else  if(prodct.productIdentifier == twoFiftyGB){
                    currentProducts.remove(at: 3)
                    currentProducts.insert(prodct, at: 3)
                }
            }
            if(progressIndicator.isAnimating){
                progressIndicator.stopAnimating()
            }
        }else
        {
            progressIndicator.stopAnimating()
            currentProducts.removeAll()
        }
        print(currentProducts.count)
        print(appDelegate.currentProducts)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func popVC(_ sender: Any) {
        userDefaults.set(true, forKey: "allowLock")
        self.navigationController?.popViewController(animated: true)
    }
    func openURL(urlLink:String) -> Void {
        if let url = URL(string: urlLink) {
            UIApplication.shared.open(url, options: [:])
        }
    }
}
extension PremiumViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if( currentProducts.count != 0)
        {
            return (currentProducts.count + 1)
        }else
        {
            return (storage.count + 1)
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(indexPath.row == 4){
            let cell = tableView.dequeueReusableCell(withIdentifier: "pcell") as! PremiumTableViewCell
            cell.privacyPolicyButtonPressed = {
                print("openPrivacyLink")
                //
                self.openURL(urlLink: OPENPRIVACYLINK)
            }
            cell.termAndConditionButtonPressed = {
                print("term and condition pressed")
                //
                self.openURL(urlLink: TERMCONDITIONS)
            }
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! PremiumTableViewCell
            
            
            if(currentProducts.count != 0){
                cell.labelPrice.text = currentProducts[indexPath.row].localizedPrice
                cell.labelStorage.text = currentProducts[indexPath.row].productIdentifier.components(separatedBy: ".").last?.uppercased()
                if(userDefaults.bool(forKey: SUBSCRIPTION_PRO)){
                    if(currentProducts[indexPath.row].productIdentifier == subscription){
                        cell.buyButton.isUserInteractionEnabled = false
                        cell.buyButton.setBackgroundImage(#imageLiteral(resourceName: "disable_btn"), for: .normal)
                        cell.buyButton.setTitle("SUBSCRIBED", for: .normal)
                        
                    }else
                    {
                        cell.buyButton.isUserInteractionEnabled = true
                        cell.buyButton.setBackgroundImage(#imageLiteral(resourceName: "button"), for: .normal)
                        cell.buyButton.setTitle("CHANGE PLAN", for: .normal)
                    }
                }
                if(currentProducts[indexPath.row].productIdentifier == tenGB){
                    cell.imageViewMemory.image = #imageLiteral(resourceName: "icon_0")
                    cell.imageViewBestSellings.isHidden = true
                }else if(currentProducts[indexPath.row].productIdentifier == twentyFiveGB){
                    cell.imageViewBestSellings.isHidden = true
                    cell.imageViewMemory.image = #imageLiteral(resourceName: "icon_1")
                }else if(currentProducts[indexPath.row].productIdentifier == hundredGB){
                    cell.imageViewMemory.image = #imageLiteral(resourceName: "icon_2")
                    cell.imageViewBestSellings.isHidden = false
                }else{
                    cell.imageViewBestSellings.isHidden = true
                    cell.imageViewMemory.image = #imageLiteral(resourceName: "icon_3")
                }
                
            }else
            {
                cell.labelPrice.text = price[indexPath.row]
                cell.labelStorage.text = storage[indexPath.row]
                cell.imageViewMemory.image = UIImage(named: "icon_" + "\(indexPath.row)")
            }
            
            cell.premiumButtonPressed = {
                if self.reachability.connection != .none{
                    if(self.currentProducts.count != 0 ){
                        userDefaults.set(false, forKey: "allowLock")
                        self.progressIndicator.startAnimating()
                        self.backButton.isUserInteractionEnabled = false
                        print("offer selected",indexPath.row)
                        
                        // Changes
                        let productToBuy = self.currentProducts[indexPath.row]
                        print("product id = ",productToBuy.productIdentifier)
                        self.subscribe(currentProduct: productToBuy)
                      //  self.purchaseProd(productIdentifier: productToBuy.productIdentifier)
                    }else
                    {
                        self.getPurchases(complitionHandler: {
                            self.sortPurchases()
                            let productToBuy = self.currentProducts[indexPath.row]
                            print("product id = ",productToBuy.productIdentifier)
                            self.subscribe(currentProduct: productToBuy)
                        })
                    }
                }
                else{
                    Utility.showErrorAlert(caller: self, message: "No internet connectivity, Please connect to an active internet")
                }
            }
            cell.selectionStyle = .none
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.row == 4){
            return 380
        }else{
            return 220
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.row == 4){
            
        }
    }
}
extension PremiumViewController{
    @objc func buyPremiumOffer(index: Int) -> Void {
        print("buyed")
    }
}


//MARK// Buy Product
extension PremiumViewController{
    func subscribe(currentProduct:SKProduct) -> Void {
        if  (currentProducts) != nil && self.currentProducts.count > 1
        {
            self.progressIndicator.startAnimating()
            self.backButton.isUserInteractionEnabled = false
            SwiftyStoreKit.purchaseProduct(currentProduct) { (result) in
                switch result {
                case .success(let purchase):
                    // Deliver content from server, then:
                    if purchase.needsFinishTransaction {
                        SwiftyStoreKit.finishTransaction(purchase.transaction)
                    }
                    let appleValidator = AppleReceiptValidator(service: .production, sharedSecret: shareedSecret)
                    SwiftyStoreKit.verifyReceipt(using: appleValidator) { [weak self] result in
                     
                        if case .success(let receipt) = result {
                            let purchaseResult = SwiftyStoreKit.verifySubscriptions(productIds: self!.productIds, inReceipt: receipt)
                            switch purchaseResult {
                                
                            case .purchased(let expiryDate, let receiptItems):
                                userDefaults.set(PURCHASED, forKey: "isAllowedToSync")
                                UserDefaults.standard.set(true, forKey: SUBSCRIPTION_PRO)
                                userDefaults.set(true, forKey: "allowLock")
                                UserDefaults.standard.synchronize()
                                DispatchQueue.main.async {
                                    self?.progressIndicator.stopAnimating()
                                    self?.backButton.isUserInteractionEnabled = true
                                    self?.viewWillAppear(true)
                                }
                                print("Purchase Success: \(purchase.productId)")
                                userDefaults.set(purchase.productId, forKey: "subscribedCat")
                                switch purchase.productId {
                                    case tenGB:
                                        userDefaults.set(10.0, forKey: "spaceSubscribe")
                                        break
                                    case twentyFiveGB:
                                        userDefaults.set(25.0, forKey: "spaceSubscribe")
                                        break
                                    case hundredGB:
                                        userDefaults.set(100.0 , forKey: "spaceSubscribe")
                                        break
                                    case twoFiftyGB:
                                        userDefaults.set(250.0 , forKey: "spaceSubscribe")
                                        break
                                    default: break
                                    
                                }
                                print("Product is valid until \(expiryDate)")
                            case .expired(let expiryDate, let receiptItems):
                                print("Product is expired since \(expiryDate)")
                                userDefaults.set(true, forKey: "allowLock")
                                UserDefaults.standard.set(false, forKey: SUBSCRIPTION_PRO)
                                userDefaults.set(EXPIRED, forKey: "isAllowedToSync")
                                DispatchQueue.main.async {
                                    self?.progressIndicator.stopAnimating()
                                    self?.backButton.isUserInteractionEnabled = true
                                }
                            case .notPurchased:
                                userDefaults.set(NOTPURCHASED, forKey: "isAllowedToSync")
                                print("This product has never been purchased")
                                userDefaults.set(true, forKey: "allowLock")
                                UserDefaults.standard.set(false, forKey: SUBSCRIPTION_PRO)
                                DispatchQueue.main.async {
                                    self?.progressIndicator.stopAnimating()
                                    self?.backButton.isUserInteractionEnabled = true
                                }
                            }
                            
                        } else {
                            // receipt verification error
                        }
                    }
                    
                case .error(let error):
                    self.showErrorAlert(error: error)
                    userDefaults.set(true, forKey: "allowLock")
                    self.progressIndicator.stopAnimating()
                    self.backButton.isUserInteractionEnabled = true
                }
            }
            
            //PaymentManager.sharedManager.buyProduct(currentProdunct)
        }
        
    }
}
extension PremiumViewController{
    func getPurchases(complitionHandler:@escaping()->())-> Void{
    
        progressIndicator.startAnimating()
        SwiftyStoreKit.retrieveProductsInfo([tenGB,twentyFiveGB,hundredGB,twoFiftyGB]) { (results) in
            if results.retrievedProducts.count > 0 {
                self.currentProducts = Array(results.retrievedProducts)
                self.viewWillAppear(true)
                complitionHandler()
            }else{
               
            }
        }
    }
    func showErrorAlert(error: SKError){
        switch error.code {
            case .unknown: print("Unknown error. Please contact support")
            Utility.showErrorAlert(caller: self, message:error.localizedDescription)
                break
            case .clientInvalid: print("Not allowed to make the payment")
            Utility.showErrorAlert(caller: self, message: "Not allowed to make the payment")
                break
            case .paymentCancelled: break
            case .paymentInvalid:
                print("The purchase identifier was invalid")
                Utility.showErrorAlert(caller: self, message: "Not allowed to make the payment")
                break
            case .paymentNotAllowed: print("The device is not allowed to make the payment")
                Utility.showErrorAlert(caller: self, message: "The device is not allowed to make the payment")
                break
            case .storeProductNotAvailable: print("The product is not available in the current storefront")
                Utility.showErrorAlert(caller: self, message: "The product is not available in the current storefront")
                break
            case .cloudServicePermissionDenied: print("Access to cloud service information is not allowed")
                Utility.showErrorAlert(caller: self, message: "Access to cloud service information is not allowed")
                break
            case .cloudServiceNetworkConnectionFailed: print("Could not connect to the network")
            case .cloudServiceRevoked: print("User has revoked permission to use this cloud service")
                break
            default: break
            
        }
    }
}
