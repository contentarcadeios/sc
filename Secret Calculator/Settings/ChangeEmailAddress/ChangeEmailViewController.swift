//
//  ChangeEmailViewController.swift
//  Secret Calculator
//
//  Created by Awais on 26/09/2018.
//  Copyright © 2018 Awais. All rights reserved.
//

import UIKit

class ChangeEmailViewController: UIViewController {
    @IBOutlet weak var newEmailTextFiwls: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        newEmailTextFiwls.delegate = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func changeEmailAddress(_ sender: Any) {
        if((newEmailTextFiwls.text?.count)! > 0){
            let isValid = isValidEmail(testStr: newEmailTextFiwls.text!)
            if(isValid){
                userDefaults.set(true, forKey: "isFromChangeEmail")
                userDefaults.set(newEmailTextFiwls.text!, forKey: "newEmail")
                let vc = storyboard?.instantiateViewController(withIdentifier: "resetpasswordvc") as! ResetPasswordViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }else
            {
                Utility.showMessageWithOkButton(caller: self, title: "Alert!", message: "Please enter a valid email address")
            }
        }
    }
    
    @IBAction func popVc(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
extension ChangeEmailViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        newEmailTextFiwls.resignFirstResponder()
        return true
    }
}
