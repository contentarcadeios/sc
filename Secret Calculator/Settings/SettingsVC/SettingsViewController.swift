//
//  SettingsViewController.swift
//  Secret Calculator
//
//  Created by Awais on 13/08/2018.
//  Copyright © 2018 Awais. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    @IBOutlet weak var tableViewSettings: UITableView!
    let settingsNames = ["account","pin","premium","private cloud","space saver","sign out"]
    let imagesNames = ["account","pin","premium","private_cloud","SpaceSaver", "sign out"]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        tableViewSettings.delegate = self
        tableViewSettings.dataSource = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    @IBAction func popVC(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
}
extension SettingsViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settingsNames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableViewSettings.dequeueReusableCell(withIdentifier: "cell") as? SettingsTableViewCell
        cell?.labelSettingName.text = settingsNames[indexPath.row].uppercased()
        
            cell?.imageViewIcon.image = UIImage(named: imagesNames[indexPath.row])
        
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 80
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.row == 0){
            let vc = storyboard?.instantiateViewController(withIdentifier: "accountvc") as! AccountsViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if(indexPath.row == 1)
        {
            let vc = storyboard?.instantiateViewController(withIdentifier: "pinvc") as! PinSettingsViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if (indexPath.row == 4)
        {
            let vc = storyboard?.instantiateViewController(withIdentifier: "spacesaver") as! SpaceSaverViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if (indexPath.row == 3){
            let vc = storyboard?.instantiateViewController(withIdentifier: "privatecloudvc") as! PrivateCloudViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if (indexPath.row == 2){
            let vc = storyboard?.instantiateViewController(withIdentifier: "premiumvc") as! PremiumViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if(indexPath.row == 5){
            Utility.showAlertWithTwoButtons(caller: self, title: "Alert", message: "Are you sure ? This will erase all the data in application.", firstButtonTitle: "Cancel", firstButtonStyle: .cancel, firstButtonHandler: nil, secondButtonTitle: "Sign Out", secondButtonStyle: .destructive) { (alert) in
                print("Action Goes Here")
                self.clearDiskCache()
                CoreDataManager.sharedManager.deleteAllData()
                UserDefaults.standard.set(60.0, forKey: "lockTime")
                UserDefaults.standard.set(false, forKey: "deleteAfterMove")
                UserDefaults.standard.set(false, forKey: "isLocked")
                userDefaults.set(true, forKey: "priorityCloud")
                userDefaults.set(nil, forKey: "userName")
                userDefaults.set(nil, forKey: "uN")
                userDefaults.set(nil, forKey: "password")
                userDefaults.set(false, forKey: "fromgvc")
                userDefaults.set(false, forKey: "launchedBefore")
                userDefaults.set(false, forKey: "allowLock")
                userDefaults.set("", forKey: "subscribedCat")
                userDefaults.set(false, forKey: "isSyncing")
                userDefaults.set(nil, forKey: SUBSCRIPTION_PRO)
                let window = UIWindow(frame: UIScreen.main.bounds)
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let initialViewController = storyboard.instantiateViewController(withIdentifier: "signInVC") as! UINavigationController
//                window.rootViewController = initialViewController
//                window.makeKeyAndVisible()
                UIApplication.shared.keyWindow?.rootViewController = initialViewController
                print(window.rootViewController?.restorationIdentifier)
                self.navigationController?.popToRootViewController(animated: true)
            }
        }
        self.tableViewSettings.deselectRow(at: indexPath, animated: true)
    }
    func clearDiskCache() {
        let fileManager = FileManager.default
        let myDocuments = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first!
        guard let filePaths = try? fileManager.contentsOfDirectory(at: myDocuments, includingPropertiesForKeys: nil, options: []) else { return }
        for filePath in filePaths {
            try? fileManager.removeItem(at: filePath)
        }
    }
    func removeAllAlbumsFromCoreData()->Void{}

}
