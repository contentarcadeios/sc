//
//  SettingsTableViewCell.swift
//  Secret Calculator
//
//  Created by Awais on 13/08/2018.
//  Copyright © 2018 Awais. All rights reserved.
//

import UIKit

class SettingsTableViewCell: UITableViewCell {

    
    @IBOutlet weak var imageViewIcon: UIImageView!
    
    @IBOutlet weak var labelSettingName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
