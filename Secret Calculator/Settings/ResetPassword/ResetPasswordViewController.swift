//
//  ResetPasswordViewController.swift
//  Secret Calculator
//
//  Created by Awais on 29/08/2018.
//  Copyright © 2018 Awais. All rights reserved.
//

import UIKit
import Firebase
import Reachability
import NVActivityIndicatorView

class ResetPasswordViewController: UIViewController {
    @IBOutlet weak var collectionViewResetPass: UICollectionView!
    @IBOutlet weak var testFieldResetPass: UITextField!
    @IBOutlet weak var backButton: UIButton!
    var internetReachable = Bool()
    let progressBar = LinearProgressBar()
    
    @IBOutlet weak var lineView: UIView!
    
    @IBOutlet weak var indicatorNC: NVActivityIndicatorView!
    let reachability = Reachability()!
    let numberOfCellsPerRow: CGFloat = 4
    var textToDisplay = "0"
    var valueOne : CGFloat?
    var valueTwo : CGFloat?
    var ansWer : CGFloat?
    var oprationToPerform = ""
    var isOperation = true
    var password: String? = nil
    var oldPassword: String? = nil
    var isOldPasswordOK = false
    var isFromLoginVC = false
    var isFromGallery = false
    var isFromSettings = false
    
  
    
    
    // To make the activity indicator appear:
 
    
    // To make the activity indicator disappear:
  //  activityIndicator.stopAnimating()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        collectionViewResetPass.delegate = self
        collectionViewResetPass.dataSource = self
        indicatorNC.color = UIColor(red: 255/255, green: 157/255, blue: 0, alpha: 1)
        isFromLoginVC = userDefaults.bool(forKey: "fromLoginVC")
        isFromSettings = userDefaults.bool(forKey: "isFromSettings")
        isFromGallery = userDefaults.bool(forKey: "fromgvc")
        if(isFromLoginVC){
            testFieldResetPass.placeholder = "Enter Password".uppercased()
        }
        else if(isFromSettings){
            isFromGallery = false
            userDefaults.set(false, forKey: "fromgvc")
            testFieldResetPass.placeholder = "Enter   old   Password".uppercased()
            oldPassword = UserDefaults.standard.value(forKey: "password") as! String
        }
    
    }
    override func viewWillAppear(_ animated: Bool) {
        reachability.whenReachable = { reachability in
            if reachability.connection == .wifi {
                print("Reachable via WiFi")
                self.internetReachable = true
            } else {
                self.internetReachable = true
                print("Reachable via Cellular")
            }
        }
        reachability.whenUnreachable = { _ in
            self.internetReachable = false
        }
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
        isFromGallery = userDefaults.bool(forKey: "fromgvc")
        if(isFromGallery){
            collectionViewResetPass.isUserInteractionEnabled = true
            backButton.isHidden = true
            testFieldResetPass.placeholder = ""
            testFieldResetPass.text = ""
            textToDisplay = ""
            oldPassword = UserDefaults.standard.value(forKey: "password") as! String
            
        }else{
            backButton.isHidden = false
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        userDefaults.set(false, forKey: "fromgvc")
     
    }
    override func viewDidAppear(_ animated: Bool) {
        let screenSize:CGRect = UIScreen.main.bounds
        let y = lineView.frame.origin.y
        progressBar.frame = CGRect(x: 0, y: y - 1, width: screenSize.size.width, height: 3)
        self.view.addSubview(progressBar)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func popVc(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

}
extension ResetPasswordViewController: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        detectButtonPressed(buttonNumber: indexPath.row)
        testFieldResetPass.text = textToDisplay
        
    }
    
    
}
extension ResetPasswordViewController: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 19
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionViewResetPass.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ResetPassCollectionViewCell
        let img  =  indexPath.row + 1
        cell.imageViewCell.image = UIImage(named:"\(img)" + ".png")
        cell.layoutIfNeeded()
        return cell
    }
}
extension ResetPasswordViewController: UICollectionViewDelegateFlowLayout {
    //MARK: - UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat =  0
        let collectionViewSize = collectionView.frame.size.width / 4
        if(DeviceType.IS_IPAD){
           
            if(indexPath.row == 16){
                
                return CGSize(width: collectionViewSize+collectionViewSize  , height: collectionViewSize - 80)
                
            }else if ( indexPath.row == 17 || indexPath.row == 18){
                
                return CGSize(width: collectionViewSize  , height: collectionViewSize - 80)
            }
            else{
                return CGSize(width: collectionViewSize - padding , height: collectionViewSize - 80)
            }
        }
        else{
            if(indexPath.row == 16){
                
                return CGSize(width: collectionViewSize+collectionViewSize  , height: collectionViewSize - 8)
            }else if ( indexPath.row == 17 || indexPath.row == 18){
                
                return CGSize(width: collectionViewSize  , height: collectionViewSize - 8)
            }
            else{
                return CGSize(width: collectionViewSize - padding , height: collectionViewSize - 8)
            }
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
}
extension ResetPasswordViewController{
    func detectButtonPressed(buttonNumber: Int) -> Void {
        switch buttonNumber {
        case 0:
            textToDisplay = "0"
            isOperation = true
            break
        case 1:
            
            if(textToDisplay != "0"){
                if (( textToDisplay.range(of: "-")) == nil)
                {
                    textToDisplay = "-" + textToDisplay
                }
                else{
                    textToDisplay = textToDisplay.replacingOccurrences(of: "-", with: "")
                }
                
            }
            break
        case 2:
            if(internetReachable){
                if(isFromGallery){
                    collectionViewResetPass.isUserInteractionEnabled = true
                    if (oldPassword == testFieldResetPass.text!)
                    {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "gvc")
                        self.navigationController?.pushViewController(vc!, animated: true)
                    }
                    return
                }
                if(isFromLoginVC){
                    progressBar.startAnimating()
                    backButton.isUserInteractionEnabled = false
                    collectionViewResetPass.isUserInteractionEnabled = false
                    let userName = userDefaults.value(forKey: "uN") as! String
                    Auth.auth().signIn(withEmail: userName, password: testFieldResetPass.text!, completion: { (user, error) in
                        if (error == nil)
                        {
                            let isVarifiedUser = Auth.auth().currentUser?.isEmailVerified
                            if(isVarifiedUser)!{
                                self.backButton.isUserInteractionEnabled = true
                                userDefaults.set(false, forKey: "fromLoginVC")
                                userDefaults.set(true, forKey: "launchedBefore")
                                userDefaults.setValue(userName, forKey: "userName")
                                userDefaults.set(self.testFieldResetPass.text!, forKey: "password")
                                userDefaults.set(true, forKey: "allowLock")
                                S3SyncManager.sharedManager.downloadAllobjectsFromS3()
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "gvc")
                                self.progressBar.stopAnimating()
                                self.reachability.stopNotifier()
                                self.navigationController?.pushViewController(vc!, animated: true)
                            }
                            else
                            {
                                self.backButton.isUserInteractionEnabled = true
                                Utility.showAlertWithTwoButtons(caller: self, title: "Alert", message: "Please verify your email address", firstButtonTitle: "Re-send verification email", firstButtonStyle: .default, firstButtonHandler: { (action) in
                                        Auth.auth().currentUser?.sendEmailVerification
                                        { (error) in
                                            if(error == nil)
                                            {
                                                Utility.showMessageWithOkButton(caller: self, title: "Alert", message: "An email has been sent to your account, Please verify your email address")
                                                
                                            }
                                        }
                                }, secondButtonTitle: "OK", secondButtonStyle: .cancel, secondButtonHandler: {(alert) in
                                        self.navigationController?.popViewController(animated: true)
                                })
                                self.collectionViewResetPass.isUserInteractionEnabled = true
                            }
                        }
                        else
                        {
                            Utility.showMessageWithOkButton(caller: self, title: "Alert", message: "Wrong Credentials!")
                            self.progressBar.stopAnimating()
                            self.backButton.isUserInteractionEnabled = true
                            self.collectionViewResetPass.isUserInteractionEnabled = true
                            userDefaults.set(false, forKey: "launchedBefore")
                        }
                    })
                    
                }
                else if(isFromSettings)
                { // Changed on 01/10/18
                    if((testFieldResetPass.text?.count)!<6)
                    {
                        Utility.showMessageWithOkButton(caller: self, title: "Alert", message: "Please chose password of length greater than 6 ")
                        return
                    }
                    if(testFieldResetPass.text == oldPassword)
                    {
                        testFieldResetPass.text = ""
                        textToDisplay = ""
                        testFieldResetPass.placeholder = "New  Password".uppercased()
                        isOldPasswordOK = true
                        return
                    }
                    if(testFieldResetPass.placeholder == "new  password".uppercased())
                    {
                        password = testFieldResetPass.text
                        testFieldResetPass.text = ""
                        textToDisplay = ""
                        testFieldResetPass.placeholder  = "confirm   password".uppercased()
                        return
                    }
                    if(testFieldResetPass.text == password)
                    {
                           progressBar.startAnimating()
                            collectionViewResetPass.isUserInteractionEnabled = false
                           let userName = UserDefaults.standard.value(forKey: "userName") as? String
                            self.changePassword(email: userName!, currentPassword: self.oldPassword!, newPassword: self.testFieldResetPass.text!, completion: {_ in
                                Utility.showMessageWithOkButton(caller: self, title: "Congratulations", message: "Password Changed", firstButtonStyle: .default , firstButtonHandler:  { (action) in
                                    userDefaults.set(false, forKey: "isFromSettings")
                                    UserDefaults.standard.set(self.testFieldResetPass.text, forKey: "password")
                                    DispatchQueue.main.async {
                                       self.progressBar.stopAnimating()
                                        self.collectionViewResetPass.isUserInteractionEnabled = true
                                    }
                                    self.navigationController?.popViewController(animated: true)
                            })
                           
                        
                        } )
                        self.collectionViewResetPass.isUserInteractionEnabled = true
                        
                    }else{
                         Utility.showMessageWithOkButton(caller: self, title: "Alert", message: "Password  miss match")
                        testFieldResetPass.text = ""
                        textToDisplay = ""
                        
                        testFieldResetPass.placeholder = "CURRENT PASSWORD"
                    }
                }
            }
            else
            {
                Utility.showMessageWithOkButton(caller: self, title: "Error", message: "Could not connect to internet")
            }
                
            break
        case 3:
            if(textToDisplay != "")
            {
                isOperation = true
                oprationToPerform = "div"
                valueOne = converToString(str: textToDisplay)
                textToDisplay = "0"
            }
            break
        case 4:
            if(isOperation)
            {
                textToDisplay = ""
                isOperation = false
            }
            textToDisplay = textToDisplay + "\(7)"
            
            break
        case 5:
            if(isOperation)
            {
                textToDisplay = ""
                isOperation = false
            }
            textToDisplay = textToDisplay + "\(8)"
            
            break
        case 6:
            if(isOperation)
            {
                textToDisplay = ""
                isOperation = false
            }
            textToDisplay = textToDisplay + "\(9)"
            
            break
        case 7:
            if(textToDisplay != "")
            {
                isOperation = true
                oprationToPerform = "mul"
                valueOne = converToString(str: textToDisplay)
                textToDisplay = "0"
            }
            break
        case 8:
            if(isOperation)
            {
                textToDisplay = ""
                isOperation = false
            }
            textToDisplay = textToDisplay + "\(4)"
            
            break
        case 9:
            if(isOperation)
            {
                textToDisplay = ""
                isOperation = false
            }
            textToDisplay = textToDisplay + "\(5)"
            
            break
        case 10:
            if(isOperation)
            {
                textToDisplay = ""
                isOperation = false
            }
            textToDisplay = textToDisplay + "\(6)"
            
            break
        case 11:
            if(textToDisplay != "")
            {
                isOperation = true
                oprationToPerform = "sub"
                valueOne = converToString(str: textToDisplay)
                textToDisplay = "0"
            }
            break
        case 12:
            if(isOperation)
            {
                textToDisplay = ""
                isOperation = false
            }
            textToDisplay = textToDisplay + "\(1)"
            
            break
        case 13:
            if(isOperation)
            {
                textToDisplay = ""
                isOperation = false
            }
            textToDisplay = textToDisplay + "\(2)"
            
            break
        case 14:
            
            if(isOperation)
            {
                textToDisplay = ""
                isOperation = false
            }
            textToDisplay = textToDisplay + "\(3)"
            
            
            break
        case 15:
            isOperation = true
            oprationToPerform = "add"
            valueOne = converToString(str: textToDisplay)
            textToDisplay = "0"
            break
        case 16:
            textToDisplay = textToDisplay + "\(0)"
            break
        case 17:
            textToDisplay = textToDisplay + "."
            break
        case 18:
            if(textToDisplay != "")
            {
                valueTwo = converToString(str: textToDisplay)
                textToDisplay = "0"
                switch oprationToPerform{
                case "sub":
                    subValues()
                    break
                case "mul":
                    mulValues()
                    break
                case "add":
                    addValues()
                    break
                case "div":
                    divValues()
                    break
                default:
                    break
                }
            }
            isOperation = true
            valueOne = 0
            valueTwo = 0
            break
            
            
            
        default: break
            
        }
    }
    func addValues(){
        ansWer = valueOne! + valueTwo!
        textToDisplay = "\(ansWer!)"
        if(textToDisplay.last == "0"){
            textToDisplay.removeLast()
        }
        if(textToDisplay.last == "."){
            textToDisplay.removeLast()
        }
    }
    func subValues(){
        ansWer = valueOne! - valueTwo!
        textToDisplay = "\(ansWer!)"
        if(textToDisplay.last == "0"){
            textToDisplay.removeLast()
        }
        if(textToDisplay.last == "."){
            textToDisplay.removeLast()
        }
    }
    func divValues(){
        ansWer = valueOne! / valueTwo!
        textToDisplay = "\(ansWer!)"
        if(textToDisplay.last == "0"){
            textToDisplay.removeLast()
        }
        if(textToDisplay.last == "."){
            textToDisplay.removeLast()
        }
    }
    func mulValues(){
        ansWer = valueOne! * valueTwo!
        textToDisplay = "\(ansWer!)"
        if(textToDisplay.last == "0"){
            textToDisplay.removeLast()
        }
        if(textToDisplay.last == "."){
            textToDisplay.removeLast()
        }
    }
    
    func converToString(str: String) -> CGFloat
    {
        let fl: CGFloat = CGFloat((str as NSString).doubleValue)
        return fl
    }
    typealias Completion = (Error?) -> Void
    
    func changePassword(email: String, currentPassword: String, newPassword: String, completion: @escaping Completion) {
        
        let user = Auth.auth().currentUser
        let credential = EmailAuthProvider.credential(withEmail: email, password: currentPassword)
        Auth.auth().currentUser?.reauthenticate(with: credential, completion: { (error) in
            if error == nil {
                user?.updatePassword(to: newPassword) { (errror) in
                    completion(errror)
                }
            } else {
                completion(error)
            }
        })
    }
}
extension Notification.Name {
    
    static let syncCompleted = Notification.Name("syncCompleted")
    
}
