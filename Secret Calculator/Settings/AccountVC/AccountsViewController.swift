//
//  AccountsViewController.swift
//  Secret Calculator
//
//  Created by Awais on 29/08/2018.
//  Copyright © 2018 Awais. All rights reserved.
//

import UIKit
import Photos
import AWSS3
import SwiftyStoreKit
import NVActivityIndicatorView
import Reachability
class AccountsViewController: UIViewController {
    let reachability = Reachability()
    var isPro = false
    var settingMenuItems = [String]()
    var settingMenuItems2 = [String]()
    var isTheDataOnCloud  = false
    @IBOutlet weak var tableViewSetting: UITableView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var progressIndicator: NVActivityIndicatorView!
    override func viewDidLoad() {
            super.viewDidLoad()
        
        let userName = userDefaults.value(forKey: "userName") as! String
        self.getTheListFromBucket(userName: userName, completion: { (objects) in
            if objects.count > 1{
                self.isTheDataOnCloud = true
                DispatchQueue.main.async {
                    self.tableViewSetting.reloadData()
                }
            }else
            {
                self.isTheDataOnCloud = false
                DispatchQueue.main.async {
                    self.tableViewSetting.reloadData()
                }
            }
        })
            // Do any additional setup after loading the view.
            settingMenuItems = ["restore purchase","delete after import","Erase data from cloud "]
            settingMenuItems2 = ["add recovery email","update to pro version"]
            tableViewSetting.delegate = self
            tableViewSetting.dataSource = self
            tableViewSetting.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
            tableViewSetting.tableFooterView = UIView()
        }
    override func viewWillAppear(_ animated: Bool) {
        isPro = userDefaults.bool(forKey: SUBSCRIPTION_PRO)
        tableViewSetting.reloadData()
    }
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
    
        @IBAction func dismissVC(_ sender: Any) {
            self.navigationController?.popViewController(animated: true)
        }
    func restorePurchase() -> () {
        SwiftyStoreKit.restorePurchases(atomically: true) { results in
            if results.restoreFailedPurchases.count > 0 {
                self.progressIndicator.stopAnimating()
                self.backButton.isUserInteractionEnabled = true
                print("Restore Failed: \(results.restoreFailedPurchases)")
            }
            else if results.restoredPurchases.count > 0 {
                self.validateRecipt()
                
            }
            else{
                Utility.showMessageWithOkButton(caller: self, title: "Error", message: "You not purchased yet!")
                self.progressIndicator.stopAnimating()
                self.backButton.isUserInteractionEnabled = true
                self.progressIndicator.stopAnimating()
                print("Nothing to Restore")
            }
        }
    }
    
    }
    //MARK: tableview Delegates
    extension AccountsViewController: UITableViewDelegate{
        func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            return 50
        }
        func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
            if(section == 0)
            {
                let header = view as! UITableViewHeaderFooterView
                header.textLabel?.text = "GENERAL"
                header.textLabel?.textColor = UIColor(red: 255/255, green: 149/255, blue: 0/255, alpha: 1)
                header.textLabel?.font = UIFont(name: "bebas", size: 14)!
                header.backgroundColor = UIColor.clear
            }
            else{
//                let header = view as! UITableViewHeaderFooterView
//                header.textLabel?.text = "ACCOUNT"
//                header.textLabel?.textColor = UIColor(red: 255/255, green: 149/255, blue: 0/255, alpha: 1)
//                header.textLabel?.font = UIFont(name: "bebas", size: 14)!
//                header.backgroundColor = UIColor.clear
            }
        }
    
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            if indexPath.row == 0 || indexPath.row == 2
            {
                return 75
            }
            else
            {
                return 99
            }
            
        }
    }
    extension AccountsViewController: UITableViewDataSource{
        func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        }
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//            if( section == 0){
                return 3
//            }
//            else
//            {
//                return 2
//            }
        }
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            if(indexPath.row == 0 && !isPro){
                if let reachable = reachability?.isReachable {
                    if(reachable){
                        progressIndicator.startAnimating()
                        backButton.isUserInteractionEnabled = false
                        restorePurchase()
                    }else
                    {
                        Utility.showErrorAlert(caller: self, message: "Could not connect to internet")
                    }
                }
            }
            if indexPath.row == 2{
                if(isTheDataOnCloud)
                {
                    Utility.showAlertWithTwoButtons(caller: self, title: "Erase all data from cloud", message: "Are you sure to erase all synced data from cloud ? This will also turn off upload to cloud", firstButtonTitle: "Erase", firstButtonStyle: .destructive, firstButtonHandler: { (alert) in
                        if let reachable = self.reachability?.isReachable {
                            if(reachable){
                                    self.progressIndicator.startAnimating()
                                    self.backButton.isUserInteractionEnabled = false
                                    self.eraseAllDataFromCloud {
                                        userDefaults.set(false, forKey: "uploadToCloud")
                                        DispatchQueue.main.async {
                                            self.progressIndicator.stopAnimating()
                                            self.backButton.isUserInteractionEnabled = true
                                            Utility.showAlert(caller: self, title: "Congratulation", message: "All the data erased from cloud sucessfully")
                                    }
                                }
                            }
                            else
                            {
                                Utility.showErrorAlert(caller: self, message: "Could not connect to internet")
                            }
                        }
                        
                    }, secondButtonTitle: "Cancel", secondButtonStyle: .cancel, secondButtonHandler: nil)
                }
            }
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! AccountTableViewCell
            if(indexPath.section == 0)
            {
                if(indexPath.row == 0){
                    cell.cellImageView.isHidden = true
                    cell.lableMessage.isHidden = true
                }
                if(indexPath.row == 1)
                {
                    cell.cellSwitch.isHidden = false
                   if( UserDefaults.standard.bool(forKey: "deleteAfterMove"))
                   {
                        cell.cellSwitch.isOn = true
                    }
                   else{
                        cell.cellSwitch.isOn = false
                    }
                    cell.cellSwitch.onTintColor = UIColor(red: 255/255, green: 157/255, blue: 0, alpha: 1)
                    cell.cellSwitch.addTarget(self, action: #selector(self.switchChanged(_:)), for: .valueChanged)
                }
                if indexPath.row == 2{
                    if(!isTheDataOnCloud){
                        cell.labelCell.textColor = UIColor.gray
                    }else
                    {
                        cell.labelCell.textColor = UIColor.black
                    }
                    cell.cellImageView.isHidden = true
                    cell.lableMessage.isHidden = true
                   
                }
                if(indexPath.row == 0 && isPro){
                    cell.labelCell.text = "SUBSCRIBED"
                    cell.labelCell.textColor = UIColor.gray
                }else{
                    cell.labelCell.text = settingMenuItems[indexPath.row].uppercased()
                }
            }
//            else if(indexPath.section == 1)
//            {
//                cell.labelCell.text = settingMenuItems2[indexPath.row].uppercased()
//                cell.cellImageView.isHidden = false
//            }
            cell.selectionStyle = .none
            return cell
        }
    @objc func switchChanged(_ sender : UISwitch!){
        if(sender.isOn)
        {
            Utility.showAlertWithTwoButtons(caller: self, title: "Delete after import", message: "Image or video will be deleted from camera role after importing to application", firstButtonTitle: "Turn On", firstButtonStyle: UIAlertActionStyle.default, firstButtonHandler: { (alert) in
                    sender.isOn = true
                    UserDefaults.standard.set(true, forKey: "deleteAfterMove")
            }, secondButtonTitle: "Cancel", secondButtonStyle: .cancel, secondButtonHandler: {(alert) in
                sender.isOn = false
                UserDefaults.standard.set(false, forKey: "deleteAfterMove")
            })
        }
        else
        {
            Utility.showAlertWithTwoButtons(caller: self, title: "Delete after import", message: "Image or video will not be deleted from camera role after importing to application", firstButtonTitle: "Turn Off", firstButtonStyle: UIAlertActionStyle.default, firstButtonHandler: { (alert) in
                sender.isOn = false
                UserDefaults.standard.set(false, forKey: "deleteAfterMove")
            }, secondButtonTitle: "Cancel", secondButtonStyle: .cancel, secondButtonHandler: {(alert) in
                sender.isOn = true
                UserDefaults.standard.set(true, forKey: "deleteAfterMove")
            })
        }
    }
}
extension AccountsViewController{
    func validateRecipt()->Void{
        let appleValidator = AppleReceiptValidator(service: .production, sharedSecret: shareedSecret)
        SwiftyStoreKit.verifyReceipt(using: appleValidator, forceRefresh: true) { (result) in
            switch result {
            case .success(let receipt):
                
                let productIds = Set([ tenGB,
                                       twentyFiveGB,hundredGB,twoFiftyGB])
                let purchaseResult = SwiftyStoreKit.verifySubscriptions(productIds: productIds, inReceipt: receipt)
                
                switch purchaseResult {
                case .purchased(let expiryDate, let items):
                  //  print("\(productIds) is valid until \(expiryDate)\n\(items)\n")
                 
                            print(items[0].productId," is not expired")
                            userDefaults.set(PURCHASED, forKey: "isAllowedToSync")
                            userDefaults.set(items[0].productId, forKey: "subscribedCat")
                            UserDefaults.standard.set(true, forKey: SUBSCRIPTION_PRO)
                            UserDefaults.standard.synchronize()
                            self.progressIndicator.stopAnimating()
                            self.backButton.isUserInteractionEnabled = true
                            self.tableViewSetting.reloadData()
                            Utility.showMessageWithOkButton(caller: self, title: "Congratulations!", message: "Restored purchase sucessful")
                            self.viewWillAppear(true)
                            self.backButton.isUserInteractionEnabled = true
                   
                    
                case .expired(let expiryDate, let items):
                    print("\(productIds) is expired since \(expiryDate)\n\(items)\n")
                    UserDefaults.standard.set(false, forKey: SUBSCRIPTION_PRO)
                    userDefaults.set(EXPIRED, forKey: "isAllowedToSync")
                    userDefaults.set("1", forKey: "subscribedCat")
                    self.progressIndicator.stopAnimating()
                    self.backButton.isUserInteractionEnabled = true
                    
                    self.backButton.isUserInteractionEnabled = true
                    Utility.showMessageWithOkButton(caller: self, title: "Alert", message: "Your subscription has expired")
                case .notPurchased:
                    userDefaults.set(NOTPURCHASED, forKey: "isAllowedToSync")
                    print("The user has never purchased \(productIds)")
                    Utility.showMessageWithOkButton(caller: self, title: "Alert!", message: "Not purchased yet")
                    self.progressIndicator.stopAnimating()
                    self.backButton.isUserInteractionEnabled = true
                }
                
            case .error(let error):
                self.progressIndicator.stopAnimating()
                self.backButton.isUserInteractionEnabled = true
                print("Receipt verification failed: \(error)")
            }
        }
    }
}
extension AccountsViewController{
    func eraseAllDataFromCloud(callbackHandler:@escaping()->()) -> Void {
        self.progressIndicator.startAnimating()
        self.backButton.isUserInteractionEnabled = false
        let fetchedAlbums =  CoreDataManager.sharedManager.fetchAllalbums()
        if(fetchedAlbums.count>0){
            for i in 0 ..< fetchedAlbums.count{
                if let album = fetchedAlbums[i].albumName{
                    S3DownloadManager.sharedManager.deleteAllObjectsinBucket(albumName: album, callbackHandler: {
                    })
                }
                if(i == fetchedAlbums.count - 1){
                    callbackHandler()
                }
            }
        }else
        {
            Utility.showMessageWithOkButton(caller: self, title: "Error", message: "Nothig to erase from cloud", firstButtonStyle: .default) { (alert) in
                DispatchQueue.main.async {
                    self.progressIndicator.stopAnimating()
                    self.backButton.isUserInteractionEnabled = true
                }
            }
        }
    }
    func getTheListFromBucket(userName: String , completion: @escaping ([String])->()){
        var objects = [String]()
        let s3 = AWSS3.default()
        let req = AWSS3ListObjectsRequest()
        req?.bucket = "ca-ios-apps"
        req?.prefix = "ios/SecretCalculator/"+userName+"/"
        s3.listObjects(req!).continueWith { (task) -> Void in
            if (task.error != nil) {
                print("error fetching objects")
            }else{
                print(task.result?.contents?.count)
                if(task.result?.contents?.count != nil){
                    for data in (task.result?.contents)!{
                        objects.append(data.key!)
                    }
                }
                completion(objects)
            }
            
        }
    }
}

