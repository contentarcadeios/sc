//
//  AccountTableViewCell.swift
//  Secret Calculator
//
//  Created by Awais on 29/08/2018.
//  Copyright © 2018 Awais. All rights reserved.
//

import UIKit

class AccountTableViewCell: UITableViewCell {

        @IBOutlet weak var labelCell: UILabel!
        @IBOutlet weak var cellImageView: UIImageView!
    @IBOutlet weak var lableMessage: UILabel!
    
        @IBOutlet weak var cellSwitch: UISwitch!
        override func awakeFromNib() {
            super.awakeFromNib()
            // Initialization code
        }
    
        override func setSelected(_ selected: Bool, animated: Bool) {
            super.setSelected(selected, animated: animated)
    
            // Configure the view for the selected state
        }

}
