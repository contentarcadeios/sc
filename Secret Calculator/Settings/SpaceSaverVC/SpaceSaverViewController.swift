//
//  SpaceSaverViewController.swift
//  Secret Calculator
//
//  Created by Awais on 04/09/2018.
//  Copyright © 2018 Awais. All rights reserved.
//
import NVActivityIndicatorView
import UIKit
import AWSS3
import Foundation
import CircleProgressBar
class SpaceSaverViewController: UIViewController {
    @IBOutlet weak var indicatorNV: NVActivityIndicatorView!
    @IBOutlet weak var switchUpload: UISwitch!
    @IBOutlet weak var progressBarStorage: CircleProgressBar!
    @IBOutlet weak var progressBarCloud: CircleProgressBar!
    var sizeOfObjects : CGFloat = 0.0
    var fetchedAlbumsCoreData = [Album]()
    var sizeOfImages : CGFloat = 0
    var sizeOfVideos : CGFloat = 0
    var spaceSubscribed : CGFloat = 0.0
    @IBOutlet weak var labelImageSize: UILabel!
    
    @IBOutlet weak var labelVideSize: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        let space = userDefaults.value(forKey: "spaceSubscribe") as? CGFloat
        spaceSubscribed = space! * TOBYTES
        indicatorNV.color = UIColor(red: 255/255, green: 157/255, blue: 0, alpha: 1)
        // Do any additional setup after loading the view.
        switchUpload.onTintColor = UIColor(red: 255/255, green: 157/255, blue: 0, alpha: 1)
        progressBarCloud.hintViewBackgroundColor = UIColor.clear
        progressBarStorage.hintViewBackgroundColor = UIColor.clear
        progressBarCloud.progressBarWidth = 10
        progressBarStorage.progressBarWidth = 10
        progressBarStorage.setProgress(CGFloat(1), animated: false, duration: 1.5)
        progressBarCloud.setProgress(CGFloat(1), animated: false, duration: 1.5)
        let uploadToCloud = userDefaults.bool(forKey: "uploadToCloud")
        if(uploadToCloud){
            switchUpload.isOn = true
        }else
        {
            switchUpload.isOn = false
        }
      
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.countVideosAndImages()
        self.labelVideSize.text = sizeToPrettyString(size: sizeOfVideos)
        self.labelImageSize.text = sizeToPrettyString(size: sizeOfImages)
        sizeOfObjects = 0
        fetchedAlbumsCoreData = CoreDataManager.sharedManager.fetchAllalbums()
        if(fetchedAlbumsCoreData.count > 0){
            indicatorNV.startAnimating()
            var i = 0;
            for album in fetchedAlbumsCoreData{
                calculateSizeOfBucket(albumName: album.albumName!, callbackHandler: {
                 
                        DispatchQueue.main.async{
                            self.indicatorNV.stopAnimating()
                            if(i == self.fetchedAlbumsCoreData.count){
                                self.createProgressIndicator()
                                self.createProgressIndicatorForStorage()
                            }
                        }
                    })
                i = i + 1
            }
        }else{
            self.createProgressIndicator()
            self.createProgressIndicatorForStorage()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func uploadSwitchOffOn(_ sender: Any) {
        let uploadToCloud = userDefaults.bool(forKey: "uploadToCloud")
        if(uploadToCloud){
            Utility.showAlertWithTwoButtons(caller: self, title: "Upload to cloud", message: "Data will not be uploded to cloud", firstButtonTitle: "Turn Off", firstButtonStyle: .default, firstButtonHandler: { (alert) in
                userDefaults.set(false, forKey: "uploadToCloud")
                self.switchUpload.isOn = false
            }, secondButtonTitle: "Cancel", secondButtonStyle: .cancel) { (alert) in
                userDefaults.set(true, forKey: "uploadToCloud")
                self.switchUpload.isOn = true
            }
           
        }else
        {
            Utility.showAlertWithTwoButtons(caller: self, title: "Upload to cloud", message: "Data will be uploded to cloud", firstButtonTitle: "Turn On", firstButtonStyle: .default, firstButtonHandler: { (alert) in
                userDefaults.set(true, forKey: "uploadToCloud")
                self.switchUpload.isOn = true
                self.createAlbumsOnS3()
                SyncManager.sharedManager.syncData()
            }, secondButtonTitle: "Cancel", secondButtonStyle: .cancel) { (alert) in
                userDefaults.set(false, forKey: "uploadToCloud")
                self.switchUpload.isOn = false
            }
        }
    }
    @IBAction func popVc(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
extension SpaceSaverViewController{
    
    func calculateSizeOfBucket(albumName: String, callbackHandler: @escaping () -> Void) -> Void {
        let userName = userDefaults.value(forKey: "userName") as! String
        let s3 = AWSS3.default()
        let req = AWSS3ListObjectsRequest()
        req?.bucket = "ca-ios-apps"
        req?.prefix = "ios/SecretCalculator/"+userName+"/"+albumName+"/"
        s3.listObjects(req!).continueWith { (task) -> Void in
            if (task.error != nil) {
                print("error fetching objects")
            }else{
                if let data = task.result?.contents
                {
                for  i in 0 ..< data.count {
                    let firstObject = data[i]
                    if(((firstObject.key?.range(of: "jpg")) != nil) || ((firstObject.key?.range(of: "mov")) != nil))
                    {
                        self.sizeOfObjects =  self.sizeOfObjects + CGFloat(truncating: (firstObject.size)!)
                    }
                        print("size of object ",round(self.sizeOfObjects/(1024*1024)) , "MB")
                    }
                }
                callbackHandler()
            }
        }
    }
    func createProgressIndicator() -> Void {
        let fileSizeWithUnit = ByteCountFormatter.string(fromByteCount: Int64(sizeOfObjects), countStyle: .file)
        print("File Size: \(fileSizeWithUnit)")
        progressBarCloud.startAngle = -90
        let spacedUsed = (sizeOfObjects / ( 1024 * 1024 ) ) / 1024
        progressBarCloud.setProgress(spacedUsed, animated: true, duration: 1.5)
        progressBarCloud.hintViewBackgroundColor = UIColor.clear
        progressBarCloud.hintTextColor = UIColor.black
        progressBarCloud.progressBarTrackColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)
        progressBarCloud.progressBarWidth = 10
        progressBarCloud.hintTextFont = UIFont(name: "bebas", size: 13)
        progressBarCloud.hintViewSpacing = 0
        progressBarCloud.setHintTextGenerationBlock { (progress) -> String? in
            return String.init(format: "\(fileSizeWithUnit)/\(self.sizeToPrettyString(size: self.spaceSubscribed) )", arguments: [fileSizeWithUnit])
        }
    }
    func createProgressIndicatorForStorage() -> Void {
        let freeBytes = (deviceRemainingFreeSpaceInBytes()!  / ( 1024 * 1024 * 1024 ))
        let totalBytes = (deviceFullSpaceFreeSpaceInBytes()!  / ( 1024 * 1024 * 1024 ))
        let usedSpace = totalBytes - freeBytes
        let progress =  CGFloat( usedSpace ) / CGFloat( totalBytes)
        progressBarStorage.startAngle = -90
        progressBarStorage.setProgress(CGFloat(progress), animated: true, duration: 1.5)
        progressBarStorage.hintViewBackgroundColor = UIColor.clear
        progressBarStorage.hintTextColor = UIColor.black
        progressBarStorage.progressBarTrackColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)
        progressBarStorage.progressBarWidth = 10
        progressBarStorage.hintTextFont = UIFont(name: "bebas", size: 13)
        progressBarStorage.hintViewSpacing = 0
        progressBarStorage.setHintTextGenerationBlock { (progress) -> String? in
            return String.init(format: "\(usedSpace) GB/ \(totalBytes)GB", arguments: [usedSpace, totalBytes])
        }
    }
    
    func deviceRemainingFreeSpaceInBytes() -> Int64? {
        let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).last!
        guard
            let systemAttributes = try? FileManager.default.attributesOfFileSystem(forPath: documentDirectory),
            let freeSize = systemAttributes[.systemFreeSize] as? NSNumber
            else {
                // something failed
                return nil
        }
        return freeSize.int64Value
    }
    func deviceFullSpaceFreeSpaceInBytes() -> Int64? {
        let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).last!
        guard
            let systemAttributes = try? FileManager.default.attributesOfFileSystem(forPath: documentDirectory),
            let freeSize = systemAttributes[.systemSize] as? NSNumber
            else {
                // something failed
                return nil
        }
        return freeSize.int64Value
    }
    func countVideosAndImages() -> Void {
       
        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        do {
            // Get the directory contents urls (including subfolders urls)
            let directoryContents = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil, options: [])
            print(directoryContents)
            for dir in directoryContents{
                do{
                   let contents = try FileManager.default.contentsOfDirectory(at: dir, includingPropertiesForKeys: nil, options: [.skipsHiddenFiles])
                    for cont in contents{
                        if((cont.path.range(of: "jpg") != nil) || (cont.path.range(of: "mov") != nil) ){
                            if(cont.path.contains("jpg")){
                                do{
                                    let atr = try FileManager.default.attributesOfItem(atPath: cont.path)
                                   print( atr.values)
                                    self.sizeOfImages = self.sizeOfImages + CGFloat(truncating: atr[.size] as! NSNumber)
                                
                                }
                                catch{
                                    print("error")
                                }
                            }
                            else if (cont.path.contains("mov")){
                                do{
                                    let atr = try FileManager.default.attributesOfItem(atPath: cont.path)
                                    print( atr.values)
                                     self.sizeOfVideos = self.sizeOfVideos + CGFloat(truncating: atr[.size] as! NSNumber)
                                    
                                }
                                catch{
                                    print("error")
                                }
                            }
                        }
                    }
                }
                catch{
                    print(error.localizedDescription)
                }
            }
            // if you want to filter the directory contents you can do like this:
            let mp3Files = directoryContents.filter{ $0.pathExtension == "mp3" }
            print("mp3 urls:",mp3Files)
            let mp3FileNames = mp3Files.map{ $0.deletingPathExtension().lastPathComponent }
            print("mp3 list:", mp3FileNames)
            
        } catch {
            print(error.localizedDescription)
        }
    }
    private func sizeToPrettyString(size: CGFloat) -> String {
        let s = size/(1024*1024*1024)
        let folderSize = String(format: "%.2f GB", s)
        //        let byteCountFormatter = ByteCountFormatter()
        //        byteCountFormatter.allowedUnits = .useGB
        //        byteCountFormatter.countStyle = .file
        //        let folderSizeToDisplay = byteCountFormatter.string(fromByteCount: Int64(round(( Double(Int64(size))))))
        
        return folderSize
        
    }
    func createAlbumsOnS3()->Void{
        let localAlbums = CoreDataManager.sharedManager.fetchAllalbums()
        if localAlbums.count > 0 {
            for album in localAlbums{
                var folderName = "/" + album.albumName!
                let userName = userDefaults.value(forKey: "userName") as! String
                Utility.createBucketOnS3(bucketName: userName + folderName)
            }
        }
    }
}
