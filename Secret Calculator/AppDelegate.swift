//
//  AppDelegate.swift
//  Secret Calculator
//
//  Created by Awais on 06/08/2018.
//  Copyright © 2018 Awais. All rights reserved.
//

import UIKit
import CoreData
import Firebase
import AWSS3
import Reachability
import SwiftyStoreKit

enum prefer {
    case mobilePhone
    case privateCloud
}
@available(iOS 10.0, *)
enum Connection {
    case none, wifi, cellular
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate , UIGestureRecognizerDelegate {
    let reachability = Reachability()
    var currentProducts = [SKProduct]()

    var window: UIWindow?
    //timer changes
    var countdownTimer: Timer!
    var totalTime  = 60
    //timer changes
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
    
        if let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first?.path {
            print("Documents Directory: \(documentsPath)")
        }
        
        
        FirebaseApp.configure()
        // S3 Bucket ca-apps/ios/SecretCalculator
        let credentialsProvider = AWSCognitoCredentialsProvider(regionType:.USEast2,
                                                                identityPoolId:"us-east-2:fa97539a-0963-46da-bfe8-e8fecb501084")
        let configuration = AWSServiceConfiguration(region:.USEast2, credentialsProvider:credentialsProvider)
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        let launchedBefore = UserDefaults.standard.bool(forKey: "launchedBefore")
        if !launchedBefore  {
           setInitialLaunch()
            let win = DMZTouchesWindow(frame: UIScreen.main.bounds);
            win.dmz_touchesEnabled = true
            
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.window = win
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "signInVC") as! UINavigationController
            self.window?.rootViewController = initialViewController
            self.window?.makeKeyAndVisible()
            
        } else
        {
           changeInitialViewController()
        }
        let userName = userDefaults.value(forKey: "userName") as? String
        if(userName != nil){
            let backUponWifi = UserDefaults.standard.bool(forKey: "wifiBackup")
            
            let cloud_priority = userDefaults.bool(forKey: "priorityCloud")
            if(cloud_priority){
                if(backUponWifi && reachability?.connection == .wifi){
                   S3SyncManager.sharedManager.downloadAllobjectsFromS3()
                }
                else if(!backUponWifi)
                {
                    S3SyncManager.sharedManager.downloadAllobjectsFromS3()
                }
                
            }else
            {
                SyncManager.sharedManager.syncData()
            }
        }
        getPurchases()
        //In-App Purchase
        SwiftyStoreKit.completeTransactions(atomically: true) { purchases in
            for purchase in purchases {
                switch purchase.transaction.transactionState {
                case .purchased, .restored:
                if purchase.needsFinishTransaction {
                        // Deliver content from server, then:
                        SwiftyStoreKit.finishTransaction(purchase.transaction)
                }// Unlock content
                case .failed, .purchasing, .deferred:
                    break // do nothing
                }
            }
        }
        if(launchedBefore){
            SwiftyStoreKit.retrieveProductsInfo([tenGB,twentyFiveGB,hundredGB,twoFiftyGB]) { (results) in                if results.retrievedProducts.count > 0 {
                    self.currentProducts = Array(results.retrievedProducts)
                }
            }
            
        }
        totalTime = UserDefaults.standard.value(forKey: "lockTime") as! Int
        startTimer()
        return true
       
   
    }
   @objc func touched() -> () {
        print("touched")
    }
    func startTimer() {
        print("timer started")
        
        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
        print("count down timer",countdownTimer)
    }
    @objc func updateTime() {
        if totalTime != 0 {
            totalTime -= 1
            print("time Remaining",totalTime)
        } else {
            endTimer()
        }
    }
    func endTimer() {
        countdownTimer.invalidate()
        lockApplication()
        print("Time Ended")
    }
    

    func getPurchases()-> Void{
        SwiftyStoreKit.retrieveProductsInfo([tenGB,twentyFiveGB,hundredGB,twoFiftyGB]) { (results) in
            if results.retrievedProducts.count > 0 {
                self.currentProducts = Array(results.retrievedProducts)
            }
            else{
                print(results.error?.localizedDescription as! String)
            }
        }
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        // 3- home or lock detection
        let isDisplayStatusLocked = UserDefaults.standard
        if let lock = isDisplayStatusLocked.value(forKey: "isDisplayStatusLocked"){
            // user locked screen
            if(lock as! Bool){
                // do anything you want here
                let allowLock = userDefaults.bool(forKey: "allowLock")
                if(allowLock){
                   lockApplication()
                }
                print("Lock button pressed.")
            }
                // user pressed home button
            else{
                // do anything you want here
                let allowLock = userDefaults.bool(forKey: "allowLock")
                if(allowLock){
                   lockApplication()
                }
                print("Home button pressed.")
            }
        }
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        //home or lock detection
        print("Back to foreground.")
        // 4- Home and lock screen setting
        let isDisplayStatusLocked = UserDefaults.standard
        isDisplayStatusLocked.set(false, forKey: "isDisplayStatusLocked")
        isDisplayStatusLocked.synchronize()
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    // MARK: - Core Data stack
    
    @available(iOS 10.0, *)
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Gallery")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    //lock time finished lock the app
    @objc func applicationDidTimeout(notification: NSNotification) {
        lockApplication()
    }
    func lockApplication() -> Void {
        if let viewControllers = window?.rootViewController?.childViewControllers {
            if  UserDefaults.standard.bool(forKey: "allowLock"){
                for viewController in viewControllers {
            
                    if viewController.isKind(of: CalculatorViewController.self) {
                        // already locked
                    }
                    else{
                        if let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "calculatorvc") as? CalculatorViewController {
                            if let window = self.window, let rootViewController = window.rootViewController {
                                var currentController = rootViewController
                                while let presentedController = currentController.presentedViewController {
                                    currentController = presentedController
                                }
                                currentController.present(controller, animated: false, completion: {
                                    UserDefaults.standard.set(true, forKey: "isLocked")
                                })
                            }
                        }
                    }
                }
            }
        }
    }
}
extension AppDelegate{
    func setInitialLaunch()->Void{
        userDefaults.set(false, forKey: "allowLock")
        UserDefaults.standard.set(60.0, forKey: "lockTime")
        UserDefaults.standard.set(false, forKey: "deleteAfterMove")
        UserDefaults.standard.set(false, forKey: "isLocked")
        userDefaults.set(true, forKey: "uploadToCloud")
        userDefaults.set(true, forKey: "priorityCloud")
        userDefaults.set(true, forKey: "wifiBackup")
        userDefaults.set(1.0, forKey: "spaceSubscribe")
        userDefaults.set("1", forKey: "subscribedCat")
        userDefaults.set(false, forKey: "popUpShown")
        userDefaults.set(NOTPURCHASED, forKey: "isAllowedToSync")
        
    }
    func changeInitialViewController() -> Void{
        let win = DMZTouchesWindow(frame: UIScreen.main.bounds);
        win.dmz_touchesEnabled = true
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window = win
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewController = storyboard.instantiateViewController(withIdentifier: "initialVC") as! UINavigationController
        self.window?.rootViewController = initialViewController
        self.window?.makeKeyAndVisible()
    }


}
//extension AppDelegate: SKPaymentTransactionObserver {
//    
//    func paymentQueue(_ queue: SKPaymentQueue,
//                      updatedTransactions transactions: [SKPaymentTransaction]) {
//        for transaction in transactions {
//            switch transaction.transactionState {
//            case .purchasing:
//                handlePurchasingState(for: transaction, in: queue)
//            case .purchased:
//                handlePurchasedState(for: transaction, in: queue)
//            case .restored:
//                handleRestoredState(for: transaction, in: queue)
//            case .failed:
//                handleFailedState(for: transaction, in: queue)
//            case .deferred:
//                handleDeferredState(for: transaction, in: queue)
//            }
//        }
//    }
//    
//    func handlePurchasingState(for transaction: SKPaymentTransaction, in queue: SKPaymentQueue) {
//        print("User is attempting to purchase product id: \(transaction.payment.productIdentifier)")
//    }
//    
//    func handlePurchasedState(for transaction: SKPaymentTransaction, in queue: SKPaymentQueue) {
//        print("User purchased product id: \(transaction.payment.productIdentifier)")
//        
//        queue.finishTransaction(transaction)
//        SubscriptionManager.sharedManager.uploadReceipt { (success) in
//            DispatchQueue.main.async {
//                NotificationCenter.default.post(name: SubscriptionManager.purchaseSuccessfulNotification, object: nil)
//            }
//        }
//    }
//    
//    func handleRestoredState(for transaction: SKPaymentTransaction, in queue: SKPaymentQueue) {
//        print("Purchase restored for product id: \(transaction.payment.productIdentifier)")
//        queue.finishTransaction(transaction)
////        SubscriptionService.shared.uploadReceipt { (success) in
////            DispatchQueue.main.async {
////                NotificationCenter.default.post(name: SubscriptionService.restoreSuccessfulNotification, object: nil)
////            }
////        }
//    }
//    
//    func handleFailedState(for transaction: SKPaymentTransaction, in queue: SKPaymentQueue) {
//        print("Purchase failed for product id: \(transaction.payment.productIdentifier)")
//    }
//    
//    func handleDeferredState(for transaction: SKPaymentTransaction, in queue: SKPaymentQueue) {
//        print("Purchase deferred for product id: \(transaction.payment.productIdentifier)")
//    }
//}


