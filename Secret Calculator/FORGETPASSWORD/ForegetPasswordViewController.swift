//
//  ForegetPasswordViewController.swift
//  Secret Calculator
//
//  Created by Awais on 26/09/2018.
//  Copyright © 2018 Awais. All rights reserved.
//

import UIKit
import Firebase
import NVActivityIndicatorView
import Reachability
class ForegetPasswordViewController: UIViewController {
    let reachability = Reachability()
    @IBOutlet weak var textFieldEmail: UITextField!
    
    @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldEmail.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @IBAction func sendEmail(_ sender: Any) {
        if let reachable = reachability?.isReachable{
            textFieldEmail.resignFirstResponder()
            if((textFieldEmail.text?.count)! > 0)
            {
                if(isValidEmail(testStr: textFieldEmail.text!))
                {
                    if(reachable)
                    {
                        self.activityIndicator.startAnimating()
                        Auth.auth().sendPasswordReset(withEmail: textFieldEmail.text!)
                        { error in
                            if(error == nil)
                            {
                                Utility.showMessageWithOkButton(caller: self, title: "Recover Account", message: "An email has been sent to \(String(describing: self.textFieldEmail.text!))", firstButtonStyle: UIAlertActionStyle.default, firstButtonHandler:
                                    { (alert) in
                                        self.activityIndicator.stopAnimating()
                                        self.navigationController?.popViewController(animated: true)
                                    })
                            }
                            else
                            {
                                Utility.showMessageWithOkButton(caller: self, title: "Error", message: "Please enter a valid email address")
                                self.activityIndicator.stopAnimating()
                                print(error?.localizedDescription)
                            }
                        }
                    }
                    else
                    {
                        Utility.showMessageWithOkButton(caller: self, title: "Error", message: "Could not connect to internet")
                    }
                }
                else
                {
                    Utility.showMessageWithOkButton(caller: self, title: "Error!", message: "Please enter a valid email address")
                    self.activityIndicator.stopAnimating()
                }
            }
            else
            {
                Utility.showMessageWithOkButton(caller: self, title: "Error", message: "Please enter a valid email address")
                self.activityIndicator.stopAnimating()
            }
        }
        else
        {
             Utility.showMessageWithOkButton(caller: self, title: "Error", message: "Could not connect to internet")
        }
    }
    @IBAction func popVc(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
//MARK: TextField Delegates
extension ForegetPasswordViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textFieldEmail.resignFirstResponder()
        return true
    }
}
