//
//  GalleryCollectionViewCell.swift
//  Secret Calculator
//
//  Created by Awais on 07/08/2018.
//  Copyright © 2018 Awais. All rights reserved.
//

import UIKit

class GalleryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageViewCell: UIImageView!
    var moreOptionsBtnPressed:(()->Void)?
    
    @IBOutlet weak var moreOptionBtn: UIButton!
    @IBOutlet weak var labelCell: UILabel!
    @IBAction func moreOptions(_ sender: Any) {
        if(moreOptionsBtnPressed != nil) {
            moreOptionsBtnPressed!()
        }
    }
}
