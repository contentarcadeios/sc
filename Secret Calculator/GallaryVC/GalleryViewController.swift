//
//  GalleryViewController.swift
//  Secret Calculator
//
//  Created by Awais on 07/08/2018.
//  Copyright © 2018 Awais. All rights reserved.
//

import UIKit
import AWSS3
import XLActionController
import NVActivityIndicatorView
import Reachability
class GalleryViewController: UIViewController {
    @IBOutlet weak var collectionViewGallery: UICollectionView!
    var fetchedAlbum : [Album]?
    var thumbNails  = [UIImage]()
    var listOfObjectsAtBucket = [String]()
    var albumsOnBucket = [String]()
    var objectNamesOnDevice = [String]()
    @IBOutlet weak var indicatorNV: NVActivityIndicatorView!
    @IBOutlet weak var syncIndicator: NVActivityIndicatorView!
    let reachability = Reachability()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionViewGallery.delegate = self
        collectionViewGallery.dataSource = self
        syncIndicator.color = UIColor(red: 255/255, green: 157/255, blue: 0, alpha: 1)
        indicatorNV.color =  UIColor(red: 255/255, green: 157/255, blue: 0, alpha: 1)
        notifCenter.addObserver(self, selector: #selector(GalleryViewController.syncStarted), name: .syncStarted, object: nil)
        notifCenter.addObserver(self, selector: #selector(GalleryViewController.syncCompleted), name: .syncCompleted, object: nil)
        notifCenter.addObserver(self, selector: #selector(GalleryViewController.refreshView), name: .downloadImageCompleted, object: nil)
        notifCenter.addObserver(self, selector: #selector(GalleryViewController.refreshView), name: .updateCollectionView, object: nil)
        let isSyncing = userDefaults.bool(forKey: "isSyncing")
        if (isSyncing){
                self.syncIndicator.startAnimating()
        }
        if(reachability?.connection != .wifi){
            if(userDefaults.bool(forKey: "priorityCloud")){
                if !userDefaults.bool(forKey: "popUpShown"){
                    Utility.showAlertWithTwoButtons(caller: self, title: "Alert", message: "Please connect to Wifi to sync data or turn off backup on wifi", firstButtonTitle: "Open Settings", firstButtonStyle: .default, firstButtonHandler: { (alert) in
                         userDefaults.set(true, forKey: "popUpShown")
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "privatecloudvc") as! PrivateCloudViewController
                        self.navigationController?.pushViewController(vc, animated: true)
                    }, secondButtonTitle: "Cancel", secondButtonStyle: .cancel, secondButtonHandler: {(alert) in
                        userDefaults.set(true, forKey: "popUpShown")
                    })
                }
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        let isSyncing = userDefaults.bool(forKey: "isSyncing")
        if (!isSyncing){
           
                self.syncIndicator.stopAnimating()
            
        }else
        {
                self.syncIndicator.startAnimating()
        }
        getFromDocument()

    }
    func getFromDocument(){
        listOfObjectsAtBucket.removeAll()
        objectNamesOnDevice.removeAll()
        fetchedAlbum?.removeAll()
        thumbNails.removeAll()
        fetchedAlbum =  CoreDataManager.sharedManager.fetchAllalbums()
        for album in fetchedAlbum!{
            let tNail = fetchThumbNailOfAllAlbums(path: album.albumName! + "/thumbNail")
            thumbNails.append(tNail)
        }
        DispatchQueue.main.async {
            self.collectionViewGallery.reloadData()
        }
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func lockGallery(_ sender: Any) {
        userDefaults.set(true, forKey: "fromgvc")
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func openSettings(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "settingsvc") as! SettingsViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }

}
extension GalleryViewController : UICollectionViewDelegate
{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(!checkAllowedToSyncData()){
            return
        }
        if(indexPath.row == fetchedAlbum?.count){
            let vc = storyboard?.instantiateViewController(withIdentifier: "createalbumvc")
            self.present(vc!, animated: true, completion: nil)
        }else
        {
            let vc = ExploreAlbumViewController.exploreAlbum()
            let itm = fetchedAlbum![indexPath.row]
            vc.selectedAlbum = itm.albumName
            vc.numberOfAlbumsOnDevice = (fetchedAlbum?.count)!
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
}
extension GalleryViewController : UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        let c = (fetchedAlbum?.count)! + 1
        return c
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(fetchedAlbum?.count == indexPath.row )
        {
            let cell = collectionViewGallery.dequeueReusableCell(withReuseIdentifier: "cellAdd", for: indexPath) as! GalleryCollectionViewCell
            return cell
        }
        else{
            let cell = collectionViewGallery.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! GalleryCollectionViewCell
            let indx = indexPath.row
            let fAlbum = fetchedAlbum![indx]
            cell.labelCell.text = fAlbum.albumName?.uppercased()
            cell.moreOptionBtn.isHidden = false
            cell.imageViewCell.image = thumbNails[indexPath.row]
            cell.moreOptionsBtnPressed={
                self.showMoreOptions(SelectedIndex: indexPath.row)
            }
            return cell
        }
    }
    func showMoreOptions(SelectedIndex: Int) -> Void {
        print("more Options Pressed")
        let actionSheet = SkypeActionController()
        actionSheet.addAction(Action("Delete Album", style: .default, handler: { action in
            Utility.showAlertWithTwoButtons(caller: self, title: "Delete", message: "Are you sure to delete all the contents in this folder? this will also be deleted from cloud ",firstButtonTitle:"Cancel",firstButtonStyle: .default,firstButtonHandler: {
                (action) in
            }, secondButtonTitle: "Delete",secondButtonStyle: .destructive,secondButtonHandler: {
                (action) in
                    let album = self.fetchedAlbum![SelectedIndex].albumName!
                self.indicatorNV.startAnimating()
                    S3DownloadManager.sharedManager.deleteAllObjectsinBucket(albumName: album, callbackHandler: {
                        DispatchQueue.main.async {
                            self.indicatorNV.stopAnimating()
                            self.deleteFromGalary(albumName: album)
                            self.viewWillAppear(true)
                        }
                    })
            })
        }))
        actionSheet.addAction(Action("Cancel", style: .cancel, handler: nil))
        present(actionSheet, animated: true, completion: nil)
    }
    
}
//MARK: - UICollectionViewDelegateFlowLayout
extension GalleryViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat =  3
        let collectionViewSize = collectionView.frame.size.width / 2
        return CGSize(width: collectionViewSize - padding , height: collectionViewSize + 34)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
}



//MARK: Document Directory Methods
extension GalleryViewController{
    func fetchThumbNailOfAllAlbums(path: String) -> UIImage {
        var thumbNailImage = UIImage()
        let fileManager = FileManager.default
        let documentsURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let fullPath = documentsURL.appendingPathComponent(path)
        do {
            let fileURLs = try fileManager.contentsOfDirectory(at: fullPath, includingPropertiesForKeys: nil)
            print("file url count", fileURLs.count)
            let ThumbNAil = fileURLs.last?.path
            if(ThumbNAil != nil)
            {
                thumbNailImage = UIImage(contentsOfFile: ThumbNAil!)!
            }else
            {
                thumbNailImage = #imageLiteral(resourceName: "photo_thumbnail-1")
            }
            // process files
        } catch {
            print("Error while enumerating files \(documentsURL.path): \(error.localizedDescription)")
        }
        return thumbNailImage
    }
    func deleteFromGalary(albumName: String) -> Void {
        ProgressIndicator.showAdded(to: self.view, animated: true)
        if(DocumentManager.sharedManager.deleteDirectoryAndContents(albumToDelete: albumName)){
            if( CoreDataManager.sharedManager.deleteAlbum(albumToDelete: albumName)){
                ProgressIndicator.hide(for: self.view, animated: true)
               self.viewWillAppear(true)
            }
        }
    }
    
    @objc func refreshView()
    {
        DispatchQueue.main.async {
            self.getFromDocument()
        }
      
    }
    @objc func syncCompleted(){
        print("syncCompleted ")
        DispatchQueue.main.async{
                self.syncIndicator.stopAnimating()
                userDefaults.set(false, forKey: "isSyncing")
            
        }
      
    }
    @objc func syncStarted(){
        DispatchQueue.main.async {
            if(!self.syncIndicator.isAnimating){
                self.syncIndicator.startAnimating()
            }
        }
    }
    func checkAllowedToSyncData()-> Bool{
        var allow = true
        let spaceUsed = DocumentManager.sharedManager.countVideosAndImages()
        let space = userDefaults.value(forKey: "spaceSubscribe") as? CGFloat
        let spaceSubscribe = space! * TOBYTES
        print(spaceSubscribe)
        print(spaceUsed)
        let allowed = userDefaults.value(forKey: "isAllowedToSync") as! String
        if( allowed != nil){
            if (allowed == NOTPURCHASED && spaceUsed > TOBYTES){
                Utility.showMessageWithOkButton(caller: self, title: "Error", message: "You have consumed gifted free space. Please subscribe for further use!")
                ProgressIndicator.hide(for: self.view, animated: true)
                allow =  false
            }else if(allowed == PURCHASED && spaceUsed > spaceSubscribe){
                Utility.showMessageWithOkButton(caller: self, title: "Error", message: "You have consumed subscribed space. Please subscribe for further use!")
                ProgressIndicator.hide(for: self.view, animated: true)
                allow =  false
            }else if( allowed == EXPIRED && spaceUsed > spaceSubscribe){
                Utility.showMessageWithOkButton(caller: self, title: "Error", message: "Your subcscription has expired. Please renew your subscription to access your data")
                ProgressIndicator.hide(for: self.view, animated: true)
                allow = false
            }
            else if( allowed == EXPIRED ){
                
                Utility.showAlertWithTwoButtons(caller: self, title: "Error", message: "Your subcscription has expired. Please renew your subscription to access your data", firstButtonTitle: "Subscribe", firstButtonStyle: .default, firstButtonHandler: { (alert) in
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "premiumvc") as! PremiumViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                }, secondButtonTitle: "Cancel", secondButtonStyle: .cancel, secondButtonHandler: {(alert) in
                  
                })
                
                allow = false
            }else{
                allow = true
            }
        }
        return allow
    }
}


