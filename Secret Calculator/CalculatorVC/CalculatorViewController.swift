//
//  CalculatorViewController.swift
//  Secret Calculator
//
//  Created by Awais on 07/08/2018.
//  Copyright © 2018 Awais. All rights reserved.
//

import UIKit
import AWSS3
import AWSCore

class CalculatorViewController: UIViewController,UIGestureRecognizerDelegate {
    @IBOutlet weak var textFieldCalculator: UITextField!
    @IBOutlet weak var collectionViewCalculator: UICollectionView!
    let numberOfCellsPerRow: CGFloat = 4
    @IBOutlet weak var labelSequence: UILabel!
    var firstNumber  = "firstNumber"
    var fixedNumber  = "fixedNumber"
    var secondNumber = "secondNumber"
    var answerD = 0.0
    var isUserTyping = false
    var oprand = ""
    var hasDecimalPoint = false
    var lastOprand = ""
    
    
     var userIsInTheMiddleOfTyping = false

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        collectionViewCalculator.delegate = self
        collectionViewCalculator.dataSource = self
        //Utility.addTapGuesture(view: self.view)

    
    }

    override func viewWillAppear(_ animated: Bool) {
      
        textFieldCalculator.text = "0"
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
extension CalculatorViewController: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        detectButtonPressed(buttonNumber: indexPath.row)
       // textFieldCalculator.text = textToDisplay

    }
    

}
extension CalculatorViewController: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 19
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionViewCalculator.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CalculaorCollectionViewCell
        let img  =  indexPath.row + 1
        cell.imageViewCal.image = UIImage(named:"\(img)" + ".png")
        cell.tag = indexPath.row
        cell.layoutIfNeeded()
        return cell
    }
}
extension CalculatorViewController: UICollectionViewDelegateFlowLayout {
    //MARK: - UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat =  0
        let collectionViewSize = collectionView.frame.size.width / numberOfCellsPerRow
        
        if(DeviceType.IS_IPAD){
            if(indexPath.row == 16){
                
                return CGSize(width: collectionViewSize+collectionViewSize  , height: collectionViewSize - 80)
                
            }else if ( indexPath.row == 17 || indexPath.row == 18){
                
                return CGSize(width: collectionViewSize  , height: collectionViewSize - 80)
            }
            else{
                return CGSize(width: collectionViewSize - padding , height: collectionViewSize - 80)
            }
        }else{
            
            if(indexPath.row == 16){
                
                return CGSize(width: collectionViewSize+collectionViewSize  , height: collectionViewSize - 8)
            }else if ( indexPath.row == 17 || indexPath.row == 18){
                
                return CGSize(width: collectionViewSize  , height: collectionViewSize - 8)
            }
            else{
                return CGSize(width: collectionViewSize - padding , height: collectionViewSize - 8)
            }
        }
        
       
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
}
extension CalculatorViewController{
    func detectButtonPressed(buttonNumber: Int) -> Void {
        switch buttonNumber {
        case 4:
            touchDigit("7")
            break
        case 5:
            touchDigit("8")
            break
        case 6:
            touchDigit("9")
            break
        case 8:
            touchDigit("4")
            break
        case 9:
             touchDigit("5")
            break
        case 10:
             touchDigit("6")
            break
        case 12:
            touchDigit("1")
            break
        case 13:
            touchDigit("2")
            break
        case 14:
            touchDigit("3")
            break
        case 16:
            touchDigit("0")
            break
        case 17:
            addDecimal()
            break
        case 3:// Division
            performOperation("/")
            break
        case 7:// Multiply
           performOperation("*")
            
            break
        case 11://Subtraction
            if(oprand != "")
            {
                fixedNumber = textFieldCalculator.text!
            }
            performOperation("-")
            break
        case 15://Addition
            if(oprand != "")
            {
                fixedNumber = textFieldCalculator.text!
            }
            performOperation("+")
            break
        case 18:
            performOperation("=")
            break
        case 0:
            clearDisplay()
            break
        case 1:
            addOrRemoveMinus()
            break
        case 2:
            openGallery()
            break
        default: break
            
        }
      
    }
    func addDecimal(){
        if !hasDecimalPoint{
            let textInDipslay = textFieldCalculator.text!
            if isUserTyping {
                textFieldCalculator.text = textInDipslay + "."
            } else {
                textFieldCalculator.text = "0."
                isUserTyping = true
                hasDecimalPoint = true
            }
        }
    }
    func clearDisplay() {
        textFieldCalculator.text = String(0)
        isUserTyping = false
        hasDecimalPoint = false
        firstNumber = "firstNumber"
        secondNumber = "secondNumber"
        fixedNumber = "fixedNumber"
        oprand = ""
        answerD = 0
        lastOprand = ""
    }

    func openGallery() -> Void{
        let islocked = UserDefaults.standard.bool(forKey: "isLocked")
        let pass  = UserDefaults.standard.value(forKey: "password") as? String
        if(textFieldCalculator.text == pass)
        {
            if(islocked)
            {
                if(!appDelegate.countdownTimer.isValid){
                    appDelegate.startTimer()
                }
                self.view.window!.rootViewController?.dismiss(animated: true, completion:nil)
                UserDefaults.standard.set(false, forKey: "isLocked")
            }
            else
            {
                if(!appDelegate.countdownTimer.isValid){
                    appDelegate.startTimer()
                }
                let vc = storyboard?.instantiateViewController(withIdentifier: "gvc") as! GalleryViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }else{
            textFieldCalculator.text = String(Double(textFieldCalculator.text!)!/100)
        }
    }
    
    func touchDigit(_ digit: String){
        if(!isUserTyping){
            textFieldCalculator.text = ""
            textFieldCalculator.text = digit
            isUserTyping = true
        }else
        {
            if textFieldCalculator.text == "0"{
                textFieldCalculator.text = ""
            }
            textFieldCalculator.text = textFieldCalculator.text! + digit
            isUserTyping = true
        }
    }
    func performOperation(_ oprationToDo: String){
        if(isUserTyping){
            if(fixedNumber != "fixedNumber" && oprand != ""){
                secondNumber = textFieldCalculator.text!
            }else{
                fixedNumber = textFieldCalculator.text!
            }
            isUserTyping = false
        }
        switch oprationToDo {
        case "+":
            if(checkIfPending(oprationToDo: oprationToDo)){
                performCalculation()
                secondNumber = "secondNumber"
                
               oprand = "+"
            }
            else
            {
                oprand = "+"
                if(secondNumber != "secondNumber"){
                    performCalculation()
                }
            }
            break
        case "-":
            if(checkIfPending(oprationToDo: oprationToDo)){
                performCalculation()
                secondNumber = "secondNumber"
                oprand = "-"
            }
            else
            {
                oprand = "-"
                if(secondNumber != "secondNumber"){
                    performCalculation()
                }
            }
            break
        case "*":
            if(checkIfPending(oprationToDo: oprationToDo)){
                performCalculation()
                secondNumber = "secondNumber"
                oprand = "*"
            }
            else
            {
                oprand = "*"
                if(secondNumber != "secondNumber"){
                    performCalculation()
                }
            }
            break
        case "/":
            if(checkIfPending(oprationToDo: oprationToDo)){
                performCalculation()
                secondNumber = "secondNumber"
                oprand = "/"
            }
            else
            {
                oprand = "/"
                if(secondNumber != "secondNumber"){
                    performCalculation()
                }
            }
            break
        case "=":
            performCalculation()
    
            //secondNumber = "secondNumber"
       
            break
        default:
            break
        }
        
    }
    func performCalculation(){
        if(fixedNumber == "fixedNumber")
        {
            fixedNumber = textFieldCalculator.text!
        }
        switch oprand{
        case "+":
            if(answerD == 0 && secondNumber == "secondNumber"){
                print("1st cond.")
                answerD = Double(textFieldCalculator.text!)! + Double(fixedNumber)!
                textFieldCalculator.text = String(answerD).removeAfterPointIfZero()
            }else if(answerD != 0 && secondNumber == "secondNumber"){
                print("2nd cond.")
                answerD = answerD + Double(fixedNumber)!
                textFieldCalculator.text = String(answerD).removeAfterPointIfZero()
            }else if( answerD == 0 && secondNumber != "secondNumber"){
                print("3rd cond.")
                answerD = Double(fixedNumber)! + Double(secondNumber)!
                fixedNumber  = secondNumber
                secondNumber = "secondNumber"
                textFieldCalculator.text = String(answerD).removeAfterPointIfZero()
            }else if( answerD != 0 && secondNumber != "secondNumber"){
                print("4th cond.")
                answerD = answerD + Double(secondNumber)!
                fixedNumber = secondNumber
                secondNumber = "secondNumber"
                textFieldCalculator.text = String(answerD).removeAfterPointIfZero()
            }
//            if(secondNumber == "secondNumber"){
//                if(fixedNumber == "fixedNumber"){
//                    fixedNumber = textFieldCalculator.text!
//                }
//                answerD = answerD + Double(fixedNumber)!
//                textFieldCalculator.text = String(answerD).removeAfterPointIfZero()
//
//            }else
//            {
//                fixedNumber = String(Double(fixedNumber)! + Double(secondNumber)!).removeAfterPointIfZero()
//                answerD = Double(fixedNumber)!
//                textFieldCalculator.text = fixedNumber
//
//            }
            break
        case "-":
            if(answerD == 0 && secondNumber == "secondNumber"){
                print("1st cond.")
                answerD = Double(textFieldCalculator.text!)! - Double(fixedNumber)!
                textFieldCalculator.text = String(answerD).removeAfterPointIfZero()
            }else if(answerD != 0 && secondNumber == "secondNumber"){
                print("2nd cond.")
                answerD = answerD - Double(fixedNumber)!
                textFieldCalculator.text = String(answerD).removeAfterPointIfZero()
            }else if( answerD == 0 && secondNumber != "secondNumber"){
                print("3rd cond.")
                answerD = Double(fixedNumber)! - Double(secondNumber)!
                fixedNumber  = secondNumber
                secondNumber = "secondNumber"
                textFieldCalculator.text = String(answerD).removeAfterPointIfZero()
            }else if( answerD != 0 && secondNumber != "secondNumber"){
                print("4th cond.")
                answerD = answerD - Double(secondNumber)!
                fixedNumber  = secondNumber
                secondNumber = "secondNumber"
                textFieldCalculator.text = String(answerD).removeAfterPointIfZero()
            }
//            if(secondNumber == "secondNumber"){
//                if(fixedNumber == "fixedNumber"){
//                    fixedNumber = textFieldCalculator.text!
//                }
//                    answerD = answerD - Double(fixedNumber)!
//
//                //textFieldCalculator.text = String(answerD).removeAfterPointIfZero()
//                textFieldCalculator.text = String(answerD).removeAfterPointIfZero()
//
//
//            }else
//            {
//                fixedNumber = String(Double(fixedNumber)! - Double(secondNumber)!).removeAfterPointIfZero()
//                answerD = Double(fixedNumber)!
//                textFieldCalculator.text = fixedNumber
//
//            }
            break
        case "*":
//            if(secondNumber == "secondNumber"){
//                if(fixedNumber == "fixedNumber"){
//                    fixedNumber = textFieldCalculator.text!
//                }
//                if(answerD != 0 ){
//                    answerD = answerD * Double(fixedNumber)!
//                    textFieldCalculator.text = String(answerD).removeAfterPointIfZero()
//                }else
//                {
//                    textFieldCalculator.text = String(Double(textFieldCalculator.text!)! * Double(fixedNumber)!).removeAfterPointIfZero()
//                    answerD = Double(textFieldCalculator.text!)!
//                }
//
//            }else
//            {
//                answerD = Double(fixedNumber)! * Double(secondNumber)!
//             //   fixedNumber = String(Double(fixedNumber)! * Double(secondNumber)!).removeAfterPointIfZero()
//                textFieldCalculator.text = String(answerD).removeAfterPointIfZero()
//                fixedNumber = secondNumber
//
//            }
            if(answerD == 0 && secondNumber == "secondNumber"){
                print("1st cond.")
                answerD = Double(textFieldCalculator.text!)! * Double(fixedNumber)!
                textFieldCalculator.text = String(answerD).removeAfterPointIfZero()
            }else if(answerD != 0 && secondNumber == "secondNumber"){
                print("2nd cond.")
                answerD = answerD * Double(fixedNumber)!
                textFieldCalculator.text = String(answerD).removeAfterPointIfZero()
            }else if( answerD == 0 && secondNumber != "secondNumber"){
                print("3rd cond.")
                answerD = Double(fixedNumber)! * Double(secondNumber)!
                fixedNumber  = secondNumber
                secondNumber = "secondNumber"
                textFieldCalculator.text = String(answerD).removeAfterPointIfZero()
            }else if( answerD != 0 && secondNumber != "secondNumber"){
                print("4th cond.")
                answerD = answerD * Double(secondNumber)!
                fixedNumber  = secondNumber
                secondNumber = "secondNumber"
                textFieldCalculator.text = String(answerD).removeAfterPointIfZero()
            }
            break
        case "/":
//            if(secondNumber == "secondNumber"){
//                if(fixedNumber == "fixedNumber"){
//                    fixedNumber = textFieldCalculator.text!
//                }
////                if(answerD == 0.0){
////                    textFieldCalculator.text = String(Double(textFieldCalculator.text!)! / Double(fixedNumber)!).removeAfterPointIfZero()
////                }else{
//                textFieldCalculator.text = String(Double(textFieldCalculator.text!)! / Double(fixedNumber)!).removeAfterPointIfZero()
////                }
//
//            }else
//            {
//                textFieldCalculator.text = String(Double(fixedNumber)! / Double(textFieldCalculator.text!)!).removeAfterPointIfZero()
//
//            }
            if(answerD == 0 && secondNumber == "secondNumber"){
                print("1st cond.")
                answerD = Double(textFieldCalculator.text!)! / Double(fixedNumber)!
                textFieldCalculator.text = String(answerD).removeAfterPointIfZero()
            }else if(answerD != 0 && secondNumber == "secondNumber"){
                print("2nd cond.")
                answerD = answerD / Double(fixedNumber)!
                textFieldCalculator.text = String(answerD).removeAfterPointIfZero()
            }else if( answerD == 0 && secondNumber != "secondNumber"){
                print("3rd cond.")
                answerD = Double(fixedNumber)! / Double(secondNumber)!
                fixedNumber  = secondNumber
                secondNumber = "secondNumber"
                textFieldCalculator.text = String(answerD).removeAfterPointIfZero()
            }else if( answerD != 0 && secondNumber != "secondNumber"){
                print("4th cond.")
                answerD = answerD / Double(secondNumber)!
                fixedNumber  = secondNumber
                secondNumber = "secondNumber"
                textFieldCalculator.text = String(answerD).removeAfterPointIfZero()
            }
            break
            
        default:
            break
        }
        
    }
    func checkIfPending(oprationToDo: String )->Bool{
        if(fixedNumber != "fixedNumber" && secondNumber != "secondNumber" && oprand != oprationToDo ){
            return true
        }else
        {
            return false
        }
    }
    func addOrRemoveMinus()->Void{
        if((textFieldCalculator.text?.contains("-"))!){
            if(answerD == Double(textFieldCalculator.text!)! ){
                answerD = answerD * -1
            }else if( fixedNumber == textFieldCalculator.text){
                fixedNumber = String(Double(fixedNumber)! * -1)
            }else if(secondNumber == textFieldCalculator.text){
                secondNumber = String(Double(secondNumber)! * -1)
            }
            textFieldCalculator.text = String(Double(textFieldCalculator.text!)! * -1).removeAfterPointIfZero()
        }
        else{
            if(answerD == Double(textFieldCalculator.text!)! ){
                answerD = 0 - answerD
            }else if( fixedNumber == textFieldCalculator.text){
                fixedNumber = String(0 - Double(fixedNumber)!)
            }else if(secondNumber == textFieldCalculator.text){
                secondNumber = String(0 - Double(secondNumber)!)
            }
            textFieldCalculator.text = String(0 - Double(textFieldCalculator.text!)!).removeAfterPointIfZero()
        }
    }
    
}

