//
//  ExploreAlbumCollectionViewCell.swift
//  Secret Calculator
//
//  Created by Awais on 09/08/2018.
//  Copyright © 2018 Awais. All rights reserved.
//

import UIKit

class ExploreAlbumCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var cellImageView: UIImageView!
    
    @IBOutlet weak var SelectedCellView: UIView!
    
    
    override var isSelected: Bool{
        didSet{
            if self.isSelected
            {
                //This block will be executed whenever the cell’s selection state is set to true (i.e For the selected cell)
                SelectedCellView.isHidden = false
                print("cell is xxxxxxx")
            }
            else
            {
                //This block will be executed whenever the cell’s selection state is set to false (i.e For the rest of the cells)
                print("cell is not xxxxxxx")
                 SelectedCellView.isHidden = true
            }
        }
    }
}
