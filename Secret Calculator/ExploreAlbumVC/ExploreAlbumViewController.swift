//
//  ExploreAlbumViewController.swift
//  Secret Calculator
//
//  Created by Awais on 08/08/2018.
//  Copyright © 2018 Awais. All rights reserved.
//

import UIKit
import Photos
import AVFoundation
import CircleProgressBar
import GoogleMobileAds

class ExploreAlbumViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, moveViewControllerDelegate, UIDocumentBrowserViewControllerDelegate{
    func isFromMoveVC(wantToMove: Bool) {
        self.wantToMove = wantToMove
    }
    

    
    
    @IBOutlet weak var transferMessage: UILabel!
    @IBOutlet weak var selectAllButton: UIButton!
    @IBOutlet weak var progressIndicatorView: UIView!
    @IBOutlet weak var completeDone: UIImageView!
    @IBOutlet weak var dismissOptionButton: UIButton!
    @IBOutlet weak var confirmationView: UIView!
    @IBOutlet weak var topView: UIView!
    var menuButton: ExpandingMenuButton!
    @IBOutlet weak var bottomOptionView: UIView!
    @IBOutlet weak var headerLabel: UILabel!
    var interstitial: GADInterstitial!
    @IBOutlet weak var emptyBackView: UIView!
    @IBOutlet weak var collectionViewAlbum: UICollectionView!
    var selectedAlbum: String?
    var numberOfAlbumsOnDevice = 0
    var optionView : UIView? = nil
    var imagePicker: UIImagePickerController!
    var savedImages = [String]()
    var savedImagesNames = [String]()
    var imagePath : String?
    var multiSelectionEnabled : Bool = false
    var selectedIndexes = [Int]()
    var isAllSelected : Bool = false
    var destinationAlbum : String?
    @IBOutlet weak var editButton: UIButton!
    var image: UIImage!
    var assetCollection: PHAssetCollection!
    var albumFound : Bool = false
    var photosAsset = PHFetchResult<AnyObject>()
    var assetThumbnailSize:CGSize!
    var collection: PHAssetCollection!
    var assetCollectionPlaceholder: PHObjectPlaceholder!
    var wantToMove: Bool = false
    @IBOutlet weak var circleProgressBar: CircleProgressBar!
    var currentIndex : Int = -1
    class func exploreAlbum() -> ExploreAlbumViewController {
    return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "explorealbum") as! ExploreAlbumViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        headerLabel.text = selectedAlbum
        topHeaderLabel.text = selectedAlbum
        self.createMenuButtons()
        collectionViewAlbum.delegate = self
        collectionViewAlbum.dataSource = self
        collectionViewAlbum.allowsMultipleSelection = true
        confirmationView.backgroundColor = UIColor.white.withAlphaComponent(0.9)
    //  progressIndicatorView.backgroundColor = UIColor.white.withAlphaComponent(0.9)
        interstitial = GADInterstitial(adUnitID: adInterstital)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        let isPro = userDefaults.bool(forKey: SUBSCRIPTION_PRO)
        if(!isPro){
          //  createInterstitialAd()
        }
        if(self.wantToMove)
        {
            createProgressIndicator()
            confirmationView.isHidden = false
            progressIndicatorView.isHidden = false
            moveImagesToOtherAlbum(destinationAlbum: destinationAlbum!, complitionHandler: {
                SyncManager.sharedManager.syncData()
            })
            self.wantToMove = false

        }
        reloadViewAlbum()
    }
    func albumSelected(selectedAlbum: String) {
        self.destinationAlbum = selectedAlbum
    }
    func reloadViewAlbum() {
        savedImages = listFilesFromDocumentsFolder()
        if(progressIndicatorView.isHidden == true){
            if(savedImages.count == 0 || savedImages.count == nil){
                emptyBackView.isHidden = false
            }
            else{
                emptyBackView.isHidden = true
            }
        }
        print(savedImages.count)
        collectionViewAlbum.reloadData()
        
    }
    override func viewWillDisappear(_ animated: Bool) {
      
    }
    
    //MARK: Camera Delegates
    func openCamera() {
       multiSelectionEnabled = false
        imagePicker =  UIImagePickerController()
        imagePicker.mediaTypes = ["public.movie","public.image"]
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        present(imagePicker, animated: true, completion: nil)
        
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        DispatchQueue.main.async {
            ProgressIndicator.showHUDAddedTo(self.view)
        }
        dismiss(animated: true, completion: {
            if(!self.checkAllowedToSyncData()){
                
                return
            }
        let mediaType = info[UIImagePickerControllerMediaType] as? String
        if(mediaType == "public.movie")
        {
            let videoURL = info[UIImagePickerControllerMediaURL] as? URL as NSURL?
            let myVideoVarData = try! Data(contentsOf: videoURL! as URL)
            let sucess = DocumentManager.sharedManager.saveMovieToDocument(albumName: self.selectedAlbum!, videoData: myVideoVarData)
            if(picker.sourceType == .savedPhotosAlbum && sucess){
                let imageUrl = info[UIImagePickerControllerReferenceURL] as! NSURL
                let imageUrls = [imageUrl]
               
                //Delete asset
                let delAfterImport = UserDefaults.standard.bool(forKey: "deleteAfterMove")
                if(delAfterImport){
                    PHPhotoLibrary.shared().performChanges( {
                        let imageAssetToDelete = PHAsset.fetchAssets(withALAssetURLs: imageUrls as [URL], options: nil)
                        
                        PHAssetChangeRequest.deleteAssets(imageAssetToDelete)
                    },
                                                            completionHandler: { success, error in
                                                                print("Finished deleting asset. %@", (success ? "Success" : error))
                                                                
                    })
                }
                DispatchQueue.main.async {
                    self.viewWillAppear(true)
                    ProgressIndicator.hideHUDForView(self.view)
                }
                
            }
            if(sucess){
                DispatchQueue.main.async {
                    self.viewWillAppear(true)
                    ProgressIndicator.hideHUDForView(self.view)
                }
                let uploadToCloud = userDefaults.bool(forKey: "uploadToCloud")
                if(uploadToCloud){
                    SyncManager.sharedManager.syncData()
                }
            }else
            {
                DispatchQueue.main.async {
                    self.viewWillAppear(true)
                    ProgressIndicator.hideHUDForView(self.view)
                }
            }
            
        }
        else{
            let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
            let img = pickedImage?.fixedOrientation()
            let savedSuccess = self.saveImageToDocument(image: img!)
            if(savedSuccess)
            {
                let uploadToCloud = userDefaults.bool(forKey: "uploadToCloud")
                if(uploadToCloud){
                   SyncManager.sharedManager.syncData()
                }
            }
            if(picker.sourceType == .savedPhotosAlbum && savedSuccess){
                let imageUrl = info[UIImagePickerControllerReferenceURL] as! NSURL
                let imageUrls = [imageUrl]
                //Delete asset
                let delAfterImport = UserDefaults.standard.bool(forKey: "deleteAfterMove")
                if(delAfterImport){
                    PHPhotoLibrary.shared().performChanges( {
                        let imageAssetToDelete = PHAsset.fetchAssets(withALAssetURLs: imageUrls as [URL], options: nil)
                    
                        PHAssetChangeRequest.deleteAssets(imageAssetToDelete)
                    },
                                                            completionHandler: { success, error in
                                                                print("Finished deleting asset. %@", (success ? "Success" : error))
                                                                
                    })
                }
           
                
            }
         
                let thumbNail = self.thumbnailForImage(image: img!)
                self.saveThumbNailToDocument(image: thumbNail!)
            DispatchQueue.main.async {
                self.viewWillAppear(true)
                ProgressIndicator.hideHUDForView(self.view)
            }
   
        }
        })
    }
    //MARK: Saved Album
    func openSavedAlbum(){
        multiSelectionEnabled = false
        imagePicker =  UIImagePickerController()
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum;
            imagePicker.mediaTypes = ["public.movie","public.image"]
            imagePicker.allowsEditing = false
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
//MARK: Save Image to Document Directory
    
    func saveImageToDocument(image: UIImage)->Bool{
        // Save image to Document directory
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] .appending("/" + selectedAlbum!)
        imagePath =  NSDate().description
        imagePath = imagePath?.replacingOccurrences(of: " ", with: "")
        imagePath = documentsPath.appending(("/\(selectedAlbum!)\(imagePath!).jpg"))
        let data = UIImagePNGRepresentation(image)
        let success = FileManager.default.createFile(atPath: imagePath!, contents: data, attributes: nil)
        if(success)
        {
//            let s = imagePath?.components(separatedBy: "/")
//            let nameOfImage = s!.last
//                        let path = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("\(selectedAlbum!)/\(nameOfImage!)")
//            let fileUrl = NSURL(fileURLWithPath: path)
//            Utility.uploadImageToS3(image: image, albumName: selectedAlbum!, imageName: nameOfImage!, fileUrl:fileUrl as URL)
//            print("saved")
//
            return true
        }
        else{
            print("problem")
            return false
        }
    }
    func saveThumbNailToDocument(image: UIImage)->Void{
        // Save image to Document directory
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] .appending("/" + selectedAlbum! + "/thumbNail")
        let s = imagePath?.components(separatedBy: "/")
        let nameOfImage = s!.last
      //  imagePath = imagePath.replacingOccurrences(of: " ", with: "")
        imagePath = documentsPath.appending(("/" + nameOfImage!))
        let imageUrl = URL.init(string: imagePath!)
        let data = UIImagePNGRepresentation(image)
        let success = FileManager.default.createFile(atPath: imagePath!, contents: data, attributes: nil)
        if(success)
        {
            let path = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("\(selectedAlbum!)/thumbNail/\(nameOfImage!)")
            
          imagePath = ""
            print("saved")
          
        }
        else{
            print("problem")
        }
    }

    func listFilesFromDocumentsFolder() -> [String] {
        var filesNames = [String]()
        let fullPath = selectedAlbum! + "/thumbNail"
        var documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        documentsUrl = documentsUrl.appendingPathComponent(fullPath, isDirectory: true)
        
        do {
            // Get the directory contents urls (including subfolders urls)
            let directoryContents = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil, options: [])
           
            
            // if you want to filter the directory contents you can do like this:
            let pngFiles = directoryContents.filter{ $0.pathExtension == "jpg" }
            print("mp3 urls:",pngFiles)
            let pngFileNames = pngFiles.map{ $0.deletingPathExtension().lastPathComponent }
            savedImagesNames = pngFileNames
            print("mp3 list:", pngFileNames)
            for url in pngFiles{
                filesNames.append(url.path)
            }
           
            
        } catch {
            print(error.localizedDescription)
        }
        return filesNames
    }

    //MARK: Create Thumbnail
     func thumbnailForImage(image: UIImage) -> UIImage? {
        let imageData = UIImagePNGRepresentation(image)!
        let options = [
            kCGImageSourceCreateThumbnailWithTransform: true,
            kCGImageSourceCreateThumbnailFromImageAlways: true,
            kCGImageSourceThumbnailMaxPixelSize: 300] as CFDictionary
        let source = CGImageSourceCreateWithData(imageData as CFData, nil)!
        let imageReference = CGImageSourceCreateThumbnailAtIndex(source, 0, options)!
        let thumbnail = UIImage(cgImage: imageReference)
        return thumbnail
    }
    

    @IBAction func buttonAction(_ sender: Any) {
        if((sender as AnyObject).tag == 1)
        {
            //createInterstitialAd()
            self.navigationController?.popViewController(animated: true)
        } else if((sender as AnyObject).tag == 2)
        {
            //MARK: EDIT BUTTON
            if(savedImages.count > 0){
                self.selectAllButton.setTitle("Select All", for: .normal)
                multiSelectionEnabled = !multiSelectionEnabled
                collectionViewAlbum.allowsMultipleSelection = true
                topView.isHidden = false
                bottomOptionView.isHidden = false
                menuButton.isHidden = true
                self.menuButton.isHidden = true
              
            }
        
        }
        else if((sender as AnyObject).tag == 3){
            
        }
     
    }
    
    @IBOutlet weak var topHeaderLabel: UILabel!
    @IBAction func selectAllImages(_ sender: Any) {
        if(sender as! UIButton).currentTitle == "Select All"
        {
            selectedIndexes.removeAll()
            for i in (0..<savedImages.count)
            {
               selectedIndexes.append(i)
            }
            print(selectedIndexes.count)
            isAllSelected = true
             ( sender as! UIButton).setTitle("Deselect All", for: UIControlState.normal)
            collectionViewAlbum.reloadData()
        }else{
            selectedIndexes.removeAll()
            ( sender as! UIButton).setTitle("Select All", for: UIControlState.normal)
            isAllSelected = false
            collectionViewAlbum.reloadData()
        }
        
    }
    @IBAction func dismissOptions(_ sender: Any) {
        self.selectAllButton.setTitle("Select All", for: .normal)
        multiSelectionEnabled = false
        collectionViewAlbum.allowsMultipleSelection = false
        topView.isHidden = true
        bottomOptionView.isHidden = true
        selectedIndexes.removeAll()
        menuButton.isHidden = false
        collectionViewAlbum.reloadData()
        
    }
    @IBAction func deleteAllSelected(_ sender: Any) {
        if(selectedIndexes.count>0){
            Utility.showAlertWithTwoButtons(caller: self, title: "Delete", message: "Are you sure to delete \(selectedIndexes.count) items?",firstButtonTitle:"Cancel",firstButtonStyle: .default,firstButtonHandler: {
                (action) in
            }, secondButtonTitle: "Delete",secondButtonStyle: .destructive,secondButtonHandler: {
                (action) in
                
                if(self.deleteAllImagesFromPath()){

                    self.resetOptions()
                    self.reloadViewAlbum()
                    self.menuButton.isHidden = false
                    self.multiSelectionEnabled = false
                    SyncManager.sharedManager.syncData()
                }
            })
        }else
        {
            Utility.showMessageWithOkButton(caller: self, title: "Error", message: "Please select photo or video before delete!")
        }
        
    }
    func resetOptions(){
        
       
        self.selectedIndexes.removeAll()
        self.savedImagesNames.removeAll()
        self.isAllSelected = false
        self.topView.isHidden = true
        self.bottomOptionView?.isHidden = true
        //self.menuButton.isHidden = false
        self.collectionViewAlbum.reloadData()
        self.collectionViewAlbum.allowsMultipleSelection = false
       
     
    }
    //MARK: MOVE TO Other Albums
    @IBAction func reStoreAllSelected(_ sender: Any) {
        if(numberOfAlbumsOnDevice != 1){
            if(selectedIndexes.count > 0){
                
                let vc = storyboard?.instantiateViewController(withIdentifier: "movevc") as! MoveViewController
                vc.delegate = self
                vc.sourceAlbum = selectedAlbum ?? ""
                self.present(vc, animated: true, completion: nil)
            }
            else{
                Utility.showMessageWithOkButton(caller: self, title: "Error", message: "Please select photo or video")
            }
        }else{
            Utility.showMessageWithOkButton(caller: self, title: "Error", message: "No other album found in this app's gallery")
        }
        
    }
    //MARK: Restore to PhotoAlbum
    @IBAction func reStoreCompleteAlbum(_ sender: Any) {
        let access = PHPhotoLibrary.authorizationStatus()
        if (access == .denied){
        
            Utility.showAlertWithTwoButtons(caller: self, title: "Error", message: "Please grant access from settings", firstButtonTitle: "Open Settings", firstButtonStyle: .default, firstButtonHandler: { (alert) in
                let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)!
                UIApplication.shared.open(settingsUrl)
            }, secondButtonTitle: "Cancel", secondButtonStyle: .cancel, secondButtonHandler: nil)
        }
        if( access == .authorized){
            DispatchQueue.main.async
                {
                    if(self.selectedIndexes.count > 0)
                    {
                        self.confirmationView.isHidden = false
                        self.menuButton.isHidden = true
                    }
                    else
                    {
                        Utility.showMessageWithOkButton(caller: self, title: "Error", message: "Please select photo or video")
                    }
            }
        }
      
        
    }
    
    func removeTheThumbNailAndObjects(thumbNailPath: String, ObjectPath: String,complitionHandler:@escaping()->()){
        do{
            if(ObjectPath.contains("%20")){
                try FileManager.default.removeItem(atPath: ObjectPath.replacingOccurrences(of: "%20", with: " "))
                try FileManager.default.removeItem(atPath: thumbNailPath.replacingOccurrences(of: "%20", with: " "))
            }else{
                try FileManager.default.removeItem(atPath: ObjectPath)
                try FileManager.default.removeItem(atPath: thumbNailPath)
            }
            DispatchQueue.main.async {
                self.viewWillAppear(true)
                self.resetOptions()
                //    ProgressIndicator.hideHUDForView(self.view)
            }
            complitionHandler()
            
        }catch{
            
        }
    }
    func setProgressIndicator(){
        circleProgressBar.progressBarTrackColor = UIColor.black
        circleProgressBar.progressBarWidth = 10
        circleProgressBar.progressBarProgressColor = UIColor(red: 255/255, green: 157/255, blue: 0/255, alpha: 1)
    }
    @IBAction func continueTransfer(_ sender: Any) {
        
        progressIndicatorView.isHidden = false
        let a = CGFloat(self.selectedIndexes.count)
        let b = self.selectedIndexes.count
        setProgressIndicator()
        circleProgressBar.setHintTextGenerationBlock { (progress) -> String? in
            if(progress == 1){
                self.completeDone.isHidden = false
                self.transferMessage.isHidden = false
                self.dismissOptionButton.isHidden = false
                self.collectionViewAlbum.allowsMultipleSelection = false
                self.viewWillAppear(true)
            }
        
            var albumToSave = self.findAlbum(albumName: self.selectedAlbum!)
            
            //IF Album is not in camera roll
            if( albumToSave == nil){
                self.createAlbum(albumName: self.selectedAlbum!) { (album) in
                    albumToSave = album
                    let incriment = CGFloat(1/a)
                    var progress = CGFloat(0.0)
                    for index in  self.selectedIndexes
                    {
                       
                        let thumbNailPath = self.savedImages[index]
                        let objectPath = self.getTheObjectURLS(path: thumbNailPath)
                        if(objectPath.contains("movie"))
                        {
                            if let fileURL = URL.init(string: objectPath)
                            {
                                
                                self.saveVideoToAlbum(album: albumToSave!, fileURL: fileURL, complition: {
                                    
                                    self.removeTheThumbNailAndObjects(thumbNailPath:thumbNailPath, ObjectPath: objectPath, complitionHandler: {
                                        progress = progress + incriment
                                        DispatchQueue.main.async {
                                            self.circleProgressBar.setProgress(progress, animated: true, duration: 2)
                                        }
                                    })
                                })
                            }
                            
                        }else
                        {
                            
                            if let imageToSave = UIImage(contentsOfFile: objectPath.replacingOccurrences(of: "%20", with: " "))
                            {
                                
                                self.saveImage(image: imageToSave, album: albumToSave!,completion: {(album) in
                                    
                                    self.removeTheThumbNailAndObjects(thumbNailPath: thumbNailPath, ObjectPath: objectPath, complitionHandler: {
                                        progress = progress + incriment
                                        DispatchQueue.main.async {
                                            self.circleProgressBar.setProgress(progress, animated: true, duration: 2)
                                        }
                                    })
                                })
                            }
                            
                        }
                    }
                }
            }
            else
            { //IF Album is already in camera Roll
                let incriment = CGFloat(1/a)
                var progress = CGFloat(0.0)
                for index in  self.selectedIndexes
                {
                    let thumbNailPath = self.savedImages[index]
                    let objectPath = self.getTheObjectURLS(path: thumbNailPath)
                 
                    if(objectPath.contains("movie"))
                    {
                        if let fileURL = URL.init(string: objectPath)
                        {
                            self.saveVideoToAlbum(album: albumToSave!, fileURL: fileURL, complition: {
                                self.removeTheThumbNailAndObjects(thumbNailPath:thumbNailPath, ObjectPath: objectPath, complitionHandler: {
                                    progress = progress + incriment
                                    DispatchQueue.main.async {
                                        self.circleProgressBar.setProgress(progress, animated: true, duration: 2)
                                    }

                                })
                            })
                        }
                    }else{
                        if let imageToSave = UIImage(contentsOfFile: objectPath.replacingOccurrences(of: "%20", with: " ")){
                            self.saveImage(image: imageToSave, album: albumToSave!,completion: {(album) in
                                self.removeTheThumbNailAndObjects(thumbNailPath: thumbNailPath, ObjectPath: objectPath, complitionHandler: {
                                    progress = progress + incriment
                                    DispatchQueue.main.async {
                                        self.circleProgressBar.setProgress(progress, animated: true, duration: 2)
                                    }
                                })
                            })
                        }
                    }
                }
            }
            return String.init(format: "%.0f / \(b)", arguments: [progress * a, b])
        }
        SyncManager.sharedManager.syncData()
    }
    func getTheObjectURLS(path:String)-> String {
        let thumbNailPath = path
        var objectPath = thumbNailPath.replacingOccurrences(of: "/thumbNail", with: "")
        if(path.contains("movie")){
            objectPath = objectPath.replacingOccurrences(of: ".jpg", with: ".mov")
        }
        if(objectPath.contains(" ")){
            objectPath = objectPath.replacingOccurrences(of: " ", with: "%20")
        }
        return objectPath
    }
    @IBAction func cancelTransfer(_ sender: Any) {
        self.reloadViewAlbum()
        self.resetOptions()
        multiSelectionEnabled = false
        confirmationView.isHidden  = true
        progressIndicatorView.isHidden = true
        circleProgressBar.setProgress(0, animated: true, duration: 0)
        completeDone.isHidden = true
        self.transferMessage.isHidden = true
        menuButton.isHidden = false
        self.viewWillAppear(true)
    }
    
}
//MARK: Collection View Delegates
extension ExploreAlbumViewController: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if(multiSelectionEnabled)
        {
            let cell = collectionView.cellForItem(at: indexPath)
            cell?.isSelected = true
            self.selectAllButton.setTitle("Deselect All", for: .normal)
            selectedIndexes.append(indexPath.row)
            
        }
        else{
                var  selectedImage = savedImages[indexPath.row]
            let imageNumber = "\(indexPath.row + 1)/" + "\(savedImages.count)"
            
            let vc = ThumbNailDetailViewController.ThumbNailDetailVC()
            selectedImage = selectedImage.replacingOccurrences(of: "/thumbNail", with: "")
            vc.selectedThumbNail = savedImages[indexPath.row]
            vc.selectedAlbum = selectedAlbum!
            vc.imageNumber = imageNumber
            vc.selectedImage = selectedImage
            vc.urlOfAllImagesInSelectedAlbum = savedImages
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if(selectedIndexes.contains(indexPath.row))
        {
            self.selectAllButton.setTitle("Select All", for: .normal)
            selectedIndexes = selectedIndexes.filter(){ $0 != indexPath.row}
        }
    }
    

}
//MARK: Collection View Data Source
extension ExploreAlbumViewController: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return savedImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let ecell = collectionViewAlbum.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? ExploreAlbumCollectionViewCell
        if(selectedIndexes.contains(indexPath.row) && multiSelectionEnabled){
            ecell?.SelectedCellView.isHidden = false
        }
    
        ecell?.cellImageView.image = UIImage(contentsOfFile: savedImages[indexPath.row])
        return ecell!
    }
}
//MARK: - UICollectionViewDelegateFlowLayout
extension ExploreAlbumViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat =  3
        let collectionViewSize = collectionView.frame.size.width / 3
        return CGSize(width: collectionViewSize - padding , height: collectionViewSize - padding)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 6.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
}
//MARK: Option Buttons
extension ExploreAlbumViewController{
    
    func createMenuButtons() -> Void{
        let screenSize: CGRect = UIScreen.main.bounds
        let menuButtonSize: CGSize = CGSize(width: 100.0, height: 100.0)
         menuButton = ExpandingMenuButton(frame: CGRect(origin: CGPoint.zero, size: menuButtonSize), image: UIImage(named: "addd")!, rotatedImage: UIImage(named: "addd_exit")!)
       // menuButton = ExpandingMenuButton(frame: CGRect(origin: CGPoint.zero, size: menuButtonSize), centerImage:#imageLiteral(resourceName: "menuButton"), centerHighlightedImage: #imageLiteral(resourceName: "menuButton"))
        menuButton.bottomViewColor = UIColor.white
        menuButton.bottomViewAlpha = 0.5
        // menuButton.expandingDirection = .bottom
        menuButton.menuTitleDirection = .right
        // menuButton.enabledFoldingAnimations = [.MenuItemMoving, .MenuItemFade, .MenuButtonRotation]
        menuButton.expandingAnimations = [.menuButtonRotate]
        menuButton.foldingAnimations = .all
        menuButton.menuItemMargin = 0.0
        menuButton.center = CGPoint(x:screenSize.width/2 , y: screenSize.height - 50)
        self.view.addSubview(menuButton)
        
        let item2 = ExpandingMenuItem(size: menuButtonSize, title: "Photos", image: #imageLiteral(resourceName: "image"), highlightedImage: #imageLiteral(resourceName: "image"), backgroundImage: nil, backgroundHighlightedImage: nil) { [weak self] () -> Void in
            if let weakSelf = self
            {
                    let photos = PHPhotoLibrary.authorizationStatus()
                    if photos == .notDetermined
                    {
                        PHPhotoLibrary.requestAuthorization({ (status)  in
                            if status == .authorized{
                                DispatchQueue.main.async {
                                  weakSelf.openSavedAlbum()
                                }
                            } else if(status == .denied) {
                                //do nothing
                            }
                        })
                    }
                    else if(photos == .authorized )
                    {
                        DispatchQueue.main.async
                            {
                            weakSelf.openSavedAlbum()
                        }
                    }else if (photos == .denied)
                    {
                        Utility.showAlertWithTwoButtons(caller: weakSelf, title: "Error", message: "Please grant access from settings", firstButtonTitle: "Open Settings", firstButtonStyle: .default, firstButtonHandler: { (alert) in
                            let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)!
                            UIApplication.shared.open(settingsUrl)
                        }, secondButtonTitle: "Cancel", secondButtonStyle: .cancel, secondButtonHandler: nil)
                    }
            
                }
            print("Photos pressed")
        }
        let item3 = ExpandingMenuItem(size: menuButtonSize, title: "Camera", image: #imageLiteral(resourceName: "camera"), highlightedImage: #imageLiteral(resourceName: "camera"), backgroundImage: nil, backgroundHighlightedImage: nil) { [weak self] () -> Void in
            if let weakself = self
            {
                weakself.openCamera()
            }
            //self.openCamera()
            print("cam pressed")
        }
        
    
        item2.titleColor = UIColor.black
        item3.titleColor = UIColor.black
        menuButton.addMenuItems([item2, item3])
        
    }

}

//MARK: Change Image Orientation
extension UIImage {
    func fixedOrientation() -> UIImage {
        if imageOrientation == .up { return self }
        
        var transform:CGAffineTransform = .identity
        switch imageOrientation {
        case .down, .downMirrored:
            transform = transform.translatedBy(x: size.width, y: size.height).rotated(by: .pi)
        case .left, .leftMirrored:
            transform = transform.translatedBy(x: size.width, y: 0).rotated(by: .pi/2)
        case .right, .rightMirrored:
            transform = transform.translatedBy(x: 0, y: size.height).rotated(by: -.pi/2)
        default: break
        }
        
        switch imageOrientation {
        case .upMirrored, .downMirrored:
            transform = transform.translatedBy(x: size.width, y: 0).scaledBy(x: -1, y: 1)
        case .leftMirrored, .rightMirrored:
            transform = transform.translatedBy(x: size.height, y: 0).scaledBy(x: -1, y: 1)
        default: break
        }
        
        let ctx = CGContext(data: nil, width: Int(size.width), height: Int(size.height),
                            bitsPerComponent: cgImage!.bitsPerComponent, bytesPerRow: 0,
                            space: cgImage!.colorSpace!, bitmapInfo: cgImage!.bitmapInfo.rawValue)!
        ctx.concatenate(transform)
        
        switch imageOrientation {
        case .left, .leftMirrored, .right, .rightMirrored:
            ctx.draw(cgImage!, in: CGRect(x: 0, y: 0, width: size.height,height: size.width))
        default:
            ctx.draw(cgImage!, in: CGRect(x: 0, y: 0, width: size.width,height: size.height))
        }
        return UIImage(cgImage: ctx.makeImage()!)
    }
}
extension ExploreAlbumViewController{
    func deleteAllImagesFromPath() -> Bool{

        if(isAllSelected){
            do{
             
                    var path = savedImages[0]
                    let imageName = savedImagesNames[0] + ".jpg"

                    path = path.replacingOccurrences(of: imageName, with: "")

                    let pathUrl = NSURL(string: path)
                    let directoryContents = try FileManager.default.contentsOfDirectory(at: pathUrl as! URL, includingPropertiesForKeys: nil, options: [])
                    let pngFiles = directoryContents.filter{ $0.pathExtension == "jpg" }
                    for url in pngFiles
                    {
                      try FileManager.default.removeItem(at: url)
                    }
                    try FileManager.default.removeItem(atPath: path)
                    path = path.replacingOccurrences(of: "/thumbNail", with: "")
                
                    let pathUrl2 = NSURL(string: path)
                    let directoryContents2 = try FileManager.default.contentsOfDirectory(at: pathUrl2! as URL, includingPropertiesForKeys: nil, options: [])
                   // if you want to filter the directory contents you can do like this:
                    let pngFiles2 = directoryContents2.filter{ $0.pathExtension == "jpg" }
                    for url in pngFiles2
                    {
                        do{
                            if(url.path.contains("movie")){
                                var vidUrl = url.path
                                vidUrl = vidUrl.replacingOccurrences(of: ".jpg", with: ".mov")
                                try FileManager.default.removeItem(atPath: vidUrl)
                            }else{
                                try FileManager.default.removeItem(atPath: url.path)
                            }
                        }
                        catch{
                            
                        }
                    }
                    createThumbNail()
                    return true
                }
               catch{
                    print(error.localizedDescription)
                    return false
            }
        }else
        {
            //MARK: Delete some selected
            
            var urlsToDelete = [String]()
            for i in selectedIndexes{
                urlsToDelete.append(savedImages[i])
            }
            do{
                for url in urlsToDelete{
                    try FileManager.default.removeItem(atPath: url)
                }
                
            }
            catch{
                
            }
            //MARK: Deleting Original Image
            var imagesToDelete = [String]()
            for url in urlsToDelete{
                imagesToDelete.append(url.replacingOccurrences(of: "/thumbNail", with: ""))
            }
            do{
                for url in imagesToDelete{
                    if(url.contains("movie")){
                        var vidUrl = url
                        vidUrl = vidUrl.replacingOccurrences(of: ".jpg", with: ".mov")
                        try FileManager.default.removeItem(atPath: vidUrl)
                    }else{
                        try FileManager.default.removeItem(atPath: url)
                    }
                    
                    
                }
                
            }
            catch{
                
            }
            let uploadToCloud = userDefaults.bool(forKey: "uploadToCloud")
            if(uploadToCloud){
                SyncManager.sharedManager.syncData()
            }
            
            return true
        }

    }
    func createThumbNail()->Void
    {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentDirectorPath:String = paths[0]
        // Get the Document directory path
        
        let  imagesDirectoryPath = documentDirectorPath.appending("/" + selectedAlbum! + "/thumbNail")
        var objcBool:ObjCBool = true
        let isExist = FileManager.default.fileExists(atPath: imagesDirectoryPath, isDirectory: &objcBool)
        // If the folder with the given path doesn't exist already, create it
        if isExist == false{
            do{
                try FileManager.default.createDirectory(atPath: imagesDirectoryPath, withIntermediateDirectories: true, attributes: nil)
            }catch{
                print("Something went wrong while creating a new folder")
            }
        }
    }
    
    //MARK: Create Album in Photos
    func createAlbum(albumName: String, completion: @escaping (PHAssetCollection?)->()){
        //Get PHFetch Options
        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "title = %@", albumName)
        let collection : PHFetchResult = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: fetchOptions)
        //Check return value - If found, then get the first album out
        if let _: AnyObject = collection.firstObject {
            self.albumFound = true
            assetCollection = collection.firstObject as! PHAssetCollection
        } else {
            //If not found - Then create a new album
            PHPhotoLibrary.shared().performChanges({
                let createAlbumRequest : PHAssetCollectionChangeRequest = PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: albumName)
                self.assetCollectionPlaceholder = createAlbumRequest.placeholderForCreatedAssetCollection
            }, completionHandler: { success, error in
                self.albumFound = success
                
                if (success) {
                    let collectionFetchResult = PHAssetCollection.fetchAssetCollections(withLocalIdentifiers: [self.assetCollectionPlaceholder.localIdentifier], options: nil)
                    print(collectionFetchResult)
                    self.assetCollection = collectionFetchResult.firstObject as! PHAssetCollection
                    completion(self.assetCollection)

                }
            })
        }
    }
    //MARK: Find Existing Album from Photos
    func findAlbum(albumName: String) -> PHAssetCollection? {
        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "title = %@", albumName)
        let fetchResult : PHFetchResult = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: fetchOptions)
        guard let photoAlbum = fetchResult.firstObject else {
            return nil
        }
        return photoAlbum
    }
    
    //MARK: SAVE IMAGE TO ALBUM
    func saveImage(image: UIImage, album: PHAssetCollection, completion:((PHAsset?)->())? = nil) {
        var placeholder: PHObjectPlaceholder?
        PHPhotoLibrary.shared().performChanges({
            let createAssetRequest = PHAssetChangeRequest.creationRequestForAsset(from: image)
            guard let albumChangeRequest = PHAssetCollectionChangeRequest(for: album),
                let photoPlaceholder = createAssetRequest.placeholderForCreatedAsset else { return }
            placeholder = photoPlaceholder
            let fastEnumeration = NSArray(array: [photoPlaceholder] as [PHObjectPlaceholder])
            albumChangeRequest.addAssets(fastEnumeration)
        }, completionHandler: { success, error in
            guard let placeholder = placeholder else {
                completion?(nil)
                return
            }
            if success {
                let assets:PHFetchResult<PHAsset> =  PHAsset.fetchAssets(withLocalIdentifiers: [placeholder.localIdentifier], options: nil)
                let asset:PHAsset? = assets.firstObject
                completion?(asset)
            } else {
                completion?(nil)
            }
        })
    }
    //MARK: Save Video to album
    func saveVideoToAlbum(album:PHAssetCollection,fileURL:URL, complition: @escaping ()->()) -> Void {
        var placeholder: PHObjectPlaceholder?
        PHPhotoLibrary.shared().performChanges({
            let createAssetRequest = PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: fileURL)
            guard let albumChangeRequest = PHAssetCollectionChangeRequest(for: album),
                        let photoPlaceholder = createAssetRequest?.placeholderForCreatedAsset else { return }
                        placeholder = photoPlaceholder
                    let fastEnumeration = NSArray(array: [photoPlaceholder] as [PHObjectPlaceholder])
                    albumChangeRequest.addAssets(fastEnumeration)
        },completionHandler: {
            (sucess, error) in
                if(sucess){
                    print("Video Saved")
                    complition()
                }else{
                    print(error?.localizedDescription)
                }
            
        })
    }
    
}
    
extension ExploreAlbumViewController{
    //MARK: Move to Other Album VC
    func moveImagesToOtherAlbum(destinationAlbum: String, complitionHandler:@escaping ()->()) -> Void {
        print("Numner of images to moves are = ",selectedIndexes.count)
        var oldPathOfThumbNailImages = [String]()
        var selectedImagesNames = [String]()
        var oldPathOfImage = [String]()
        for index in selectedIndexes{
            oldPathOfThumbNailImages.append(savedImages[index])
            var oldPath = savedImages[index]
            oldPath = oldPath.replacingOccurrences(of: "/thumbNail", with: "")
            oldPathOfImage.append(oldPath)
            selectedImagesNames.append(savedImagesNames[index] + ".jpg")
        }
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let newPathOfThumbNailImage = "/\(destinationAlbum)/thumbNail/"
        let newPathOfImage = "/\(destinationAlbum)/"
        let fullImagePath = documentsPath.appending(newPathOfImage)
        let fullThumbNailPath = documentsPath.appending(newPathOfThumbNailImage)
        //checking
        let fileManager = FileManager.default
        do {
            
            for index in 0..<selectedIndexes.count{
                try fileManager.moveItem(atPath: oldPathOfThumbNailImages[index], toPath:  fullThumbNailPath.appending(selectedImagesNames[index])) //moving thumbnail
                var imagName = selectedImagesNames[index]
                var pathOfObject = oldPathOfImage[index]
                if(imagName.contains("movie")){
                    imagName = imagName.replacingOccurrences(of: ".jpg", with: ".mov")
                    pathOfObject = pathOfObject.replacingOccurrences(of: ".jpg", with: ".mov")
                }
                try fileManager.moveItem(atPath: pathOfObject , toPath:  fullImagePath.appending(imagName)) //moving original Picture
                print("moved file")
            }
          
            reloadViewAlbum()
            collectionViewAlbum.reloadData()
            resetOptions()
        }
        catch let error as NSError {
            print("Ooops! Something went wrong: \(error)")
        }
        complitionHandler()
    }
    func createProgressIndicator() -> Void {
        let a = CGFloat(self.selectedIndexes.count)
        let b = self.selectedIndexes.count
        circleProgressBar.setProgress(1, animated: true, duration: 1.5)
        circleProgressBar.startAngle = 360
        circleProgressBar.setHintTextGenerationBlock {[weak self] (progress) -> String? in
            if(progress == 1){
                if let weakSelf = self
                {
                weakSelf.completeDone.isHidden = false
                weakSelf.transferMessage.isHidden = false
                weakSelf.dismissOptionButton.isHidden = false
              
                    
                weakSelf.collectionViewAlbum.allowsMultipleSelection = false
                }
            }
            return String.init(format: "%.0f / \(b)", arguments: [progress * a, b])
        }
    }
    func createInterstitialAd() -> () {
//
//        let request = GADRequest()
//        interstitial.load(request)
//        if interstitial.isReady {
//            interstitial.present(fromRootViewController: self)
//        } else {
//            print("Ad wasn't ready")e
        
        
//        }
    }
    
    func checkAllowedToSyncData()-> Bool{
        var allow = true
        let spaceUsed = DocumentManager.sharedManager.countVideosAndImages()
        let space = userDefaults.value(forKey: "spaceSubscribe") as? CGFloat
        let spaceSubscribe = space! * TOBYTES
        print(spaceSubscribe)
        print(spaceUsed)
        let allowed = userDefaults.value(forKey: "isAllowedToSync") as! String
        if( allowed != nil){
            if (allowed == NOTPURCHASED && spaceUsed > TOBYTES){
                ProgressIndicator.hide(for: self.view, animated: true)
                Utility.showAlertWithTwoButtons(caller: self, title: "Error", message: "You have consumed gifted free space. Please subscribe for further use!", firstButtonTitle: "Subscribe", firstButtonStyle: .default, firstButtonHandler: { (alert) in
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "premiumvc") as! PremiumViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                }, secondButtonTitle: "Cancel", secondButtonStyle: .cancel, secondButtonHandler: {(alert) in
                })
                allow =  false
            }else if(allowed == PURCHASED && spaceUsed > spaceSubscribe){
                ProgressIndicator.hide(for: self.view, animated: true)
                Utility.showAlertWithTwoButtons(caller: self, title: "Error", message: "You have consumed subscribed space. Please subscribe for further use", firstButtonTitle: "Subscribe", firstButtonStyle: .default, firstButtonHandler: { (alert) in
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "premiumvc") as! PremiumViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                }, secondButtonTitle: "Cancel", secondButtonStyle: .cancel, secondButtonHandler: {(alert) in
                })
                allow =  false
            }else if( allowed == EXPIRED ){
                ProgressIndicator.hide(for: self.view, animated: true)
                Utility.showAlertWithTwoButtons(caller: self, title: "Error", message: "Your subcscription has expired. Please renew your subscription", firstButtonTitle: "Subscribe", firstButtonStyle: .default, firstButtonHandler: { (alert) in
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "premiumvc") as! PremiumViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                }, secondButtonTitle: "Cancel", secondButtonStyle: .cancel, secondButtonHandler: {(alert) in
                    
                })

                allow = false
            }else{
                allow = true
            }
        }
        return allow
    }
}
