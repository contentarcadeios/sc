//
//  MoveViewController.swift
//  Secret Calculator
//
//  Created by Awais on 19/08/2018.
//  Copyright © 2018 Awais. All rights reserved.
//

import UIKit
protocol moveViewControllerDelegate: class {
    func albumSelected(selectedAlbum: String)
    func isFromMoveVC(wantToMove: Bool)
}
class MoveViewController: UIViewController {
 
    @IBOutlet weak var collectionViewSelection: UICollectionView!
    weak var delegate: moveViewControllerDelegate?
    var fetchedAlbum : [Album]?
    var thumbNails  = [UIImage]()
    var destinitionAlbum = String()
    var sourceAlbum = String()
    class func moveViewCont() -> MoveViewController {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "movevc") as! MoveViewController
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        collectionViewSelection.delegate = self
        collectionViewSelection.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        fetchedAlbum?.removeAll()
        thumbNails.removeAll()
        fetchedAlbum =  CoreDataManager.sharedManager.fetchAllalbums()
        var i = 0
        for album in fetchedAlbum!{
            if album.albumName == sourceAlbum{
                fetchedAlbum?.remove(at: i)
            }
            i = i + 1
            let tNail = fetchThumbNailOfAllAlbums(path: album.albumName! + "/thumbNail")
            thumbNails.append(tNail)
        }
    }
    @IBAction func dismissController(_ sender: Any) {
        self.dismiss(animated: true, completion:nil)
    }

}
extension MoveViewController : UICollectionViewDelegate
{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
            self.destinitionAlbum =  fetchedAlbum![indexPath.row].albumName!
            self.delegate?.albumSelected(selectedAlbum: destinitionAlbum)
        
            Utility.showAlertWithTwoButtons(caller: self, title: "Move", message: "Are you sure to move selected items to \(destinitionAlbum) ?",firstButtonTitle:"Cancel",firstButtonStyle: .default,firstButtonHandler: {
            (action) in
                self.delegate?.isFromMoveVC(wantToMove: false)
        }, secondButtonTitle: "Move",secondButtonStyle: .default,secondButtonHandler: {
            (action) in
            self.delegate?.isFromMoveVC(wantToMove: true)
            self.dismiss(animated: true, completion: nil)
            })
   
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
    }
}
extension MoveViewController: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (fetchedAlbum?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionViewSelection.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! MoveCollectionViewCell
        let album = fetchedAlbum![indexPath.row]
        cell.cellAlbumName.text = album.albumName?.uppercased()
        cell.imageViewThumbNAil.image = thumbNails[indexPath.row]
        return cell
    }
    
    
}
//MARK: - UICollectionViewDelegateFlowLayout
extension MoveViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat =  1
        let collectionViewSize = collectionView.frame.size.width / 2
        return CGSize(width: collectionViewSize - padding , height: collectionViewSize - padding)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
}
extension MoveViewController{
    func fetchThumbNailOfAllAlbums(path: String) -> UIImage {
        var thumbNailImage = UIImage()
        let fileManager = FileManager.default
        let documentsURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let fullPath = documentsURL.appendingPathComponent(path)
        do {
            let fileURLs = try fileManager.contentsOfDirectory(at: fullPath, includingPropertiesForKeys: nil)
            print("file url count", fileURLs.count)
            let ThumbNAil = fileURLs.last?.path
            if(ThumbNAil != nil)
            {
                thumbNailImage = UIImage(named: ThumbNAil!)!
            }else
            {
                thumbNailImage = #imageLiteral(resourceName: "photo_thumbnail-1")
            }
            // process files
        } catch {
            print("Error while enumerating files \(documentsURL.path): \(error.localizedDescription)")
        }
        return thumbNailImage
    }
}
