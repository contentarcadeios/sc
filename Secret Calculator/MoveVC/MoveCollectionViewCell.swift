//
//  MoveCollectionViewCell.swift
//  Secret Calculator
//
//  Created by Awais on 19/08/2018.
//  Copyright © 2018 Awais. All rights reserved.
//

import UIKit

class MoveCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var cellAlbumName: UILabel!
    @IBOutlet weak var imageViewThumbNAil: UIImageView!
    
    @IBOutlet weak var imageViewSelected: UIView!
    override var isSelected: Bool{
        didSet{
            if self.isSelected
            {
                //This block will be executed whenever the cell’s selection state is set to true (i.e For the selected cell)
                imageViewSelected.backgroundColor = UIColor.black.withAlphaComponent(0.7)
                imageViewSelected.isHidden = false
           
            }
            else
            {
                //This block will be executed whenever the cell’s selection state is set to false (i.e For the rest of the cells)
           
                imageViewSelected.isHidden = true
            }
        }
    }
}
