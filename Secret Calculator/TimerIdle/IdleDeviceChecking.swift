//
//  IdleDeviceChecking.swift
//  Secret Calculator
//
//  Created by Awais on 29/08/2018.
//  Copyright © 2018 Awais. All rights reserved.
//

import UIKit
import Foundation

class IdleDeviceChecking: UIApplication {
// the timeout in seconds, after which should perform custom actions
        // such as disconnecting the user
        private var timeoutInSeconds: TimeInterval {
            // 2 minutes
            let time = UserDefaults.standard.float(forKey: "lockTime") / 60
            return TimeInterval(time * 60)
        }
        
        private var idleTimer: Timer?
        
        // resent the timer because there was user interaction
        private func resetIdleTimer() {
            if let idleTimer = idleTimer {
                idleTimer.invalidate()
            }
            
            idleTimer = Timer.scheduledTimer(timeInterval: timeoutInSeconds,
                                             target: self,
                                             selector: #selector(IdleDeviceChecking.timeHasExceeded),
                                             userInfo: nil,
                                             repeats: false
            )
        }
        
        // if the timer reaches the limit as defined in timeoutInSeconds, post this notification
        @objc private func timeHasExceeded() {
           let allowLock = userDefaults.bool(forKey: "allowLock")
           if(allowLock){
                NotificationCenter.default.post(name: .appTimeout, object: nil)
                S3DownloadManager.sharedManager.postNotification()
            }
        
        }
        
        override func sendEvent(_ event: UIEvent) {
            
            super.sendEvent(event)
            
            if idleTimer != nil {
                self.resetIdleTimer()
            }
            
            if let touches = event.allTouches {
                for touch in touches where touch.phase == UITouchPhase.began {
                    self.resetIdleTimer()
                }
            }
        }
}
extension Notification.Name {
    
    static let appTimeout = Notification.Name("appTimeout")
    
}
   

