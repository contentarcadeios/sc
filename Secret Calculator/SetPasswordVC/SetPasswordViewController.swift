//
//  SetPasswordViewController.swift
//  Secret Calculator
//
//  Created by Awais on 07/08/2018.
//  Copyright © 2018 Awais. All rights reserved.
//

import UIKit
import Firebase
import AWSS3
import AWSCore
import Reachability
import NVActivityIndicatorView
class SetPasswordViewController: UIViewController {
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var collectionViewPassword: UICollectionView!
    
   
    @IBOutlet weak var labelMessage: UILabel!
    let progressBar = LinearProgressBar()
    let numberOfCellsPerRow: CGFloat = 4
    var textToDisplay = "0"
    var valueOne : CGFloat?
    var valueTwo : CGFloat?
    var ansWer : CGFloat?
    var oprationToPerform = ""
    var isOperation = true
    var password: String? = nil
    var userNaam: String? = nil
    var handle : AuthStateDidChangeListenerHandle?
    var internetReachable = Bool()
    let reachability = Reachability()!
    
    
    
    @IBOutlet weak var lineView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        collectionViewPassword.delegate = self
        collectionViewPassword.dataSource = self
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        let screenSize:CGRect = UIScreen.main.bounds
        let y = lineView.frame.origin.y
        progressBar.frame = CGRect(x: 0, y: y - 1, width: screenSize.size.width, height: 3)
        self.view.addSubview(progressBar)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        reachability.whenReachable = { reachability in
            if reachability.connection == .wifi {
                print("Reachable via WiFi")
                self.internetReachable = true
            } else {
                self.internetReachable = true
                print("Reachable via Cellular")
            }
        }
        reachability.whenUnreachable = { _ in
            self.internetReachable = false
        }
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
         handle = Auth.auth().addStateDidChangeListener { (auth, user) in
            // ...
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        Auth.auth().removeStateDidChangeListener(handle!)
    }
    
    @IBAction func popVc(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
extension SetPasswordViewController: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        detectButtonPressed(buttonNumber: indexPath.row)
        textFieldPassword.text = textToDisplay
        
    }
    
    
}
extension SetPasswordViewController: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 19
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionViewPassword.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! PasswordCollectionViewCell
        let img  =  indexPath.row + 1
        cell.imageViewCell.image = UIImage(named:"\(img)" + ".png")
        cell.layoutIfNeeded()
        return cell
    }
}
extension SetPasswordViewController: UICollectionViewDelegateFlowLayout {
    //MARK: - UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat =  0
        let collectionViewSize = collectionView.frame.size.width / 4
        if(DeviceType.IS_IPAD){
            if(indexPath.row == 16){
                
                return CGSize(width: collectionViewSize+collectionViewSize  , height: collectionViewSize - 80)
                
            }else if ( indexPath.row == 17 || indexPath.row == 18){
                
                return CGSize(width: collectionViewSize  , height: collectionViewSize - 80)
            }
            else{
                return CGSize(width: collectionViewSize - padding , height: collectionViewSize - 80)
            }
        }
        else{
            if(indexPath.row == 16){
                
                return CGSize(width: collectionViewSize+collectionViewSize  , height: collectionViewSize - 8)
            }else if ( indexPath.row == 17 || indexPath.row == 18){
                
                return CGSize(width: collectionViewSize  , height: collectionViewSize - 8)
            }
            else{
                return CGSize(width: collectionViewSize - padding , height: collectionViewSize - 8)
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
}
extension SetPasswordViewController{
    func detectButtonPressed(buttonNumber: Int) -> Void {
        switch buttonNumber {
        case 0:
            textToDisplay = "0"
            isOperation = true
            break
        case 1:
            
            if(textToDisplay != "0"){
                if (( textToDisplay.range(of: "-")) == nil)
                {
                    textToDisplay = "-" + textToDisplay
                }
                else{
                    textToDisplay = textToDisplay.replacingOccurrences(of: "-", with: "")
                }
                
            }
            break
        case 2:
            if((textFieldPassword.text?.count)! > 5){
                if(internetReachable){
                    if(password == nil){
                        password = textFieldPassword.text?.lowercased()
                        textFieldPassword.placeholder = "Confrim    Password".uppercased()
                        labelMessage.text = "Please confirm password and press".uppercased()
                        textFieldPassword.text = ""
                        textToDisplay = ""
                        return
                    }
                    if(password == textFieldPassword.text){
                        
                        userNaam = UserDefaults.standard.value(forKey: "UN") as? String
                        progressBar.startAnimating()
                        collectionViewPassword.isUserInteractionEnabled = false
                        Auth.auth().createUser(withEmail: userNaam!, password: password!) { (authResult, error) in
                            // ...
                            
                            guard let user = authResult?.user else {
                                print(error!.localizedDescription)
                                Utility.showMessageWithOkButton(caller: self, title: "Alert", message: error!.localizedDescription)
                                self.progressBar.stopAnimating()
                                self.collectionViewPassword.isUserInteractionEnabled = true
                                return }
                            if(authResult?.user != nil){
                                Auth.auth().currentUser?.sendEmailVerification { (error) in
                                    if(error == nil){
                                        self.createBucketOnS3(bucketName: self.userNaam!)
                                        DispatchQueue.main.async {
                                            self.collectionViewPassword.isUserInteractionEnabled = true
                                        }
                                        Utility.showMessageWithOkButton(caller: self, title: "Alert!", message: "An email has been sent to your account, Please verify your email address", firstButtonStyle: .default, firstButtonHandler: { (action) in
                                         
                    
                                            self.reachability.stopNotifier()
                                        })
                                       
                                    }
                                    else{
                                        Utility.showMessageWithOkButton(caller: self, title: "Alert", message: "\(String(describing: error?.localizedDescription))")
                                        DispatchQueue.main.async {
                                            self.progressBar.stopAnimating()
                                            self.collectionViewPassword.isUserInteractionEnabled = true
                                        }
                                    }
                                }
                             
                                
                            }
                            else{
                                self.collectionViewPassword.isUserInteractionEnabled = true
                                self.progressBar.stopAnimating()
                            }
                        }
                        
                  
                    }
                    if(password != textFieldPassword.text){
                        Utility.showMessageWithOkButton(caller: self, title: "Alert", message: "Password  miss match")
                        labelMessage.text = "Please enter password and press".uppercased()
                        textFieldPassword.placeholder = "set password".uppercased()
                        textToDisplay = ""
                        password = nil
                    }
                }else
                {
                    Utility.showMessageWithOkButton(caller: self, title: "Alert!", message: "please connect to internet")
                }
            }
            else{
                Utility.showMessageWithOkButton(caller: self, title: "Alert", message: "Password must be at least 6 digits long")
            }
            break
        case 3:
            if(textToDisplay != "")
            {
                isOperation = true
                oprationToPerform = "div"
                valueOne = converToString(str: textToDisplay)
                textToDisplay = "0"
            }
            break
        case 4:
            if(isOperation)
            {
                textToDisplay = ""
                isOperation = false
            }
            textToDisplay = textToDisplay + "\(7)"
            
            break
        case 5:
            if(isOperation)
            {
                textToDisplay = ""
                isOperation = false
            }
            textToDisplay = textToDisplay + "\(8)"
            
            break
        case 6:
            if(isOperation)
            {
                textToDisplay = ""
                isOperation = false
            }
            textToDisplay = textToDisplay + "\(9)"
            
            break
        case 7:
            if(textToDisplay != "")
            {
                isOperation = true
                oprationToPerform = "mul"
                valueOne = converToString(str: textToDisplay)
                textToDisplay = "0"
            }
            break
        case 8:
            if(isOperation)
            {
                textToDisplay = ""
                isOperation = false
            }
            textToDisplay = textToDisplay + "\(4)"
            
            break
        case 9:
            if(isOperation)
            {
                textToDisplay = ""
                isOperation = false
            }
            textToDisplay = textToDisplay + "\(5)"
            
            break
        case 10:
            if(isOperation)
            {
                textToDisplay = ""
                isOperation = false
            }
            textToDisplay = textToDisplay + "\(6)"
            
            break
        case 11:
            if(textToDisplay != "")
            {
                isOperation = true
                oprationToPerform = "sub"
                valueOne = converToString(str: textToDisplay)
                textToDisplay = "0"
            }
            break
        case 12:
            if(isOperation)
            {
                textToDisplay = ""
                isOperation = false
            }
            textToDisplay = textToDisplay + "\(1)"
            
            break
        case 13:
            if(isOperation)
            {
                textToDisplay = ""
                isOperation = false
            }
            textToDisplay = textToDisplay + "\(2)"
            
            break
        case 14:
            
            if(isOperation)
            {
                textToDisplay = ""
                isOperation = false
            }
            textToDisplay = textToDisplay + "\(3)"
            
            
            break
        case 15:
            isOperation = true
            oprationToPerform = "add"
            valueOne = converToString(str: textToDisplay)
            textToDisplay = "0"
            break
        case 16:
            textToDisplay = textToDisplay + "\(0)"
            break
        case 17:
            textToDisplay = textToDisplay + "."
            break
        case 18:
            if(textToDisplay != "")
            {
                valueTwo = converToString(str: textToDisplay)
                textToDisplay = "0"
                switch oprationToPerform{
                case "sub":
                    subValues()
                    break
                case "mul":
                    mulValues()
                    break
                case "add":
                    addValues()
                    break
                case "div":
                    divValues()
                    break
                default:
                    break
                }
            }
            isOperation = true
            valueOne = 0
            valueTwo = 0
            break
            
            
            
        default: break
            
        }
}
    func addValues(){
        ansWer = valueOne! + valueTwo!
        textToDisplay = "\(ansWer!)"
        if(textToDisplay.last == "0"){
            textToDisplay.removeLast()
        }
        if(textToDisplay.last == "."){
            textToDisplay.removeLast()
        }
    }
    func subValues(){
        ansWer = valueOne! - valueTwo!
        textToDisplay = "\(ansWer!)"
        if(textToDisplay.last == "0"){
            textToDisplay.removeLast()
        }
        if(textToDisplay.last == "."){
            textToDisplay.removeLast()
        }
    }
    func divValues(){
        ansWer = valueOne! / valueTwo!
        textToDisplay = "\(ansWer!)"
        if(textToDisplay.last == "0"){
            textToDisplay.removeLast()
        }
        if(textToDisplay.last == "."){
            textToDisplay.removeLast()
        }
    }
    func mulValues(){
        ansWer = valueOne! * valueTwo!
        textToDisplay = "\(ansWer!)"
        if(textToDisplay.last == "0"){
            textToDisplay.removeLast()
        }
        if(textToDisplay.last == "."){
            textToDisplay.removeLast()
        }
    }
    
    func converToString(str: String) -> CGFloat
    {
        let fl: CGFloat = CGFloat((str as NSString).doubleValue)
        return fl
    }
    //MARK: S3 bucket
    func createBucketOnS3(bucketName:String)->Void{
        let s3 = AWSS3.default()
        let req = AWSS3PutObjectRequest()
        req?.bucket = BUCKETONS3
        req?.key = bucketName + "/"
        
        s3.putObject(req!).continueWith {(task) -> Void in
            if let error = task.error {
                print(error.localizedDescription)
                
            }
            else{
//                Utility.showMessageWithOkButton(caller: self, title: "Congratulations", message: "Password has been set", firstButtonStyle: .default , firstButtonHandler:  { (action) in
//                    UserDefaults.standard.set(true, forKey: "launchedBefore")
//                    UserDefaults.standard.set(self.password, forKey: "password")
//                    UserDefaults.standard.set(self.userNaam, forKey: "userName")
                DispatchQueue.main.async {
                    
                    self.collectionViewPassword.isUserInteractionEnabled = true
                    self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
                }
                
                
                
   //             })
            }
            
        }
        
    }

}
