//
//  Album+CoreDataProperties.swift
//  Secret Calculator
//
//  Created by Awais on 08/08/2018.
//  Copyright © 2018 Awais. All rights reserved.
//
//

import Foundation
import CoreData


extension Album {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Album> {
        return NSFetchRequest<Album>(entityName: "Album")
    }

    @NSManaged public var albumName: String?

}
