//
//  CreateAlbumViewController.swift
//  Secret Calculator
//
//  Created by Awais on 08/08/2018.
//  Copyright © 2018 Awais. All rights reserved.
//

import UIKit
import GoogleMobileAds
class CreateAlbumViewController: UIViewController {
    @IBOutlet weak var textFieldAlbumName: UITextField!
    let limitCount = 25
     var interstitial: GADInterstitial!
    override func viewDidLoad() {
        super.viewDidLoad()
         interstitial = GADInterstitial(adUnitID: adInterstital)
        textFieldAlbumName.delegate = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func buttonAction(_ sender: Any) {
        if((sender as AnyObject).tag == 1){
            self.dismiss(animated: true, completion:{
               self.createInterstitialAd()
            })
            
        }
        else if((sender as AnyObject).tag == 2)
        {
            if(textFieldAlbumName.text?.count != 0 && (textFieldAlbumName.text?.count)! < 25)
            {
                if #available(iOS 10.0, *) {
                    if(CoreDataManager.sharedManager.saveAlbum(albumName: textFieldAlbumName.text!)){
                        var folderName = "/" + textFieldAlbumName.text!
                        let userName = userDefaults.value(forKey: "userName") as! String
                        if userDefaults.bool(forKey: "uploadToCloud"){
                            Utility.createBucketOnS3(bucketName: userName + "/" + textFieldAlbumName.text!)
                        }
                        createFolder(path: folderName)
                        folderName = folderName + "/thumbNail"
                        createFolder(path: folderName)
                        createInterstitialAd()
                        self.dismiss(animated: true, completion: nil)
                    } else {
                        Utility.showMessageWithOkButton(caller: self, title: "Alert", message: "Album already exists")
                         textFieldAlbumName.text = ""
                    }
                }
            }
            else
            {
//                Utility.showMessageWithOkButton(caller: self, title: "Alert", message: "Please chose an album name between 1 to 20 characters")
//                textFieldAlbumName.text = ""
            }
        }
    }
    func createFolder(path: String)->Void
    {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentDirectorPath:String = paths[0]
        // Get the Document directory path
        
        let  imagesDirectoryPath = documentDirectorPath.appending(path)
        var objcBool:ObjCBool = true
        let isExist = FileManager.default.fileExists(atPath: imagesDirectoryPath, isDirectory: &objcBool)
        // If the folder with the given path doesn't exist already, create it
        if isExist == false{
            do{
                try FileManager.default.createDirectory(atPath: imagesDirectoryPath, withIntermediateDirectories: true, attributes: nil)
            }catch{
                print("Something went wrong while creating a new folder")
            }
        }
    }
    //cahbaj
}
extension CreateAlbumViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let nsString = NSString(string: textField.text!)
        let newText = nsString.replacingCharacters(in: range, with: string)
        return  newText.count <= limitCount
    }
    func createInterstitialAd() -> () {
       
        let request = GADRequest()
        interstitial.load(request)
        if interstitial.isReady {
            interstitial.present(fromRootViewController: self)
        } else {
            print("Ad wasn't ready")
        }
    }
}
