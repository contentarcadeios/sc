

import UIKit
import AWSS3
import AWSCore
import Reachability

class SyncManager: NSObject {
    
    var albumsNamesOnLocalDevice = [String]()
    var albumsNamesOnBucket = [String]()
    var allLocalAlbumAndContents : [String:[String]] = [String:[String]] ()
    var allBucketAlbumAndContents : [String:[String]] = [String:[String]] ()
    var connection: Connection?
 
    
    static let sharedManager:SyncManager = SyncManager()
    func syncData()
    {
        let uploadToCloud = userDefaults.bool(forKey: "uploadToCloud")
        let wifiBackup = userDefaults.bool(forKey: "wifiBackup")
        if(wifiBackup){
            let reachability = Reachability()!
            if(reachability.connection == .wifi){
                print("Connected to wifi")
                if(uploadToCloud){
                    print("syncing data.........")
                    userDefaults.set(true, forKey: "isSyncing")
                    fetchTheLocalAlbums()
                    getTheAlbumNamesOnBucket { (local, bucket) in
                        if(bucket.count == 0){
                            userDefaults.set(false, forKey: "isSyncing")
                            S3DownloadManager.sharedManager.postNotification()
                        }
                        else{
                             userDefaults.set(false, forKey: "isSyncing")
                            self.objectsToDeleteORUpload(local: local, bucket: bucket, complitionOfDownload: {
                                NotificationCenter.default.post(name: .syncCompleted,
                                                                object: nil)
                            })
                        }
                    }
                }
            }
        }else
            {
                if(uploadToCloud)
                {
                    print("syncing data.........")
                    userDefaults.set(true, forKey: "isSyncing")
                    fetchTheLocalAlbums()
                    getTheAlbumNamesOnBucket { (local, bucket) in
                        if(bucket.count == 0){
                            userDefaults.set(false, forKey: "isSyncing")
                            S3DownloadManager.sharedManager.postNotification()
                        }
                        else{
                            userDefaults.set(false, forKey: "isSyncing")
                            self.objectsToDeleteORUpload(local: local, bucket: bucket, complitionOfDownload: {
                                NotificationCenter.default.post(name: .syncCompleted,
                                                                object: nil)
                            })
                        }
                    }
                }
            }
        }
    
    func fetchTheLocalAlbums()->Void{
        let albumsOnDevice = CoreDataManager.sharedManager.fetchAllalbums()
        for album in albumsOnDevice{
            let albumName = album.albumName!
            albumsNamesOnLocalDevice.append(albumName)
        }
        getTheObjectOfAlbumLocally()
        print("album on local device are = ",albumsNamesOnLocalDevice)
    }
    
    func getTheObjectOfAlbumLocally()->Void{
        for album in albumsNamesOnLocalDevice{
            let contentOfAlbum = getTheObjectsFromDocumentDirectory(albumName: album)
            
            allLocalAlbumAndContents[album] = contentOfAlbum
            print(allLocalAlbumAndContents)
        }
    }
    func getTheAlbumNamesOnBucket(complition:@escaping (_ localList:Dictionary<String, [String]>, _ bucketList: Dictionary<String,[String]>) ->())->Void{
        let userName = userDefaults.value(forKey: "userName") as! String
        let s3 = AWSS3.default()
        let req = AWSS3ListObjectsRequest()
        req?.bucket = "ca-ios-apps"
        req?.prefix = "ios/SecretCalculator/"+userName+"/"
        
        s3.listObjects(req!).continueWith { (task) -> Void in
            if (task.error != nil) {
                print("error fetching objects")
            }else{
                var objectName = [String]()
                for data in (task.result?.contents)!{
                    var contents = data.key?.components(separatedBy: "/")
                    contents?.removeLast()
                    
                    let objName = contents?.popLast()
                    if(objName == userName){
                        
                    }else if ((objName?.contains("jpg"))! || (objName?.contains("mov"))!)
                    {
                        
                    }else
                    {
                        var i = 0
                        for d in (task.result?.contents)!{
                            let key = d.key
                            if((key?.range(of: objName!)) != nil){
                                let content = key?.components(separatedBy: "/")
                                if (content?.count == 5 && content?.last != "")
                                {
                                    objectName.append(content![4])
                                }
                            }
                            if(i == (task.result?.contents?.count)! - 1){
                                self.allBucketAlbumAndContents[objName!] = objectName
                                objectName.removeAll()
                            }
                            i = i + 1
                        }
                       
                    }
                    print(self.allBucketAlbumAndContents)
                }
                print(self.albumsNamesOnBucket)
                
            }
            complition(self.allLocalAlbumAndContents, self.allBucketAlbumAndContents)
        }
    }
    func objectsToDeleteORUpload(local:Dictionary<String ,[String] >,bucket:Dictionary<String, [String]>,complitionOfDownload:@escaping ()->()) -> () {
        let localAlbums = Array(local.keys)
        let bucketAlbums = Array(bucket.keys)
        if (localAlbums.count == bucketAlbums.count){
            //upload
       
            for album in bucketAlbums{
                let objAtBucket = bucket[album]
                let objLocal = getTheObjectsInLocalAlbum(album: album)
                let objToUpload = objLocal.difference(from: objAtBucket!)
                print(objToUpload)
                var lastObject = String()
                if(objToUpload.count>0){
                    lastObject   = objToUpload.last!
                }
               
                for obj in objToUpload
                {
                    let url_of_obj = getTheURLofObj(album: album, objName: obj)
                    print(url_of_obj)
                    if(checkFile(url_of_obj)){
                        if(obj.contains(".mov"))
                        {
                            print("syncing Uploading movie......")
                            uploadVideoToS3(albumName: album, imageName: obj, fileUrl: url_of_obj)
                        }else
                        {
                            let img = UIImage()
                            print("syncing Uploading image......")
                            Utility.uploadImageToS3(image: img, albumName: album, imageName: obj, fileUrl: url_of_obj)
                        }
                    }
                    if(url_of_obj.path.contains(lastObject)){
                        S3DownloadManager.sharedManager.postNotification()
                    }
                }
            }
            // albums To Delete
            for album in bucketAlbums
            {
                let objAtBucket = bucket[album]
                let objLocal = getTheObjectsInLocalAlbum(album: album)
                let objToDelete = objAtBucket?.difference(from: objLocal)
                for obj in objToDelete!{
                    let url = getTheURLofObj(album: album, objName: obj)
                    if(!checkFile(url))
                    {
                        print("syncing deleting.........")
                        S3DownloadManager.sharedManager.deleteContentOfFolderOnS3(key: obj, album: album)
                    }
                }
            }
        }
        else if(bucketAlbums.count > localAlbums.count){

            let albumToDelete = localAlbums.difference(from: bucketAlbums)
            print("albums to delete",albumToDelete)
            for album in albumToDelete{
                //delete the album
                if let contentOfalbum = bucket[album]
                {
                    if(contentOfalbum.count != 0)
                    {
                        for con in contentOfalbum
                        {
                            S3DownloadManager.sharedManager.deleteContentOfFolderOnS3(key: con, album: album)
                            S3DownloadManager.sharedManager.deleteEmptyDirectoryOnS3(albumName: album)
                        }
                    }
                    else
                    {
                        S3DownloadManager.sharedManager.deleteEmptyDirectoryOnS3(albumName: album)
                    }
                    
                }
            }
            
        }else if(localAlbums.count > bucketAlbums.count){
            //upload Album
            let userName = userDefaults.value(forKey: "userName") as! String
            var albumToUpload = bucketAlbums.difference(from: localAlbums)
            print(albumToUpload)
            var objectesToUpload = [URL]()
            for album in albumToUpload
            {
                Utility.createBucketOnS3(bucketName: userName+"/"+album)
                do{
                    var documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                    documentsUrl = documentsUrl.appendingPathComponent(album)
                    let directoryContents = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil, options: [])
                    print(directoryContents)
                    for content in directoryContents{
                        if(content.path.contains(".jpg") || content.path.contains(".mov") ){
                            objectesToUpload.append(content)
                        }
                    }
                    print(objectesToUpload)
                    for obj in objectesToUpload{
                        let objContents = obj.path.components(separatedBy: "/")
                        let objName = objContents.last
                        if(obj.path.contains(".mov"))
                        {
                            Utility.uploadVideoToS3(albumName: album, imageName: objName!, fileUrl: obj)
                        }else
                        {
                            let img = UIImage()
                            Utility.uploadImageToS3(image: img, albumName: album, imageName: objName!, fileUrl: obj)
                        }
                    }
                   
                } catch {
                    print(error.localizedDescription)
                }
            }
            
        }
        complitionOfDownload()
      
    }

    //MARK:Document Directory Methods
    func getTheObjectsFromDocumentDirectory(albumName:String)->[String]{
        
        var fileNames = [String]()
        var documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        documentsUrl = documentsUrl.appendingPathComponent(albumName)
        
        do {
            // Get the directory contents urls (including subfolders urls)
            let directoryContents = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil, options: [])
            let mp3Files = directoryContents.filter{ $0.pathExtension == "jpg" || $0.pathExtension == "mov"}
            fileNames = mp3Files.map{ $0.lastPathComponent }
            
        } catch {
            print(error.localizedDescription)
        }
        return fileNames
    }
    
    //MARK: Checking the File Existance
    
    func checkFile(_ Url: URL) -> Bool
    {
        return FileManager.default.fileExists(atPath: Url.path) ? true : false
    }
    
    //MARK: List the local Objects
    
    func getTheObjectsInLocalAlbum(album:String)->[String]{
        var objectesToUpload = [String]()
        var documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        documentsUrl = documentsUrl.appendingPathComponent(album)
        do{
            let directoryContents = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil, options: [])
            print(directoryContents)
            for content in directoryContents{
                if(content.path.contains(".jpg") || content.path.contains(".mov") ){
                    objectesToUpload.append(content.lastPathComponent)
                }
            }
        } catch {
            print(error.localizedDescription)
        }
        return objectesToUpload
    }
    
    //MARK: URL OF LOCAL Objects
    
    func getTheURLofObj(album: String,objName: String)-> URL{
        var documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        documentsUrl = documentsUrl.appendingPathComponent(album)
        documentsUrl = documentsUrl.appendingPathComponent(objName, isDirectory: false)
        return documentsUrl
    }
    func uploadVideoToS3(albumName: String,imageName: String,fileUrl: URL)->Void{
        let userName = userDefaults.value(forKey: "userName") as! String
        let   key = userName + "/" + albumName + "/" + imageName
        let uploadRequest = AWSS3TransferManagerUploadRequest()
        uploadRequest?.key = key
        uploadRequest?.bucket = BUCKETONS3
        uploadRequest?.acl = AWSS3ObjectCannedACL.publicReadWrite
        uploadRequest?.contentType = "movie/mov"
        uploadRequest?.body = fileUrl
        uploadRequest?.serverSideEncryption = AWSS3ServerSideEncryption.awsKms
        uploadRequest?.uploadProgress = { (bytesSent, totalBytesSent, totalBytesExpectedToSend) -> Void in
            DispatchQueue.main.async(execute: {
//                print("totalBytesSent",totalBytesSent)
//                print("totalBytesExpectedToSend",totalBytesExpectedToSend)
//
//                var amountUploaded = totalBytesSent // To show the updating data status in label.
//                var fileSize = totalBytesExpectedToSend
            })
        }
        let transferManager = AWSS3TransferManager.default()
        transferManager.upload(uploadRequest!).continueWith(executor: AWSExecutor.mainThread(), block: { (task:AWSTask<AnyObject>) -> Any? in
            if task.error != nil {
                // Error.
                print("error")
            } else {
                // Do something with your result.
                print("No error Upload Done")
            }
            return nil
        })
    }
    func syncStarted(){
            print("Sync Completed")
            NotificationCenter.default.post(name: .syncStarted,
                                            object: nil
            )
    }
}
extension Array where Element: Hashable {
    func difference(from other: [Element]) -> [Element] {
        let thisSet = Set(self)
        let otherSet = Set(other)
        return Array(thisSet.symmetricDifference(otherSet))
    }
}

    

