//
//  S3DownloadManager.swift
//  LogoMaker
//
//  Created by Talha Ejaz on 9/22/17.
//  Copyright © 2017 contentarcade. All rights reserved.
//

import UIKit
import AWSS3
import AWSCore
class S3DownloadManager: NSObject {
    var listOfObjectsAtBucket = [String]()
    var albumsOnBucket = [String]()
    var objectNamesOnDevice = [String]()
    var objectNamesOnBucket = [String]()
    var urlsOfLocalObjects = [URL]()
    var objectsToUpload = [URL]()
    
    static let sharedManager:S3DownloadManager = S3DownloadManager()
    func createFolder(path: String)->Void
    {
        var objcBool:ObjCBool = true
        let isExist = FileManager.default.fileExists(atPath: path, isDirectory: &objcBool)
        // If the folder with the given path doesn't exist already, create it
        if isExist == false{
            do{
                try FileManager.default.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
            }catch{
                print("Something went wrong while creating a new folder")
            }
            
        }
    }
    
    func makeDownloadRequest(userName: String,albumName: String,imageName: String) -> AWSS3TransferManagerDownloadRequest?
    {
        let downloadRequest = AWSS3TransferManagerDownloadRequest()
        downloadRequest?.bucket = "ca-ios-apps"
        downloadRequest?.key = "ios/SecretCalculator/" + userName + "/" + albumName + "/" + imageName
        if let url = getUrlToPlaceLogo(_userName: userName, albumName: albumName, imageName: imageName)
        {
            downloadRequest?.downloadingFileURL = url
            return downloadRequest
        }
        else
        {
            return nil
        }
    }
    func getUrlToPlaceLogo( _userName: String,albumName: String,imageName: String) -> URL?
    {
        
        
        let categoryUrl = URL.init(fileURLWithPath: documentsPath+"/"+albumName)
        
        //        guard let imageUrl = URL.init(string: documentsPath+"/"+category+"/"+"\(index)"+".png")
        let imageUrl = URL.init(fileURLWithPath: documentsPath + "/" + albumName + "/" + imageName)
        
        if (!checkFile(categoryUrl))
        {
            //if this category does not has a directory then create it
            createDirectory(categoryUrl: categoryUrl)
            var  urlThumbNail = categoryUrl
            urlThumbNail = urlThumbNail.appendingPathComponent("thumbNail")
            createFolder(path: urlThumbNail.path)
        }
        
        if(!checkFile(imageUrl))
        {
            return imageUrl
        }
        else
        {
            return nil
            //dont download
        }
        
    }
    func checkFile(_ Url: URL) -> Bool
    {
        
        return FileManager.default.fileExists(atPath: Url.path) ? true : false
    }
    func createDirectory(categoryUrl: URL)->Void
    {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentDirectorPath:String = paths[0]
        // Get the Document directory path
        
        do{
            try FileManager.default.createDirectory(at: categoryUrl, withIntermediateDirectories: true, attributes: nil)
        }catch{
            print("Something went wrong while creating a new folder")
        }
        
    }
    func thumbnailForImage(image: UIImage) -> UIImage? {
        let imageData = UIImagePNGRepresentation(image)!
        let options = [
            kCGImageSourceCreateThumbnailWithTransform: true,
            kCGImageSourceCreateThumbnailFromImageAlways: true,
            kCGImageSourceThumbnailMaxPixelSize: 300] as CFDictionary
        let source = CGImageSourceCreateWithData(imageData as CFData, nil)!
        let imageReference = CGImageSourceCreateThumbnailAtIndex(source, 0, options)!
        let thumbnail = UIImage(cgImage: imageReference)
        return thumbnail
    }
    func fetchImageForThumbnail(path: String) -> UIImage {
        var thumbNailImage = UIImage()
        let fileManager = FileManager.default
        let documentsURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        var fullPath = documentsURL.appendingPathComponent(path)
        do {
            thumbNailImage  = UIImage(contentsOfFile: fullPath.path)!
        } catch {
            print("Error while enumerating files \(documentsURL.path): \(error.localizedDescription)")
        }
        return thumbNailImage
    }
    func saveThumbNailToDocument(selectedAlbum:String ,image: UIImage,nameOfImage: String)->Void{
        // Save image to Document directory
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] .appending("/" + selectedAlbum + "/thumbNail")
        
        //  imagePath = imagePath.replacingOccurrences(of: " ", with: "")
        let imagePath = documentsPath.appending(("/" + nameOfImage))
        let imageUrl = URL.init(string: imagePath)
        let data = UIImagePNGRepresentation(image)
        let success = FileManager.default.createFile(atPath: imagePath, contents: data, attributes: nil)
        if(success)
        {
            let path = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("\(selectedAlbum)/thumbNail/\(nameOfImage)")
            
            print("saved")
            NotificationCenter.default.post(name: .downloadImageCompleted,
                                            object: nil
            )
        }
        else{
            print("problem")
        }
    }
    //cahbaj
    
    
    
    //MARK: List the objects From Bucket
    func getTheListFromBucket(userName: String , completion: @escaping ()->()){
        
        let s3 = AWSS3.default()
        let req = AWSS3ListObjectsRequest()
        req?.bucket = "ca-ios-apps"
        req?.prefix = "ios/SecretCalculator/"+userName+"/"
        s3.listObjects(req!).continueWith { (task) -> Void in
            if (task.error != nil) {
                print("error fetching objects")
            }else{
                print(task.result?.contents?.count)
                if(task.result?.contents?.count != nil){
                    for data in (task.result?.contents)!{
                        self.listOfObjectsAtBucket.append(data.key!)
                    }
                }
                completion()
            }
          
        }
    }
    func checkAndCreateAlbum(albumName:String) -> Void {
        var documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] .appending("/" + albumName )
        var objcBool:ObjCBool = true
        let isExist = FileManager.default.fileExists(atPath: documentsPath, isDirectory: &objcBool)
        if isExist == false{
            do{
                try FileManager.default.createDirectory(atPath: documentsPath, withIntermediateDirectories: true, attributes: nil)
                documentsPath = documentsPath.appending("/thumbNail" )
                try FileManager.default.createDirectory(atPath: documentsPath, withIntermediateDirectories: true, attributes: nil)
                if(CoreDataManager.sharedManager.saveAlbum(albumName: albumName)){
                    DispatchQueue.main.async {
                    }
                }
                
            }catch{
                print("Something went wrong while creating a new folder")
            }
        }
    }
    func downloadContentsFromBucket(requestTransfer: AWSS3TransferManagerDownloadRequest, complition: @escaping ()->()) {
        let transferManager = AWSS3TransferManager.default()
        transferManager.download(requestTransfer).continueWith(block: ({ (task) ->  Void in
            if(task.result != nil)
            {
                complition()
            }
        }))
    }
    func syncData( completion: @escaping ()->())
    {
        userDefaults.set(true, forKey: "isSyncing")
        fetchTheLocalObjects()
        if let userName = userDefaults.value(forKey: "userName") as? String
        {
            
            getTheListFromBucket(userName: userName) {
                //spliting album name from s3 url
                if(self.listOfObjectsAtBucket.count == 0){
                    userDefaults.set(false, forKey: "isSyncing")
                    self.postNotification()
                    return
                }
                for data in self.listOfObjectsAtBucket
                {
                    let album = data
                    if(album.contains("jpg") || album.contains("mov")){
                 
                        let contents = album.split(separator: "/")
                        let objectName = "\(contents.last!)"
                        self.objectNamesOnBucket.append(objectName)
                    }
                    else{
                        
                        let contents = album.split(separator: "/")
                        let albumName = "\(contents.last!)"
                        if(albumName != userName){
                            self.albumsOnBucket.append(albumName)
                        }
                        
                    }
                }
                //---------------------------------
                //checking and creating album locally
                for data in self.albumsOnBucket{
                    self.checkAndCreateAlbum(albumName: data)
                    //update collection view
                    self.updateCollectionView()
                }
                //-----------------------------------
               // self.imageToUpload()

                let preferCloud = userDefaults.bool(forKey: "priorityCloud")
                if(preferCloud){
                    var i = 0
                    for object in self.listOfObjectsAtBucket{
                        if(object.contains("jpg") || object.contains("mov")){
                            // spliting url into album name, object name
                            let contents = object.split(separator: "/")
                            let imageName = "\(contents.last!)"
                            let albumName =  "\(contents[(contents.count - 2)])"
                            // -------------------------------------------------
                            let URL = self.createFileUrl(albumName: albumName, contentName: imageName) // creating url to check image exist or not
                            
                            if(albumName != userName)
                            {
                                if(!self.checkFile(URL)) //checking existance
                                {
                                    let requestTransfer = self.makeDownloadRequest(userName: userName, albumName: albumName, imageName: imageName)
                                    if(requestTransfer != nil)
                                    {
                                        print("requesting to download")
                                        self.downloadContentsFromBucket(requestTransfer: requestTransfer!, complition: {
                                            //create and save thumbNail
                                        
                                            if( object.contains("jpg")){
                                                let image = self.fetchImageForThumbnail(path: "\(albumName)/\(imageName)")
                                                if(image != nil){
                                                    let tNail =  self.thumbnailForImage(image: image)
                                                    self.saveThumbNailToDocument(selectedAlbum: albumName, image: tNail!, nameOfImage: imageName)
                                                    self.updateCollectionView()
                                                    if(self.objectNamesOnBucket.last?.contains(imageName))!{
                                                        print("download completed")
                                                        self.postNotification()
                                                        userDefaults.set(false, forKey: "priorityCloud")
                                                    }
                                                }

                                            }
                                            else{ //video
                                                let tNail =  DocumentManager.sharedManager.genrateThumbNailOfVideo(albumName: albumName, imageToFind: imageName)
                                                let imageName2 = imageName.replacingOccurrences(of: ".mov", with: "")
                                                DocumentManager.sharedManager.saveThumbNailOfVideo(imageName: imageName2, albumName: albumName, thumbNAilImage: tNail)
                                                self.updateCollectionView()
                                                if(self.objectNamesOnBucket.last?.contains(imageName))!{
                                                    print("download completed")
                                                    self.postNotification()
                                                    userDefaults.set(false, forKey: "priorityCloud")
                                                }
                                                
                                            }
                                         
                                        })
                                       
                                         completion()
                                    }
                                }
                            }
                        }
                   
                    }
                    
                }
//                if(self.objectNamesOnDevice.count == self.objectNamesOnBucket.count){
//                    userDefaults.set(false, forKey: "priorityCloud")
//
//                }
//                else
//                {
//                    if(self.objectNamesOnBucket.count > 0){
//                        var objectsToDeleteFromCloud = [String]()
//                        var urlOfObject = [String]()
//                        self.objectsToUpload.removeAll()
//                        //objects to delete from bucket
//                        let objects = self.objectNamesOnBucket.difference(from: self.objectNamesOnDevice)
//                        if(objects.count > 0){
//                            for ob in objects{
//                                 objectsToDeleteFromCloud.append(ob)
//                            }
//                            for obj in objectsToDeleteFromCloud{
//                                for obj2 in self.listOfObjectsAtBucket{
//                                    if(obj2.contains(obj)){
//                                        urlOfObject.append(obj2)
//                                    }
//                                }
//                            }
//                            for i in 0..<objectsToDeleteFromCloud.count{
//                                print("requesting to delete from cloud")
//                                let contentOfURL = urlOfObject[i].components(separatedBy: "/")
//                                let albumName = contentOfURL[3]
//                                let nameOfObject = objectsToDeleteFromCloud[i]
//                                self.deleteContentOfFolderOnS3(key: nameOfObject, album: albumName)
//                                self.deleteEmptyDirectoryOnS3(albumName: albumName)
//                            }
//                        }
//
//                        completion()
//                    }
//                }
                
            }
       
        }
        //       }
        
    }
    func createFileUrl(albumName:String, contentName: String) -> URL{
        let fileManager = FileManager.default
        let documentsURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let fullPath = documentsURL.appendingPathComponent("/\(albumName)/\(contentName)")
        return fullPath
    }
    func deleteAllObjectsinBucket(albumName: String , callbackHandler: @escaping () -> Void)-> Void{
        
        let userName = userDefaults.value(forKey: "userName") as! String
        var keys = [String]()
        let s3 = AWSS3.default()
        let req = AWSS3ListObjectsRequest()
        req?.bucket = "ca-ios-apps"
        req?.prefix = "ios/SecretCalculator/"+userName+"/"+albumName+"/"
        var result = s3.listObjects(req!).continueWith { (task) -> Void in
            if (task.error != nil) {
                print("error fetching objects")
            }else{
                if((task.result?.contents?.count) != nil){
                    for cont in (task.result?.contents)!{
                        keys.append(cont.key!)
                    }
                    for  i in 0 ..< keys.count{
                        
                        let key = keys[i]
                        let keies = key.components(separatedBy: "/")
                        if( keies.last!.range(of:"jpg") != nil || (keies.last!.range(of:"mov") != nil)){
                            self.deleteContentOfFolderOnS3(key: keies.last!, album: albumName)
                        }
                        
                    }
                }
                self.deleteEmptyDirectoryOnS3(albumName: albumName)
                keys.removeAll()
                callbackHandler()
            }
        }
    }
    func deleteContentOfFolderOnS3(key: String, album: String)->Void{
        
        let userName = userDefaults.value(forKey: "userName") as! String
        let s3 = AWSS3.default()
        let req = AWSS3DeleteObjectRequest()
        req?.bucket = BUCKETONS3 + "/" +  userName + "/" + album
        req?.key = key
        s3.deleteObject(req!)
    }
   func deleteEmptyDirectoryOnS3(albumName: String){
        let userName = userDefaults.value(forKey: "userName") as! String
        let s3 = AWSS3.default()
        let req = AWSS3DeleteObjectRequest()
        req?.bucket = "ca-ios-apps"
        req?.key = "ios/SecretCalculator/" + userName + "/" + albumName + "/"
        s3.deleteObject(req!)
    }

    func fetchTheLocalObjects()->Void{
        self.urlsOfLocalObjects.removeAll()
        self.objectNamesOnDevice.removeAll()
        let fileManager = FileManager.default
        let documentsURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first
        do {
            // Get the directory contents urls (including subfolders urls)
            if (documentsURL != nil){
                let directoryContents = try FileManager.default.contentsOfDirectory(at: documentsURL!, includingPropertiesForKeys: nil, options: [])
                for dicContent in directoryContents{
                    do{
                        let contentsOfAlbum = try FileManager.default.contentsOfDirectory(at: dicContent, includingPropertiesForKeys: nil, options: [])
                        print(contentsOfAlbum)
                        for object in contentsOfAlbum{
                            if (object.path.contains("mov") || object.path.contains("jpg")){
                                let file = object.lastPathComponent
                                if(!urlsOfLocalObjects.contains(object)){
                                    urlsOfLocalObjects.append(object.absoluteURL)
                                }
                                if(!objectNamesOnDevice.contains(file)){
                                    objectNamesOnDevice.append(file)
                                }
                            }
                        }
                        
                      print(objectNamesOnDevice)
                    }
                    catch{
                           print(error.localizedDescription)
                    }
                }
                let mp3Files = directoryContents.filter{ $0.pathExtension == "mp3" }
                print("mp3 urls:",mp3Files)
                let mp3FileNames = mp3Files.map{ $0.deletingPathExtension().lastPathComponent }
                print("mp3 list:", mp3FileNames)
            }
            
        }catch {
            print(error.localizedDescription)
        }
    }
    func imageToUpload()->Void{
        if(objectNamesOnBucket.count > 0){
            objectsToUpload.removeAll()
            let objects = objectNamesOnDevice.difference(from: objectNamesOnBucket)
            if(objects.count > 0){
                for ob in objects{
                    for url in urlsOfLocalObjects{
                        if(url.path.contains(ob)){
                            if(!objectsToUpload.contains(url)){
                                objectsToUpload.append(url)
                            }
                        }
                    }
                }
            }
        }
        for url in self.objectsToUpload{
            if(url.path.contains("jpg"))
            {
                let image = DocumentManager.sharedManager.fetchImageToUpload(path: url)
                if(image != nil){
                    let urlComponents = url.path.components(separatedBy: "/")
                    let imageName = urlComponents.last
                
                    print(imageName)
                    let albumName = urlComponents[urlComponents.count - 2]
                    print(albumName)
                    if(imageName != nil && albumName != nil){
                        Utility.uploadImageToS3(image: image, albumName: albumName, imageName: imageName!, fileUrl: url)
                    }
                }
            }
            else if(url.path.contains("mov")){
                let urlComponents = url.path.components(separatedBy: "/")
                let imageName = urlComponents.last
                
                print(imageName)
                let albumName = urlComponents[urlComponents.count - 2]
                print(albumName)
                if(imageName != nil && albumName != nil)
                {
                    Utility.uploadVideoToS3(albumName: albumName, imageName: imageName!, fileUrl: url)
                }
            }
        }
        print(objectsToUpload)
    }
    func checkTheExistanceOnBucket(key:String)->Void{
        let userName = userDefaults.value(forKey: "userName") as! String
        let s3 = AWSS3.default()
        
        let headObjectsRequest = AWSS3HeadObjectRequest()
        headObjectsRequest?.bucket = BUCKETONS3
        headObjectsRequest?.key = userName + "/awaismubeen/" + key
        s3.headObject(headObjectsRequest!).continueWith { (task) -> AnyObject? in
            if let error = task.error {
                print("listObjects failed: \(error)")
            }
            if let headObjectInfo = task.result {
                if headObjectInfo.contentLength != nil {
                    print("item exists")
                } else {
                    print("item does not exist")
                }
            }
            return nil
        }
    }
    
    func postNotification(){
        print("sync completed notification posted")
        NotificationCenter.default.post(name: .syncCompleted,
                                        object: nil
        )
    }
    func updateCollectionView(){
        print("refresh gallary notification posted")
        NotificationCenter.default.post(name: .updateCollectionView,
                                        object: nil
        )
    }
    //MARK: Copy Objects
    func copyObjectFromOneAlbumToOtherAlbum(sourceAlbum:String,completion:@escaping ([String])->()) -> Void {
        let userName = userDefaults.value(forKey: "userName") as! String
        var listOfObjectToMove = [String]()
        let s3 = AWSS3.default()
        let req = AWSS3ListObjectsRequest()
        req?.bucket = "ca-ios-apps"
        req?.prefix = "ios/SecretCalculator/"+userName+"/\(sourceAlbum)"
        s3.listObjects(req!).continueWith { (task) -> Void in
            if (task.error != nil) {
                print("error fetching objects")
            }else{
                for data in (task.result?.contents)!{
                    listOfObjectToMove.append(data.key!)
                    
                }
                completion(listOfObjectToMove)
            
            }
            
        }
    }
    func getNameOfObjectsAtBucket(path:[String]) -> [String] {
        var namesOfObjects = [String]()
        for p in path{
            let componentOfPath = p.components(separatedBy: "/")
            let name = componentOfPath.last
            if((name?.contains("jpg"))! || (name?.contains("mov"))! ){
                namesOfObjects.append(name!)
            }
        }
        return namesOfObjects
    }
}

extension Notification.Name {
    
    static let syncStarted = Notification.Name("syncStarted")
    static let updateCollectionView = Notification.Name("updateCollectionView")
    static let downloadImageCompleted = Notification.Name("downloadImageCompleted")
    
}

    
    

