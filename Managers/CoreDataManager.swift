//
//  PaymentManager.swift
//  Content Arcade
//
//  Created by Talha Ejaz on 06/01/2017.
//  Copyright © 2017 consultant. All rights reserved.
//

import StoreKit
import UIKit
import CoreData


//public typealias ProductIdentifier = String
//public typealias ProductsRequestCompletionHandler = (_ success: Bool, _ products: [SKProduct]?) -> ()


@available(iOS 10.0, *)
open class CoreDataManager : NSObject  {
    let appDelegate = UIApplication.shared.delegate as? AppDelegate
    static let sharedManager:CoreDataManager = CoreDataManager()
  
    
    public override init() {
        super.init()
    }
    
       
        func saveAlbum(albumName: String) -> Bool{
            var sucess = false
            //1
            let manageContex = appDelegate?.persistentContainer.viewContext
            //  checking existance of album
            let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Album")
            fetchRequest.predicate = NSPredicate(format: "albumName == %@", albumName)
            
            if let result = try? manageContex?.fetch(fetchRequest) {
                if(result?.count == 0){
                    //2
                    let entity =
                        NSEntityDescription.entity(forEntityName: "Album",
                                                   in: manageContex!)!
                    //3
                    let album = NSManagedObject(entity: entity,
                                                insertInto: manageContex)
                    
                    // 3
                    album.setValue(albumName, forKeyPath: "albumName")
                    
                    // 4
                    do
                    {
                        try manageContex?.save()
                        print("sucess")
                        sucess = true
                    } catch let error as NSError {
                        print("Could not save. \(error), \(error.userInfo)")
                    }
                }else{
                   sucess = false
                   
                }
            }
        return sucess
        }
    
    func fetchAllalbums() -> [Album] {
        var fetchedAlbums = [Album]()
        //1
        let manageContex = appDelegate?.persistentContainer.viewContext
        //2
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Album")
        do {
            let result = try manageContex?.fetch(fetchRequest) as! [Album]
            for data in result{
                fetchedAlbums.append(data)
                print(data.albumName)
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        return fetchedAlbums
    }
    
    func deleteAlbum(albumToDelete: String) -> Bool {
        var sucess = false
        let manageContex = appDelegate?.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Album")
        fetchRequest.predicate = NSPredicate(format: "albumName == %@", albumToDelete)
       
        if let result = try? manageContex?.fetch(fetchRequest) {
            for object in result! {
                manageContex?.delete(object)
            }
        }
        do {
            try manageContex?.save()
            sucess = true
        } catch {
           sucess = false
        }
        return sucess
    }
    func listFilesFromDocumentsFolder(albumName: String) -> [String] {
        var filesNames = [String]()
        let fullPath = albumName + "/thumbNail"
        var documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        documentsUrl = documentsUrl.appendingPathComponent(fullPath, isDirectory: true)
        
        do {
            // Get the directory contents urls (including subfolders urls)
            let directoryContents = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil, options: [])
            
            
            // if you want to filter the directory contents you can do like this:
        
            let pngFiles = directoryContents.filter{ ($0.pathExtension == "jpg") || ($0.pathExtension == "mov")}
            print("mp3 urls:",pngFiles)
            let pngFileNames = pngFiles.map{ $0.deletingPathExtension().lastPathComponent }
            filesNames = pngFileNames
            
            
        } catch {
            print(error.localizedDescription)
        }
        return filesNames
    }
    func deleteAllData() {
        let manageContex = appDelegate?.persistentContainer.viewContext
        //2
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Album")
        do {
            let result = try manageContex?.fetch(fetchRequest)
            for data in result!{
               guard let objectData = data as? NSManagedObject else {continue}
                manageContex?.delete(data)
            }
            try manageContex?.save()
        } catch let error as NSError {
            print("Could not delete. \(error), \(error.userInfo)")
        }
    }
}


