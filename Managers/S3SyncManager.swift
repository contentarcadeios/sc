//
//  S3DownloadManager.swift
//  LogoMaker
//
//  Created by Talha Ejaz on 9/22/17.
//  Copyright © 2017 contentarcade. All rights reserved.
//

import UIKit
import AWSS3
import AWSCore
class S3SyncManager: NSObject {
    var albumAndObjects : [String:[String]] = [String:[String]] ()
    
    static let sharedManager:S3SyncManager = S3SyncManager()
    func downloadAllobjectsFromS3()-> Void{
        print("Downloading objects in S3SyncManager")
        let userName = getTheUserName()
        if(userName != nil){
            getTheListFromBucket(userName: userName) { (objectsAtBucket) in
                print(objectsAtBucket)
                if(objectsAtBucket.count > 0){
                    userDefaults.set(true, forKey: "isSyncing")
                    self.startSyncing()
                    let albumAndObjects = self.getTheAlbumsAndObjects(listOfObjects: objectsAtBucket)
                    print(albumAndObjects)
                    let albumNames = albumAndObjects.0
                    let objectsToDown = albumAndObjects.1
                    print(albumNames)
                    if(objectsToDown.count == 0){
                        self.downloadCompleted()
                        UserDefaults.standard.set(false, forKey: "priorityCloud")
                    }
                    self.createAlbumOnDevice(albumNames: albumNames)
                    self.downloadFileFromS3(albums: albumNames, objectsToDownload: objectsToDown)
                }
                else{
                    userDefaults.set(false, forKey: "isSyncing")
                    self.downloadCompleted()
                }
            }
        }
        
    }
    func getTheUserName() -> String {
        let userName = userDefaults.value(forKey: "userName") as! String
        return userName
    }
    func getTheListFromBucket(userName: String , completion: @escaping ([String])->()){
        var listOfObjectsAtBucket = [String]()
        let s3 = AWSS3.default()
        let req = AWSS3ListObjectsRequest()
        req?.bucket = "ca-ios-apps"
        req?.prefix = "ios/SecretCalculator/"+userName+"/"
        s3.listObjects(req!).continueWith { (task) -> Void in
            if (task.error != nil) {
                print("error fetching objects")
            }else{
                print(task.result?.contents?.count)
                if(task.result?.contents?.count != nil){
                    for data in (task.result?.contents)!{
                        listOfObjectsAtBucket.append(data.key!)
                    }
                }
                
            }
           completion(listOfObjectsAtBucket)
        }
    }
    func getTheAlbumsAndObjects(listOfObjects: [String]) -> ([String], [String]){
        var albumNames = [String]()
        var objectsInAlbum = [String]()
        for object in listOfObjects{
            var components = object.components(separatedBy: "/")
            if (components.last == ""){
                components.removeLast()
            }
            let lastComponent = components.last
            if(lastComponent?.contains("@"))!{
                //noting to do
            }
            else if((lastComponent?.contains("jpg"))! || (lastComponent?.contains("mov"))!){
                objectsInAlbum.append(lastComponent!)
            }
            else{
                albumNames.append(lastComponent!)
            }
        }
        return  (albumNames, objectsInAlbum)
    }
    func createAlbumOnDevice(albumNames: [String]){
        for albumName in albumNames
        {
            let fileManager = FileManager.default
            if let tDocumentDirectory = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first
            {
                let filePath =  tDocumentDirectory.appendingPathComponent("\(albumName)")
                let thumbnailPath = filePath.appendingPathComponent("thumbNail")
                if !self.checkTheExistance(pathOfDir: filePath.path, isExist: true)
                {
                    do
                    {
                        try fileManager.createDirectory(atPath: filePath.path, withIntermediateDirectories: true, attributes: nil)
                        try fileManager.createDirectory(atPath: thumbnailPath.path, withIntermediateDirectories: true, attributes: nil)
                        CoreDataManager.sharedManager.saveAlbum(albumName: albumName)
                        self.updateCollectionView()
                    }
                    catch
                    {
                        NSLog("Couldn't create document directory")
                    }
                }
                NSLog("Document directory is \(filePath)")
            }
        }
        
    }
    func checkTheExistance(pathOfDir: String , isExist: ObjCBool) -> Bool{
        var isDir  = isExist
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: pathOfDir, isDirectory:&isDir)
        {
            print(isDir.boolValue ? "Directory exists" : "File exists")
            return true
        }
        else
        {
            print("File does not exist")
            return false
        }
    }
    func downloadFileFromS3(albums: [String] ,objectsToDownload: [String])->Void{
        var i = 0
        for album in albums{
            for object in objectsToDownload{
                if(object.contains(album)){
                   let url = getUrlToPlaceObject(_userName: self.getTheUserName(), albumName: album, imageName: object)
                    if(url != nil){
                        if(!checkFile(url!)){
                            let request =  makeDownloadRequest(userName: self.getTheUserName(), albumName: album, imageName: object, url: url!)
                            if(request != nil)
                            {
                                downloadContentsFromBucket(requestTransfer: request!)
                                {
                                //create thumbnail
                                    if(object.contains("mov"))
                                    {
                                        let thumbnail = DocumentManager.sharedManager.genrateThumbNailOfVideo(albumName: album, imageToFind: object).fixedOrientation()
                                        if(object.contains(".mov")){
                                        DocumentManager.sharedManager.saveThumbNailOfVideo(imageName: object.replacingOccurrences(of: ".mov", with: ".jpg"), albumName: album, thumbNAilImage: thumbnail)
                                        }
                                        else
                                        {
                                            DocumentManager.sharedManager.saveThumbNailOfVideo(imageName: object, albumName: album, thumbNAilImage: thumbnail)
                                        }
                                        if (i == objectsToDownload.count - 1) {
                                            userDefaults.set(false, forKey: "priorityCloud")
                                            userDefaults.set(false, forKey: "isSyncing")
                                            print("download completed posted by s3sync manager")
                                            self.downloadCompleted()
                                        }else
                                        {
                                            self.updateCollectionView()
                                            i = i + 1
                                        }
                                    }
                                    else
                                    {
                                        self.createThumbNAilForImage(album: album, imageName: object)
                                        if (i == objectsToDownload.count - 1) {
                                            userDefaults.set(false, forKey: "priorityCloud")
                                            userDefaults.set(false, forKey: "isSyncing")
                                            print("download completed posted by s3sync manager")
                                            self.downloadCompleted()
                                        }
                                        else
                                        {
                                            self.updateCollectionView()
                                            i = i + 1
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    func getUrlToPlaceObject( _userName: String,albumName: String,imageName: String) -> URL?
    {
        let categoryUrl = URL.init(fileURLWithPath: documentsPath+"/"+albumName)
        
        //        guard let imageUrl = URL.init(string: documentsPath+"/"+category+"/"+"\(index)"+".png")
        let imageUrl = URL.init(fileURLWithPath: documentsPath + "/" + albumName + "/" + imageName)
        
        if (!checkTheExistance(pathOfDir: categoryUrl.path, isExist: true))
        {
            //if this category does not has a directory then create it
            var  urlThumbNail = categoryUrl
            urlThumbNail = urlThumbNail.appendingPathComponent("thumbNail")
            createFolder(path: urlThumbNail.path)
        }
        
        if(!checkTheExistance(pathOfDir: imageUrl.path, isExist: false))
        {
            return imageUrl
        }
        else
        {
            return imageUrl
            //dont download
        }
        
    }
    func createFolder(path: String)->Void
    {
        var objcBool:ObjCBool = true
        let isExist = FileManager.default.fileExists(atPath: path, isDirectory: &objcBool)
        // If the folder with the given path doesn't exist already, create it
        if isExist == false{
            do{
                try FileManager.default.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
            }catch{
                print("Something went wrong while creating a new folder")
            }
            
        }
    }
    func makeDownloadRequest(userName: String,albumName: String,imageName: String, url: URL) -> AWSS3TransferManagerDownloadRequest?
    {
        let downloadRequest = AWSS3TransferManagerDownloadRequest()
        downloadRequest?.bucket = "ca-ios-apps"
        downloadRequest?.key = "ios/SecretCalculator/" + userName + "/" + albumName + "/" + imageName
        downloadRequest?.downloadingFileURL = url
        return downloadRequest
     
    }
    func downloadContentsFromBucket(requestTransfer: AWSS3TransferManagerDownloadRequest, complition: @escaping ()->()) {
        let transferManager = AWSS3TransferManager.default()
        transferManager.download(requestTransfer).continueWith(block: ({ (task) ->  Void in
            if(task.result != nil)
            {
                complition()
            }
        }))
    }
    func createThumbNAilForImage(album: String, imageName: String){
      
        let image = self.fetchImageForThumbnail(path: "/\(album)/\(imageName)")
        if(image != nil){
            let tNail =  self.thumbnailForImage(image: image)
            self.saveThumbNailToDocument(selectedAlbum: album, image: tNail!, nameOfImage: imageName)
        }
    }
    func fetchImageForThumbnail(path: String) -> UIImage {
        var thumbNailImage = UIImage()
        let fileManager = FileManager.default
        let documentsURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let fullPath = documentsURL.appendingPathComponent(path)
        do {
            thumbNailImage  = UIImage(contentsOfFile: fullPath.path)!
            self.updateCollectionView()
        } catch {
            print("Error while enumerating files \(documentsURL.path): \(error.localizedDescription)")
        }
        return thumbNailImage
    }
    func thumbnailForImage(image: UIImage) -> UIImage? {
        let imageData = UIImagePNGRepresentation(image)!
        let options = [
            kCGImageSourceCreateThumbnailWithTransform: true,
            kCGImageSourceCreateThumbnailFromImageAlways: true,
            kCGImageSourceThumbnailMaxPixelSize: 300] as CFDictionary
        let source = CGImageSourceCreateWithData(imageData as CFData, nil)!
        let imageReference = CGImageSourceCreateThumbnailAtIndex(source, 0, options)!
        let thumbnail = UIImage(cgImage: imageReference)
        return thumbnail
    }
    func saveThumbNailToDocument(selectedAlbum:String ,image: UIImage,nameOfImage: String)->Void{
        // Save image to Document directory
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] .appending("/" + selectedAlbum + "/thumbNail")
        //  imagePath = imagePath.replacingOccurrences(of: " ", with: "")
        let imagePath = documentsPath.appending(("/" + nameOfImage))
        let imageUrl = URL.init(string: imagePath)
        let data = UIImagePNGRepresentation(image)
        let success = FileManager.default.createFile(atPath: imagePath, contents: data, attributes: nil)
        if(success)
        {
            print("saved")
        }
        else{
            print("problem")
        }
    }
    func checkFile(_ Url: URL) -> Bool
    {
        return FileManager.default.fileExists(atPath: Url.path) ? true : false
    }
    func updateCollectionView(){
        print("refresh gallary notification posted")
        NotificationCenter.default.post(name: .updateCollectionView,
                                        object: nil
        )
    }
    func downloadCompleted(){
        print("Sync Completed")
        userDefaults.set(false, forKey: "isSyncing")
        NotificationCenter.default.post(name: .syncCompleted,
                                        object: nil
        )
    }
    func startSyncing(){
        print("Sync Started")
        NotificationCenter.default.post(name: .syncStarted,
                                        object: nil)
    }
}



    
    

