//
//  DocumentManager.swift
//  Secret Calculator
//
//  Created by Awais on 19/08/2018.
//  Copyright © 2018 Awais. All rights reserved.
//

import UIKit
import StoreKit
import AVFoundation
import AVKit

open class DocumentManager: NSObject {
    let appDelegate = UIApplication.shared.delegate as? AppDelegate
    static let sharedManager:DocumentManager = DocumentManager()
    
    
    public override init() {
        super.init()
    }
    func deleteDirectoryAndContents(albumToDelete: String)-> Bool{
        var sucess = false
        let fileManager = FileManager.default
        var dirPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        dirPath.append("/"+albumToDelete)
        do {
            try fileManager.removeItem(atPath: dirPath)
            sucess = true
            return sucess
        }
        catch let error as NSError {
            sucess = false
            print("Ooops! Something went wrong: \(error)")
            return sucess
        }
    }
    func saveMovieToDocument(albumName: String,videoData: Data)->Bool{
        // Save image to Document directory
        let img = UIImage(data: videoData)
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] .appending("/" + albumName)
        let unique_ID = NSUUID().uuidString
        var imagePath =  NSDate().description
        let imageToFind = unique_ID.appending(".mov")
        imagePath = imagePath.replacingOccurrences(of: " ", with: "")
        imagePath = documentsPath.appending(("/\(albumName)\("movie" + unique_ID).mov"))

        let success = FileManager.default.createFile(atPath: imagePath, contents: videoData, attributes: nil)
        if(success)
        {
            //Utility.uploadVideoToS3(albumName: albumName, imageName: imageToFind, fileUrl: URL.init(string: imagePath)!) //upload video s3 bucket
            let tNail =  genrateThumbNailOfVideo(albumName: albumName, imageToFind: "\(albumName)movie"+imageToFind) // generating thumbnail
            saveThumbNailOfVideo(imageName: "\(albumName)movie" + unique_ID, albumName: albumName, thumbNAilImage: tNail) // save thumbnail of video
            return true
        }
        else{
            print("problem")
            return false
        }
    }
    func genrateThumbNailOfVideo(albumName: String,imageToFind: String) -> UIImage {
        var videoThumbNail = UIImage()
        
        do{
            var documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            documentsUrl = documentsUrl.appendingPathComponent(albumName, isDirectory: true)
            let directoryContents = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil, options: [])
            let videoFiles = directoryContents.filter{ $0.lastPathComponent == imageToFind }
            let asset = AVURLAsset(url: videoFiles.first!, options: nil)
            let duration = asset.duration.seconds
            let generator = AVAssetImageGenerator(asset: asset)
            do{
                let cgImage = try generator.copyCGImage(at: CMTimeMake(0, 1), actualTime: nil)
                videoThumbNail = UIImage(cgImage: cgImage)
            }
        }
        catch{
            
        }
        return videoThumbNail
    }
    func saveThumbNailOfVideo(imageName: String,albumName: String,thumbNAilImage: UIImage) -> Void {
        var documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        documentsUrl = documentsUrl.appendingPathComponent(albumName + "/thumbNail", isDirectory: true)
        if(imageName.contains(".jpg")){
            documentsUrl = documentsUrl.appendingPathComponent(imageName,isDirectory: false)
        }else
        {
            documentsUrl = documentsUrl.appendingPathComponent(imageName + ".jpg",isDirectory: false)
        }
        let data = UIImagePNGRepresentation(thumbNAilImage.fixedOrientation())
         let success = FileManager.default.createFile(atPath: documentsUrl.path , contents: data, attributes: nil)
        if(success)
        {
            print("sucess")
        }
    }
    func fetchImageToUpload(path: URL) -> UIImage {
        var thumbNailImage = UIImage()
        let fileManager = FileManager.default
        let documentsURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        do {
            thumbNailImage  = UIImage(contentsOfFile: path.path)!
        } catch {
            print("Error while enumerating files \(documentsURL.path): \(error.localizedDescription)")
        }
        return thumbNailImage
    }
    func fetchVideoToUpload(path: URL)-> Data{
        var videoData = Data()
        let fileManager = FileManager.default
        let documentsURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        do {
            videoData  = try Data(contentsOf: path)
        } catch {
            print("Error while enumerating files \(documentsURL.path): \(error.localizedDescription)")
        }
        return videoData
    }
    
    func mapThumbnailsWithObjects(){
        let albums = CoreDataManager.sharedManager.fetchAllalbums()
        for album in albums{
            let albumName = album.albumName
            if(albumName != nil){
                let fileManager = FileManager.default
                let documentsURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
                let full_path = documentsURL.appendingPathComponent(albumName!)
                let thumbnail_path = full_path.appendingPathComponent("thumbNail")
                do {
                    let urls_of_objects = try fileManager.contentsOfDirectory(atPath: full_path.path)
                    let urls_of_thumbnails = try fileManager.contentsOfDirectory(atPath: thumbnail_path.path)
                    
                } catch {
                    print("Error while enumerating files \(documentsURL.path): \(error.localizedDescription)")
                }
            }
        }
    }
    func countVideosAndImages() -> CGFloat {
        
        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        var sizeOfDirectory : CGFloat = 0
        do {
            // Get the directory contents urls (including subfolders urls)
            let directoryContents = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil, options: [])
            print(directoryContents)
            for dir in directoryContents{
                do{
                    let contents = try FileManager.default.contentsOfDirectory(at: dir, includingPropertiesForKeys: nil, options: [.skipsHiddenFiles])
                    for cont in contents{
                        if((cont.path.range(of: "jpg") != nil) || (cont.path.range(of: "mov") != nil) ){
                            if(cont.path.contains("jpg")){
                                do{
                                    let atr = try FileManager.default.attributesOfItem(atPath: cont.path)
                                    print( atr.values)
                                    sizeOfDirectory = sizeOfDirectory + CGFloat(truncating: atr[.size] as! NSNumber)
                                    
                                }
                                catch{
                                    print("error")
                                }
                            }
                            else if (cont.path.contains("mov")){
                                do{
                                    let atr = try FileManager.default.attributesOfItem(atPath: cont.path)
                                    print( atr.values)
                                    sizeOfDirectory = sizeOfDirectory + CGFloat(truncating: atr[.size] as! NSNumber)
                                    
                                }
                                catch{
                                    print("error")
                                }
                            }
                        }
                    }
                }
                catch{
                    print(error.localizedDescription)
                }
            }
            // if you want to filter the directory contents you can do like this:
            let mp3Files = directoryContents.filter{ $0.pathExtension == "mp3" }
            print("mp3 urls:",mp3Files)
            let mp3FileNames = mp3Files.map{ $0.deletingPathExtension().lastPathComponent }
            print("mp3 list:", mp3FileNames)
            
        } catch {
            print(error.localizedDescription)
        }
        return sizeOfDirectory
    }
    private func sizeToPrettyString(size: CGFloat) -> String {
        
        let byteCountFormatter = ByteCountFormatter()
        byteCountFormatter.allowedUnits = .useMB
        byteCountFormatter.countStyle = .file
        let folderSizeToDisplay = byteCountFormatter.string(fromByteCount: Int64(size))
        
        return folderSizeToDisplay
        
    }
    
}
