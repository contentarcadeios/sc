//
//  SubscriptionManager.swift
//  Secret Calculator
//
//  Created by Awais on 06/12/2018.
//  Copyright © 2018 Awais. All rights reserved.
//

import UIKit
import StoreKit
public enum SubscriptionError: Error {
    case missingAccountSecret
    case invalidSession
    case noActiveSubscription
    case other(Error)
}
public enum Result<T> {
    case failure(SubscriptionError)
    case success(T)
}
public enum ReceiptStatus: Int {
    // Not decodable status
    case unknown = -2
    // No status returned
    case none = -1
    // valid statu
    case valid = 0
    // The App Store could not read the JSON object you provided.
    case jsonNotReadable = 21000
    // The data in the receipt-data property was malformed or missing.
    case malformedOrMissingData = 21002
    // The receipt could not be authenticated.
    case receiptCouldNotBeAuthenticated = 21003
    // The shared secret you provided does not match the shared secret on file for your account.
    case secretNotMatching = 21004
    // The receipt server is not currently available.
    case receiptServerUnavailable = 21005
    // This receipt is valid but the subscription has expired. When this status code is returned to your server, the receipt data is also decoded and returned as part of the response.
    case subscriptionExpired = 21006
    //  This receipt is from the test environment, but it was sent to the production environment for verification. Send it to the test environment instead.
    case testReceipt = 21007
    // This receipt is from the production environment, but it was sent to the test environment for verification. Send it to the production environment instead.
    case productionEnvironment = 21008
    
    var isValid: Bool { return self == .valid}
}

class SubscriptionManager: NSObject {
    
    static let sessionIdSetNotification = Notification.Name("SubscriptionServiceSessionIdSetNotification")
    static let optionsLoadedNotification = Notification.Name("SubscriptionServiceOptionsLoadedNotification")
    static let restoreSuccessfulNotification = Notification.Name("SubscriptionServiceRestoreSuccessfulNotification")
    static let purchaseSuccessfulNotification = Notification.Name("SubscriptionServiceRestoreSuccessfulNotification")
    public typealias UploadReceiptCompletion = (_ result: Result<(sessionId: String, currentSubscription: String)>) -> Void
  
    var options: [Subscription]? {
        didSet {
            NotificationCenter.default.post(name: SubscriptionManager.optionsLoadedNotification, object: options)
        }
    }
    let appDelegate = UIApplication.shared.delegate as? AppDelegate
    static let sharedManager:SubscriptionManager = SubscriptionManager()
    func loadSubscriptionOptions() {
        let productIDs = Set([tenGB, twentyFiveGB, hundredGB, twoFiftyGB]) // all defined in constents.swifts
        let request = SKProductsRequest(productIdentifiers: productIDs)
        request.delegate = self
        request.start()
    }
    func canMakePurchases() -> Bool {
        return SKPaymentQueue.canMakePayments()
    }
    //MARK: Purchase Product
    func purchase(subscription: Subscription) {
        if self.canMakePurchases() {
            let payment = SKPayment(product: subscription.product)
            SKPaymentQueue.default().add(payment)
        }
    }
    
    //MARK: Upload Recept After Sucessfull Purchase
    //This method loads receipt data from the device
    private func loadReceipt() -> Data? {
        guard let url = Bundle.main.appStoreReceiptURL else {
            return nil
        }
        do {
            let data = try Data(contentsOf: url)
            return data
        } catch {
            print("Error loading receipt data: \(error.localizedDescription)")
            return nil
        }
    }
    func uploadReceipt(completion: ((_ success: Bool) -> Void)? = nil) {
        if let receiptData = loadReceipt() {
            upload(receipt: receiptData) { [weak self] (result) in
                guard let strongSelf = self else { return }
                switch result {
                case .success(let result):
                     let currentSessionId = result.sessionId
                     let currentSubscription = result.currentSubscription
                    completion?(true)
                case .failure(let error):
                    print("🚫 Receipt Upload Failed: \(error)")
                    completion?(false)
                }
            }
        }
    }
    
    func upload(receipt: Data, completion: @escaping UploadReceiptCompletion) {
        let body = [
            "receipt-data": receipt.base64EncodedString(),
            "password": shareedSecret
        ]
        let bodyData = try! JSONSerialization.data(withJSONObject: body, options: [])
        
        let url = URL(string: "https://sandbox.itunes.apple.com/verifyReceipt")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = bodyData
        
        let task = URLSession.shared.dataTask(with: request) { (responseData, response, error) in
            if let error = error {
//                completion(.failure(.other(error)))
            } else if let responseData = responseData {
                let json = try! JSONSerialization.jsonObject(with: responseData, options: []) as! Dictionary<String, Any>
                print(json)
                    if let status = json["status"] as? Int {
                    let receiptStatus = ReceiptStatus(rawValue: status) ?? ReceiptStatus.unknown
                    if case .testReceipt = receiptStatus {
                    
                    } else {
                        if receiptStatus.isValid {
                            completion(.success(receipt: receiptInfo))
                        } else {
                            completion(.error(error: .receiptInvalid(receipt: receiptInfo, status: receiptStatus)))
                        }
                    }
                }
//                let session = Session(receiptData: data, parsedReceipt: json)
//                self.sessions[session.id] = session
//                let result = (sessionId: session.id, currentSubscription: session.currentSubscription)
//                completion(.success(result))
            }
        }
        
        task.resume()
    }
    func expirationDateFromResponse(jsonResponse: NSDictionary) -> NSDate? {
        if let receiptInfo: NSArray = jsonResponse["latest_receipt_info"] as? NSArray {
            let lastReceipt = receiptInfo.lastObject as! NSDictionary
            var formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss VV"
            let expirationDate: NSDate = formatter.date(from: lastReceipt["expires_date"] as! String) as NSDate!
            return expirationDate
        } else {
            return nil
        }
    }
}
extension SubscriptionManager: SKProductsRequestDelegate{
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        options = response.products.map { Subscription(product: $0) }
    }
    
    func request(_ request: SKRequest, didFailWithError error: Error) {
        if request is SKProductsRequest {
            print("Subscription Options Failed Loading: \(error.localizedDescription)")
        }
    }
}
