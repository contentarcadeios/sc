//
//  ProgressIndicator.swift
//  HSK
//
//  Created by Anees ur Rehman on 01/09/2016.
//  Copyright © 2016 consultant. All rights reserved.
//

import UIKit
import MBProgressHUD

class ProgressIndicator: MBProgressHUD {
    
    //MARK: - Progress Hud Methods
    
    class func showHUDAddedTo(_ view: UIView) -> ProgressIndicator {
        return showAdded(to: view, animated: true)
    }
    
    class func hideHUDForView(_ view: UIView) -> Bool {
        return self.hide(for: view, animated: true)
    }
    
    func hide() {
        self.hide(animated: true)
    }
    
    class func showHUDAddedTo(_ view: UIView, customView:UIView, forDelay delay: TimeInterval) -> ProgressIndicator {
        
        let hud = ProgressIndicator.showHUDAddedTo(view)
        hud.mode = .customView
        hud.label.text = ""
        hud.detailsLabel.text = ""
        hud.customView = customView
        hud.margin = 0.0
        hud.bezelView.color = UIColor(white: 0.0, alpha: 0.5)
        hud.bezelView.style = .solidColor
        hud.hide(animated: true, afterDelay: delay)
        return hud
        
    }
}
