//
//  Constants.swift
//  WeightLoss
//
//  Created by Talha Ejaz on 3/25/17.
//  Copyright © 2017 contentarcade. All rights reserved.
//

import Foundation

import UIKit

/// App Singelton Methods

let userDefaults:UserDefaults = UserDefaults.standard
let notifCenter:NotificationCenter = NotificationCenter.default
let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
let appScreen = UIScreen.main

//app id 1444999695
/// Constants
let deviceScale:CGFloat = UIScreen.main.scale
let SUBSCRIPTION_PRO = "com.contentarcade.cadroid"
let shareedSecret = "ac677a85120944f48d765d5a4dc74890"
let receiptURL = Bundle.main.appStoreReceiptURL
let URL_RATE_US = "itms-apps://itunes.apple.com/app/id1444999695"
let adUnitID = "ca-app-pub-3005749278400559/1800963624"
let adInterstital = "ca-app-pub-3940256099942544/4411468910"

let BUCKETONS3 = "ca-ios-apps/ios/SecretCalculator"
let tenGB = "com.contentarcade.calx.10gb"
let twentyFiveGB = "com.contentarcade.calx.25gb"
let hundredGB = "com.contentarcade.calx.100gb"
let twoFiftyGB = "com.contentarcade.calx.250gb"
let subscribedCat = "subscribedPackage"
let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
let TOBYTES : CGFloat = 1073741824.0
//let TOBYTES : CGFloat = 10485760.0
let TERMCONDITIONS: String = "http://www.contentarcade.com/calx-term-of-use"
let OPENPRIVACYLINK: String = "http://contentarcade.com/calx-policy"
let PURCHASED = "purchased"
let NOTPURCHASED = "notpurchased"
let EXPIRED = "expired"

struct ScreenSize
{
    static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType
{
    static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad
}
/* Enums  */
enum RecipesType:String {
    case first = "BREAKFAST", second = "LUNCH", third = "DINNER",forth = "SNACK"
}

/* Functions  */
func s3ImageUrl( _userName: String, _albumName:String, imageName: String) -> URL? {
    let string = "https://s3.us-east-2.amazonaws.com/ca-ios-apps/ios/SecretCalculator/\(_userName  )/\(_albumName)/\(imageName)"
    //print(string)
    let updatedString = string.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
    return URL(string: updatedString!)
}
func isValidEmail(testStr:String) -> Bool {
    // print("validate calendar: \(testStr)")
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
    
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: testStr)
}

func heightForView(_ text:String, font:UIFont, width:CGFloat) -> CGFloat{
    
    let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
    label.numberOfLines = 0
    label.lineBreakMode = NSLineBreakMode.byWordWrapping
    label.font = font
    label.text = text
    
    label.sizeToFit()
    return label.frame.height
}

func openApps(completion: @escaping ((_ success: Bool)->())) {
    guard let url = URL(string : "itms-apps://itunes.apple.com/developer/content-arcade-dubai-ltd-fze/id1355616995") else {
        completion(false)
        return
    }
    guard #available(iOS 10, *) else {
        completion(UIApplication.shared.openURL(url))
        return
    }
    UIApplication.shared.open(url, options: [:], completionHandler: completion)
}




func verifyUrl (urlString: String?) -> Bool {
    //Check for nil
    if let urlString = urlString {
        // create NSURL instance
        if let url = NSURL(string: urlString) {
            // check if your application can open the NSURL instance
            return UIApplication.shared.canOpenURL(url as URL)
        }
    }
    return false
}

/* Messages  */
let MSG_FAVORITES = "Favorites"
let MSG_SHOPPING_LIST = "Shopping List"
let MSG_HOME_TITLE = "Grilled Recipes"
let FIRST_MEAL_TYPE = "Breakfast"
let SECOND_MEAL_TYPE = "Lunch"
let THIRD_MEAL_TYPE = "Dinner"
let FORTH_MEAL_TYPE = "Snack"
let MSG_MESSAGE = "Message"
let MSG_CANCEL = "Cancel"



