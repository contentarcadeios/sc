//
//  Utility.swift
//  HSK
//
//  Created by Anees ur Rehman on 26/09/2016.
//  Copyright © 2016 consultant. All rights reserved.
//

import UIKit
import AWSS3
import GoogleMobileAds

class Utility: NSObject{

    var interstitial: GADInterstitial!
    class func showErrorAlert(caller:UIViewController,message:String) -> Void {
        
        Utility.showAlert(caller: caller, title: "Alert", message: message)
    }
    class func showAlert(caller:UIViewController,title:String?,message:String) -> Void {
        
        
        let alert = UIAlertController(title:title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        caller.present(alert, animated: true, completion: nil)
    }
    class func showAlertWithTwoButtons(caller:UIViewController,title: String, message: String,firstButtonTitle:String = "Cancel" ,firstButtonStyle: UIAlertActionStyle = UIAlertActionStyle.cancel , firstButtonHandler: ((UIAlertAction) -> Void)? = nil , secondButtonTitle: String,secondButtonStyle: UIAlertActionStyle = UIAlertActionStyle.default , secondButtonHandler: ((UIAlertAction) -> Void)? = nil ) -> Void {
        
        
        let alert = UIAlertController(title:title, message:message, preferredStyle: .alert)
        
        
        alert.addAction(UIAlertAction(title: firstButtonTitle, style: firstButtonStyle, handler:firstButtonHandler))
        alert.addAction(UIAlertAction(title: secondButtonTitle, style: secondButtonStyle, handler:secondButtonHandler))
        
        caller.present(alert, animated: true, completion: {
           
        })
    }
    class func showMessageWithOkButton(caller: UIViewController,title: String, message: String,
    firstButtonStyle: UIAlertActionStyle = UIAlertActionStyle.cancel , firstButtonHandler: ((UIAlertAction) -> Void)? = nil )
    {
        let alert = UIAlertController(title:title, message:message, preferredStyle: .alert)
         alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:firstButtonHandler))
        
        caller.present(alert, animated: true, completion: nil)
    }
    class func createBucketOnS3(bucketName:String)->Void{
        let s3 = AWSS3.default()
        let req = AWSS3PutObjectRequest()
        req?.bucket = BUCKETONS3
        req?.key = bucketName + "/"
        s3.putObject(req!).continueWith(block: {(task: AWSTask) -> AnyObject? in
        if let error = task.error {
            print("Error occurred: \(error)")
            return nil
        }
        print("Created successfully.")
        return nil
    })
            
    }
    class func deleteAlbumFromS3(albumName: String){
        let userName = userDefaults.value(forKey: "userName") as! String
        let s3 = AWSS3.default()
        let req = AWSS3DeleteBucketRequest()
       // let req = AWSS3DeleteObjectRequest()
         let key = userName + "/" + albumName
        req?.bucket = BUCKETONS3 + key
    
        s3.deleteBucket(req!).continueWith(block: {(task: AWSTask) -> AnyObject? in
            if let error = task.error {
                print("Error occurred: \(error)")
                return nil
            }
            print("Deleted successfully.")
            return nil
        })
    }
    class func uploadImageToS3(image: UIImage,albumName: String,imageName: String,fileUrl: URL)->Void{
        let userName = userDefaults.value(forKey: "userName") as! String
       // let imageData = UIImageJPEGRepresentation(image, 0)
        let uploadRequest = AWSS3TransferManagerUploadRequest()
        uploadRequest?.bucket = BUCKETONS3
        let key = userName + "/" + albumName + "/" + imageName
        uploadRequest?.key = key
        uploadRequest?.body = (fileUrl as URL?)!
        uploadRequest?.serverSideEncryption = AWSS3ServerSideEncryption.awsKms
        uploadRequest?.acl = AWSS3ObjectCannedACL.publicReadWrite
        uploadRequest?.uploadProgress = { (bytesSent, totalBytesSent, totalBytesExpectedToSend) -> Void in
            DispatchQueue.main.async(execute: {
                                    print("totalBytesSent",totalBytesSent)
                                    print("totalBytesExpectedToSend",totalBytesExpectedToSend)
                
                                     var amountUploaded = totalBytesSent // To show the updating data status in label.
                                    var fileSize = totalBytesExpectedToSend
            })
        }
        
        let transferManager = AWSS3TransferManager.default()
        transferManager.upload(uploadRequest!).continueWith(executor: AWSExecutor.mainThread(), block: { (task:AWSTask<AnyObject>) -> Any? in
            if task.error != nil {
                // Error.
                print("error")
            } else {
                // Do something with your result.
                print("No error Upload Done")
            }
            return nil
        })
    }
    
    class func uploadVideoToS3(albumName: String,imageName: String,fileUrl: URL)->Void{
        let userName = userDefaults.value(forKey: "userName") as! String
        var path = ""

      
        path = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("\(albumName)/\("movie" + imageName)")

        let fileUrl2 = NSURL(fileURLWithPath: path)
        let uploadRequest = AWSS3TransferManagerUploadRequest()
        uploadRequest?.body = (fileUrl2 as URL?)!
      
    
         let   key = userName + "/" + albumName + "/movie" + imageName
    
        
        uploadRequest?.key = key
        uploadRequest?.bucket = BUCKETONS3

        uploadRequest?.acl = AWSS3ObjectCannedACL.publicReadWrite
        uploadRequest?.contentType = "movie/mov"
        uploadRequest?.serverSideEncryption = AWSS3ServerSideEncryption.awsKms
        uploadRequest?.uploadProgress = { (bytesSent, totalBytesSent, totalBytesExpectedToSend) -> Void in
            DispatchQueue.main.async(execute: {
                print("totalBytesSent",totalBytesSent)
                print("totalBytesExpectedToSend",totalBytesExpectedToSend)

                var amountUploaded = totalBytesSent // To show the updating data status in label.
                var fileSize = totalBytesExpectedToSend
            })
        }
        
        let transferManager = AWSS3TransferManager.default()
        transferManager.upload(uploadRequest!).continueWith(executor: AWSExecutor.mainThread(), block: { (task:AWSTask<AnyObject>) -> Any? in
            if task.error != nil {
                // Error.
                print("error")
            } else {
                // Do something with your result.
                print("No error Upload Done")
            }
            return nil
        })
    }
    //typealias callbackHandler = ()  -> Void
    class func uploadVideosToS3(albumName: String,destinationAlbum: String,imageName: String,fileUrl: URL,complition: @escaping (_ sourceAlbum: String,_ imageName: String, _ destinitionAlbum:String,_ fileUrl:URL)->())->Void{
        let userName = userDefaults.value(forKey: "userName") as! String
      
       
          let  path = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("\(albumName)/\(imageName)")
  
        
        let fileUrl2 = NSURL(fileURLWithPath: path)
        let uploadRequest = AWSS3TransferManagerUploadRequest()
        uploadRequest?.body = (fileUrl2 as URL?)!
  

        let key = userName + "/" + destinationAlbum + "/" + imageName
        
        uploadRequest?.key = key
        uploadRequest?.bucket = BUCKETONS3
        
        uploadRequest?.acl = AWSS3ObjectCannedACL.publicReadWrite
        uploadRequest?.contentType = "movie/mov"
        uploadRequest?.serverSideEncryption = AWSS3ServerSideEncryption.awsKms
        uploadRequest?.uploadProgress = { (bytesSent, totalBytesSent, totalBytesExpectedToSend) -> Void in
            DispatchQueue.main.async(execute: {
                print("totalBytesSent",totalBytesSent)
                print("totalBytesExpectedToSend",totalBytesExpectedToSend)
                
                var amountUploaded = totalBytesSent // To show the updating data status in label.
                var fileSize = totalBytesExpectedToSend
            })
        }
        
        let transferManager = AWSS3TransferManager.default()
        transferManager.upload(uploadRequest!).continueWith(executor: AWSExecutor.mainThread(), block: { (task:AWSTask<AnyObject>) -> Any? in
            if task.error != nil {
                // Error.
                print("error")
            } else {
                // Do something with your result.
                complition(albumName,imageName,destinationAlbum,fileUrl)
                
                print("No error Upload Done")
            }
            return nil
        })
    }
    class func uploadImageToS3TOMove(albumName: String,desAlbum:String,imageName: String,fileUrl: URL,complition:@escaping(_ albumName: String,_ desAlbum:String,_ imageName: String,_ fileUrl: URL)->Void)->Void{
        let userName = userDefaults.value(forKey: "userName") as! String
        let uploadRequest = AWSS3TransferManagerUploadRequest()
        uploadRequest?.bucket = BUCKETONS3
        let key = userName + "/" + desAlbum + "/" + imageName
        uploadRequest?.key = key
        uploadRequest?.body = (fileUrl as URL?)!
        uploadRequest?.serverSideEncryption = AWSS3ServerSideEncryption.awsKms
        uploadRequest?.acl = AWSS3ObjectCannedACL.publicReadWrite
        uploadRequest?.uploadProgress = { (bytesSent, totalBytesSent, totalBytesExpectedToSend) -> Void in
            DispatchQueue.main.async(execute: {
                print("totalBytesSent",totalBytesSent)
                print("totalBytesExpectedToSend",totalBytesExpectedToSend)
                
                var amountUploaded = totalBytesSent // To show the updating data status in label.
                var fileSize = totalBytesExpectedToSend
            })
        }
        
        let transferManager = AWSS3TransferManager.default()
        transferManager.upload(uploadRequest!).continueWith(executor: AWSExecutor.mainThread(), block: { (task:AWSTask<AnyObject>) -> Any? in
            if task.error != nil {
                // Error.
                print("error")
            } else {
                // Do something with your result.
                complition(albumName,desAlbum,imageName,fileUrl)
                print("No error Upload Done")
            }
            return nil
        })
    }
    
    func createInterstitialAd(vc : UIViewController) -> () {
        interstitial = GADInterstitial(adUnitID: adInterstital)
        let request = GADRequest()
        interstitial.load(request)
        if interstitial.isReady {
            interstitial.present(fromRootViewController: vc)
        } else {
            print("Ad wasn't ready")
        }
    }
}
  

